{
    "id": "33111208-4a3f-49ce-bd4d-1bf9b36b5f8a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objUITileSelector",
    "eventList": [
        {
            "id": "f19f3442-8d2c-4738-baa2-d9c8617448ce",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "33111208-4a3f-49ce-bd4d-1bf9b36b5f8a"
        },
        {
            "id": "dec68362-44e0-41dd-8a23-ac940d075c7a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "33111208-4a3f-49ce-bd4d-1bf9b36b5f8a"
        },
        {
            "id": "7010db4e-68be-4805-9b9a-4bdc89d97b1d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "33111208-4a3f-49ce-bd4d-1bf9b36b5f8a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "f4a9cfe3-3ffd-4ff4-94e8-4f8bd9f4eea8",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}