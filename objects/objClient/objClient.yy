{
    "id": "20652d91-8f7d-40c0-9ccd-c77b832e510a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objClient",
    "eventList": [
        {
            "id": "84573298-d314-426a-bb58-9c57b396e370",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "20652d91-8f7d-40c0-9ccd-c77b832e510a"
        },
        {
            "id": "4bc33288-9734-4a6f-89e4-f9a4d3a4637e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 68,
            "eventtype": 7,
            "m_owner": "20652d91-8f7d-40c0-9ccd-c77b832e510a"
        },
        {
            "id": "b4c3b8e4-f5df-42d7-a859-7bd83340b65d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "20652d91-8f7d-40c0-9ccd-c77b832e510a"
        },
        {
            "id": "26cfe02f-0e1e-4a79-9d50-5622c12b7d8c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "20652d91-8f7d-40c0-9ccd-c77b832e510a"
        },
        {
            "id": "47376868-a88f-42dd-8f6c-d6ea39fc304a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "20652d91-8f7d-40c0-9ccd-c77b832e510a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d3a7757f-04a5-4aec-8244-f32fad0e7ee2",
    "visible": true
}