{
    "id": "69a54d79-9e2b-4186-a54e-c2ad72de67e2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objLayerPanel",
    "eventList": [
        {
            "id": "dc1e2dbb-8764-4fe4-b10d-f35a07d55c60",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "69a54d79-9e2b-4186-a54e-c2ad72de67e2"
        },
        {
            "id": "eb33ccd4-4b7b-4473-bd0d-cfc3ad96899d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "69a54d79-9e2b-4186-a54e-c2ad72de67e2"
        },
        {
            "id": "e852ca55-9445-4bb9-a539-e819865272a5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "69a54d79-9e2b-4186-a54e-c2ad72de67e2"
        },
        {
            "id": "1b019a97-978d-447d-8084-b9eac921a79e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "69a54d79-9e2b-4186-a54e-c2ad72de67e2"
        },
        {
            "id": "cb98e0b2-a183-4501-bbda-2d81c11c4ca3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "69a54d79-9e2b-4186-a54e-c2ad72de67e2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "54a84626-d5c3-4d7f-bf46-ba1e5a336898",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}