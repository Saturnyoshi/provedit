{
    "id": "54a84626-d5c3-4d7f-bf46-ba1e5a336898",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "parCaptureMouse",
    "eventList": [
        {
            "id": "b313ca7c-8116-4330-a2c4-21e75b9cafcb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "54a84626-d5c3-4d7f-bf46-ba1e5a336898"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "fc835c85-edbe-4d08-af84-db3c8637e581",
    "visible": true
}