{
    "id": "5c8c9120-234e-4345-ab07-10c47aceeb82",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objCollisionBoss",
    "eventList": [
        {
            "id": "63271326-cfa4-4da1-bce8-fb5135803dc3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5c8c9120-234e-4345-ab07-10c47aceeb82"
        }
    ],
    "maskSpriteId": "b2dd7c49-639d-447c-bfe3-e1f7c3fbc8f6",
    "overriddenProperties": null,
    "parentObjectId": "633fe443-8442-4666-8cfa-430f5009d781",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d0994106-416c-4c3b-9476-2fcdd48516fc",
    "visible": false
}