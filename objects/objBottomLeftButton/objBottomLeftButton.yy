{
    "id": "5c7abfd7-0a1c-4bc2-8c1e-143d3c5f6b25",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objBottomLeftButton",
    "eventList": [
        {
            "id": "30752180-8da3-4ff5-af17-f1e210e26445",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5c7abfd7-0a1c-4bc2-8c1e-143d3c5f6b25"
        },
        {
            "id": "9e5ab928-0214-4048-928f-22ccb6b54668",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "5c7abfd7-0a1c-4bc2-8c1e-143d3c5f6b25"
        },
        {
            "id": "3b3ff6cd-9524-4559-8127-d5928b63fd56",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "5c7abfd7-0a1c-4bc2-8c1e-143d3c5f6b25"
        },
        {
            "id": "bb89fcb6-50ce-43cf-9654-e0511bd42892",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "5c7abfd7-0a1c-4bc2-8c1e-143d3c5f6b25"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}