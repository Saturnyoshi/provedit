{
    "id": "8429df93-f15e-4403-a356-2edbc9a277d2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objTileLayer",
    "eventList": [
        {
            "id": "56d0f2b3-3c76-4c34-b02c-b12dd767f2b6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8429df93-f15e-4403-a356-2edbc9a277d2"
        },
        {
            "id": "e8ffb5f7-2ed5-4a01-884f-6927b5d7c94c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "8429df93-f15e-4403-a356-2edbc9a277d2"
        },
        {
            "id": "d780436b-c0de-4bfa-9d67-4dc8fd82253a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "8429df93-f15e-4403-a356-2edbc9a277d2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "fc835c85-edbe-4d08-af84-db3c8637e581",
    "visible": true
}