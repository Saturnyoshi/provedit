{
    "id": "d5a23d7a-49ec-49c1-a3a0-a809adf201d8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objCollisionBoss2",
    "eventList": [
        {
            "id": "215c653f-4fef-4292-8144-a05ccef93391",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d5a23d7a-49ec-49c1-a3a0-a809adf201d8"
        }
    ],
    "maskSpriteId": "b2dd7c49-639d-447c-bfe3-e1f7c3fbc8f6",
    "overriddenProperties": null,
    "parentObjectId": "633fe443-8442-4666-8cfa-430f5009d781",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "6b72689d-1e7c-45df-924f-cc877b8e7ec3",
    "visible": false
}