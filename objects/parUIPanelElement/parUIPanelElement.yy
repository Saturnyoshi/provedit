{
    "id": "f4a9cfe3-3ffd-4ff4-94e8-4f8bd9f4eea8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "parUIPanelElement",
    "eventList": [
        {
            "id": "cf6dec77-c1b1-4b8d-bd77-3b362a0128dd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f4a9cfe3-3ffd-4ff4-94e8-4f8bd9f4eea8"
        },
        {
            "id": "2b3f9056-3a97-4de9-ab52-77827a2a77f0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 76,
            "eventtype": 8,
            "m_owner": "f4a9cfe3-3ffd-4ff4-94e8-4f8bd9f4eea8"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "fc835c85-edbe-4d08-af84-db3c8637e581",
    "visible": true
}