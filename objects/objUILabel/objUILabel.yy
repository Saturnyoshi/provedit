{
    "id": "81b244b3-ae52-495c-afdf-a70c080b7d5e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objUILabel",
    "eventList": [
        {
            "id": "e43d9dae-09f2-46e2-9aa9-4edeae95453c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "81b244b3-ae52-495c-afdf-a70c080b7d5e"
        },
        {
            "id": "7ba46061-4e04-4912-8ee1-4b4dbde92d43",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "81b244b3-ae52-495c-afdf-a70c080b7d5e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "f4a9cfe3-3ffd-4ff4-94e8-4f8bd9f4eea8",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}