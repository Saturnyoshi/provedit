{
    "id": "5ec25251-2259-4621-9df9-afb298ff64ab",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objCollisionSlow",
    "eventList": [
        {
            "id": "07939571-aa30-42f4-9f55-57b70bed6934",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5ec25251-2259-4621-9df9-afb298ff64ab"
        }
    ],
    "maskSpriteId": "b2dd7c49-639d-447c-bfe3-e1f7c3fbc8f6",
    "overriddenProperties": null,
    "parentObjectId": "be25b882-01d3-4e8b-a49a-99faf016cf94",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "49fab134-f9bf-468c-b475-bfbd416807b9",
    "visible": false
}