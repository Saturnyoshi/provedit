{
    "id": "6149bcf5-057e-4249-8345-db5653acfa2a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objSidebarButton",
    "eventList": [
        {
            "id": "087ec01d-a839-4b36-af68-92a2b5a690cd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "6149bcf5-057e-4249-8345-db5653acfa2a"
        },
        {
            "id": "645a477b-657e-4ad3-9663-02c47cfa0a43",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6149bcf5-057e-4249-8345-db5653acfa2a"
        },
        {
            "id": "759a7d74-25c6-4619-9259-31b759ccaf4d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "6149bcf5-057e-4249-8345-db5653acfa2a"
        },
        {
            "id": "fe4516a6-f6e8-48e0-a4b0-4d5ff8375498",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "6149bcf5-057e-4249-8345-db5653acfa2a"
        },
        {
            "id": "244cb324-7038-4a90-b4e2-8822cd315b91",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "6149bcf5-057e-4249-8345-db5653acfa2a"
        },
        {
            "id": "0c2868f5-06dd-48d2-bec5-47374791ceaf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "6149bcf5-057e-4249-8345-db5653acfa2a"
        },
        {
            "id": "8d04f5c1-8131-443a-b22a-1670791758e7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 72,
            "eventtype": 8,
            "m_owner": "6149bcf5-057e-4249-8345-db5653acfa2a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}