{
    "id": "9f50e1c2-3783-4a7d-8d37-ecf2efc8d7bf",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objHistory",
    "eventList": [
        {
            "id": "f52ca47e-2c0d-43d8-ad3d-1a1720fe63c7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9f50e1c2-3783-4a7d-8d37-ecf2efc8d7bf"
        },
        {
            "id": "b8035868-47f0-4211-bd14-37e0121dabb8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "9f50e1c2-3783-4a7d-8d37-ecf2efc8d7bf"
        },
        {
            "id": "a79021b3-5174-4f55-8fe3-7b4d7c068e57",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 17,
            "eventtype": 5,
            "m_owner": "9f50e1c2-3783-4a7d-8d37-ecf2efc8d7bf"
        },
        {
            "id": "30430172-d31d-4cab-b3a6-3f6c306b6824",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "9f50e1c2-3783-4a7d-8d37-ecf2efc8d7bf"
        },
        {
            "id": "be607b1e-f47e-42b4-9add-eacdaecaf9cc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "9f50e1c2-3783-4a7d-8d37-ecf2efc8d7bf"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d3a7757f-04a5-4aec-8244-f32fad0e7ee2",
    "visible": true
}