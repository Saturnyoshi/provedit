{
    "id": "0ffce396-8101-4263-afb4-8806ac1e0922",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objDropdownDisplay",
    "eventList": [
        {
            "id": "dbb90a0a-ab04-4fbb-8781-9e3fcf604a32",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0ffce396-8101-4263-afb4-8806ac1e0922"
        },
        {
            "id": "e5636c3c-397a-4d3b-8ed2-f03eb8ef9622",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "0ffce396-8101-4263-afb4-8806ac1e0922"
        },
        {
            "id": "16f4cbe9-fb9a-4084-ba36-cc60887f17f6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "0ffce396-8101-4263-afb4-8806ac1e0922"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "54a84626-d5c3-4d7f-bf46-ba1e5a336898",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}