{
    "id": "70f94062-7dcc-488f-a5ef-ce2bd1f8158d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objSnapCorners",
    "eventList": [
        {
            "id": "50b345b3-2632-44f2-a6b6-67e6f5fba003",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "70f94062-7dcc-488f-a5ef-ce2bd1f8158d"
        },
        {
            "id": "3e81f3dd-67af-470b-a3bb-30cf0250e426",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "70f94062-7dcc-488f-a5ef-ce2bd1f8158d"
        },
        {
            "id": "5934220b-6ece-4aa2-9463-56144b03da32",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "70f94062-7dcc-488f-a5ef-ce2bd1f8158d"
        },
        {
            "id": "df80b988-fb2c-4a42-a641-ff822b413b2d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "70f94062-7dcc-488f-a5ef-ce2bd1f8158d"
        },
        {
            "id": "bfcdc998-f1b7-466e-831f-7da274ebf781",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "70f94062-7dcc-488f-a5ef-ce2bd1f8158d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}