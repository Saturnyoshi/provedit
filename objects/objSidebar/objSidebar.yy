{
    "id": "b05dfc5e-109f-45c3-b15e-b1370185ed0f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objSidebar",
    "eventList": [
        {
            "id": "7f902dcd-732b-4063-9a0a-9498d490537c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "b05dfc5e-109f-45c3-b15e-b1370185ed0f"
        },
        {
            "id": "8ac0caa6-71ff-494a-a59f-4aede1d911bf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b05dfc5e-109f-45c3-b15e-b1370185ed0f"
        },
        {
            "id": "b02ed5c4-e1cf-4a3a-8e39-3064cdae6cd5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b05dfc5e-109f-45c3-b15e-b1370185ed0f"
        },
        {
            "id": "b1b52023-84ce-4023-910d-216acf7e7ea4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "b05dfc5e-109f-45c3-b15e-b1370185ed0f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d3a7757f-04a5-4aec-8244-f32fad0e7ee2",
    "visible": true
}