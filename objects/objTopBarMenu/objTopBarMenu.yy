{
    "id": "1873c73f-0124-4f4c-83d0-76b050bfcc09",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objTopBarMenu",
    "eventList": [
        {
            "id": "fc1d1bec-04ac-4015-9363-4db9221aa4d0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1873c73f-0124-4f4c-83d0-76b050bfcc09"
        },
        {
            "id": "5caae6c5-95ad-4025-bf58-0df6c5fccc40",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "1873c73f-0124-4f4c-83d0-76b050bfcc09"
        },
        {
            "id": "f0159e79-db4d-412f-b75c-4d142fdd4871",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "1873c73f-0124-4f4c-83d0-76b050bfcc09"
        },
        {
            "id": "724393b4-2bbd-4e2f-b31a-42ff687197be",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "1873c73f-0124-4f4c-83d0-76b050bfcc09"
        },
        {
            "id": "a0229c88-bcd8-4f1c-9521-4bbaee0362ca",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "1873c73f-0124-4f4c-83d0-76b050bfcc09"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}