{
    "id": "53eb8749-69c4-4bd3-8ab8-af861dfbce14",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objHighlighterEventHandler",
    "eventList": [
        {
            "id": "cbf59f30-4097-4e6f-956f-54678002234f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "53eb8749-69c4-4bd3-8ab8-af861dfbce14"
        },
        {
            "id": "19accfbe-5f21-4598-a8b8-527c55e15b9b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 13,
            "eventtype": 7,
            "m_owner": "53eb8749-69c4-4bd3-8ab8-af861dfbce14"
        },
        {
            "id": "d80097c5-3b1e-4746-8a9a-7dee010fe562",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "53eb8749-69c4-4bd3-8ab8-af861dfbce14"
        },
        {
            "id": "bdcb027d-93c3-4169-9021-fb2ab3c7be6f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "53eb8749-69c4-4bd3-8ab8-af861dfbce14"
        },
        {
            "id": "0e7bf021-db04-42a0-8fb6-13f100baad26",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "53eb8749-69c4-4bd3-8ab8-af861dfbce14"
        },
        {
            "id": "29fdd15b-fe62-478e-a9dd-40cad9083de9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 73,
            "eventtype": 8,
            "m_owner": "53eb8749-69c4-4bd3-8ab8-af861dfbce14"
        },
        {
            "id": "5e50930b-6d09-411b-831c-b9a89deeb127",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "53eb8749-69c4-4bd3-8ab8-af861dfbce14"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}