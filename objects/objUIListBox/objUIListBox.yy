{
    "id": "a6c0f06b-18f3-4428-a6bf-fb4964d0e967",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objUIListBox",
    "eventList": [
        {
            "id": "c849cb19-9e8b-4b2a-94ee-46a23fe84f3e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a6c0f06b-18f3-4428-a6bf-fb4964d0e967"
        },
        {
            "id": "45fc7913-ab6d-46b9-8d55-915d80885f75",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "a6c0f06b-18f3-4428-a6bf-fb4964d0e967"
        },
        {
            "id": "f887bec9-ee20-4984-8b31-8f7b29de615e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "a6c0f06b-18f3-4428-a6bf-fb4964d0e967"
        },
        {
            "id": "02285498-21ea-408a-84ea-316cec3d731a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "a6c0f06b-18f3-4428-a6bf-fb4964d0e967"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "f4a9cfe3-3ffd-4ff4-94e8-4f8bd9f4eea8",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}