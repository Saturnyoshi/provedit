{
    "id": "29f54e96-37d7-4a83-99da-3cee2f4d18e1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objToolPanel",
    "eventList": [
        {
            "id": "0201383c-99eb-4d46-8c9d-047f11b3a5cd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "29f54e96-37d7-4a83-99da-3cee2f4d18e1"
        },
        {
            "id": "a98997db-1926-4d59-b766-8f3b7de77ca3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "29f54e96-37d7-4a83-99da-3cee2f4d18e1"
        },
        {
            "id": "abbdbeb1-4d9d-4ac7-a7b4-8e5d60734a9d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "29f54e96-37d7-4a83-99da-3cee2f4d18e1"
        },
        {
            "id": "8e3d63bc-f5fa-48eb-a711-82e6a4be1049",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "29f54e96-37d7-4a83-99da-3cee2f4d18e1"
        },
        {
            "id": "20070b89-4679-4f6b-8579-f5e7688fd887",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "29f54e96-37d7-4a83-99da-3cee2f4d18e1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d3a7757f-04a5-4aec-8244-f32fad0e7ee2",
    "visible": true
}