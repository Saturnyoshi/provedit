{
    "id": "8bef4dcb-2dba-41a3-9885-207ccb9b1ad9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objResizeBoundaries",
    "eventList": [
        {
            "id": "f4facbbd-0ebd-4c72-98ec-63d159dde7b0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 73,
            "eventtype": 8,
            "m_owner": "8bef4dcb-2dba-41a3-9885-207ccb9b1ad9"
        },
        {
            "id": "b62fe4d0-b302-4c8a-b5b7-274a4088b346",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8bef4dcb-2dba-41a3-9885-207ccb9b1ad9"
        },
        {
            "id": "1e12542a-d396-403c-b809-2e1cd7f13547",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "8bef4dcb-2dba-41a3-9885-207ccb9b1ad9"
        },
        {
            "id": "c47a22b7-1b8f-4ca3-8aab-7d914d8876ce",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "8bef4dcb-2dba-41a3-9885-207ccb9b1ad9"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}