{
    "id": "efb17bec-9b21-4822-961e-9ed90a7e7867",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "parForm",
    "eventList": [
        {
            "id": "a8a9c6d3-00df-410a-af5d-e97e3ba4ddad",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "efb17bec-9b21-4822-961e-9ed90a7e7867"
        },
        {
            "id": "6eb949d3-ccdb-420a-a74e-0cf157ad0ecc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "efb17bec-9b21-4822-961e-9ed90a7e7867"
        },
        {
            "id": "5f226ccc-6dec-4b07-9766-a0f874b14b78",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "efb17bec-9b21-4822-961e-9ed90a7e7867"
        },
        {
            "id": "197c8b7d-8489-4e58-b65a-27b5dd209be3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "efb17bec-9b21-4822-961e-9ed90a7e7867"
        },
        {
            "id": "0cbb5501-cc33-451f-aa43-dae699c128af",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "efb17bec-9b21-4822-961e-9ed90a7e7867"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "54a84626-d5c3-4d7f-bf46-ba1e5a336898",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "fc835c85-edbe-4d08-af84-db3c8637e581",
    "visible": true
}