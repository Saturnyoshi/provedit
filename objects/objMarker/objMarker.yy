{
    "id": "f73693d3-1d58-4257-9d52-3cc4f3627900",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objMarker",
    "eventList": [
        {
            "id": "cc88e90a-a95a-4f73-beaa-1db656f00c19",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f73693d3-1d58-4257-9d52-3cc4f3627900"
        },
        {
            "id": "7e52c385-a989-4a25-b1a5-a825917f51fd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "f73693d3-1d58-4257-9d52-3cc4f3627900"
        },
        {
            "id": "e53d92ea-58c8-4cb8-be9a-b71cfb41dd0f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "f73693d3-1d58-4257-9d52-3cc4f3627900"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "3e2b9669-50e5-4b95-b257-38586075f8d7",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "092c744c-95d3-4dea-a100-f3ca66ecd530",
    "visible": true
}