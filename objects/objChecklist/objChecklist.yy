{
    "id": "30427aac-d2ad-41a1-aab4-a8b170df0648",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objChecklist",
    "eventList": [
        {
            "id": "b42b50de-79ad-42bd-b9c8-6e42791e8cd0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "30427aac-d2ad-41a1-aab4-a8b170df0648"
        },
        {
            "id": "429ee3cf-c5b8-42a9-b440-01a74b4dabdd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "30427aac-d2ad-41a1-aab4-a8b170df0648"
        },
        {
            "id": "708ce797-dca0-49fb-92ef-b45451ddbd46",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "30427aac-d2ad-41a1-aab4-a8b170df0648"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "54a84626-d5c3-4d7f-bf46-ba1e5a336898",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}