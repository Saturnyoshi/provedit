{
    "id": "436d490e-e3bc-437c-b3f5-710352a130f4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objUICheckbox",
    "eventList": [
        {
            "id": "6fb66dff-73d0-4421-97f4-f8d87a475857",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "436d490e-e3bc-437c-b3f5-710352a130f4"
        },
        {
            "id": "940d761e-2e3d-4c62-8f6f-948cd1d2bbe2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "436d490e-e3bc-437c-b3f5-710352a130f4"
        },
        {
            "id": "1e31e856-04bb-45d8-8aa9-50cd74dc0f8e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 25,
            "eventtype": 7,
            "m_owner": "436d490e-e3bc-437c-b3f5-710352a130f4"
        },
        {
            "id": "456046d1-3ea2-4eb9-9666-d5429988330a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "436d490e-e3bc-437c-b3f5-710352a130f4"
        },
        {
            "id": "a11b89ba-5ed3-4927-aa7d-7b160e04b5e8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "436d490e-e3bc-437c-b3f5-710352a130f4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "f4a9cfe3-3ffd-4ff4-94e8-4f8bd9f4eea8",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}