{
    "id": "53289a3c-ef8d-4e5c-b683-8ef7a66b2bfb",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objCollisionRope",
    "eventList": [
        {
            "id": "e43fc135-6786-4022-b0cd-dc79088c45d6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "53289a3c-ef8d-4e5c-b683-8ef7a66b2bfb"
        }
    ],
    "maskSpriteId": "b2dd7c49-639d-447c-bfe3-e1f7c3fbc8f6",
    "overriddenProperties": null,
    "parentObjectId": "633fe443-8442-4666-8cfa-430f5009d781",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2ba414c6-8b61-4a5c-b992-32a4e5f46cea",
    "visible": false
}