{
    "id": "ab973004-8690-4616-acae-ba549fc0a367",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objTopBar",
    "eventList": [
        {
            "id": "351bfd55-9a54-45b6-abad-1fb108e69f2b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ab973004-8690-4616-acae-ba549fc0a367"
        },
        {
            "id": "2a8bef41-e777-4055-8a3b-7b22fde0b1e5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "ab973004-8690-4616-acae-ba549fc0a367"
        },
        {
            "id": "a9b93c01-5747-439c-8446-038bbf8eee94",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ab973004-8690-4616-acae-ba549fc0a367"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d3a7757f-04a5-4aec-8244-f32fad0e7ee2",
    "visible": true
}