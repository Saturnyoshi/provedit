{
    "id": "b12b4c5a-f8ab-45ee-b1fd-ded2b71fef54",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objCollisionWall",
    "eventList": [
        {
            "id": "b7278b38-15d3-41f1-bcad-2c1159897db2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b12b4c5a-f8ab-45ee-b1fd-ded2b71fef54"
        }
    ],
    "maskSpriteId": "b2dd7c49-639d-447c-bfe3-e1f7c3fbc8f6",
    "overriddenProperties": null,
    "parentObjectId": "be25b882-01d3-4e8b-a49a-99faf016cf94",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "46833096-4252-4a97-a164-44036ac2c349",
    "visible": false
}