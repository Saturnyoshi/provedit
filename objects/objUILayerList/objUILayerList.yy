{
    "id": "2319a136-0da9-4e88-bac3-c9ab42d666ff",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objUILayerList",
    "eventList": [
        {
            "id": "916ba53f-5036-46d7-a298-89562b532f44",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2319a136-0da9-4e88-bac3-c9ab42d666ff"
        },
        {
            "id": "f6565b8e-ec9f-44d4-8ad4-f1f77e27d38b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "2319a136-0da9-4e88-bac3-c9ab42d666ff"
        },
        {
            "id": "26c5d661-97bd-4653-bd57-3c44f136620c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "2319a136-0da9-4e88-bac3-c9ab42d666ff"
        },
        {
            "id": "1966e62c-f545-43f0-a7f8-af68d18db150",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "2319a136-0da9-4e88-bac3-c9ab42d666ff"
        },
        {
            "id": "482af863-de34-4cec-8e33-d2231cd830f2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "2319a136-0da9-4e88-bac3-c9ab42d666ff"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "f4a9cfe3-3ffd-4ff4-94e8-4f8bd9f4eea8",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": false
}