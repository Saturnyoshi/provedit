{
    "id": "739c0e3c-19a7-4e16-8e43-9d33dca2b173",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objObjectMenu",
    "eventList": [
        {
            "id": "54809fb7-7a48-48c7-a7b1-dab721f25576",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 74,
            "eventtype": 8,
            "m_owner": "739c0e3c-19a7-4e16-8e43-9d33dca2b173"
        },
        {
            "id": "1f737c4a-7f8a-4ac1-84d4-47959fad04ee",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "739c0e3c-19a7-4e16-8e43-9d33dca2b173"
        },
        {
            "id": "19ad6fbd-eea1-49bc-9bb2-eff0bc22a038",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "739c0e3c-19a7-4e16-8e43-9d33dca2b173"
        },
        {
            "id": "9fa9fe8d-87f1-4935-8a85-2c8e95cb3b5d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "739c0e3c-19a7-4e16-8e43-9d33dca2b173"
        },
        {
            "id": "2c781537-f623-4562-bd79-5e61b0c951bb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "739c0e3c-19a7-4e16-8e43-9d33dca2b173"
        },
        {
            "id": "f59642d6-0e8a-413e-834c-8bf8355742bc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "739c0e3c-19a7-4e16-8e43-9d33dca2b173"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "54a84626-d5c3-4d7f-bf46-ba1e5a336898",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}