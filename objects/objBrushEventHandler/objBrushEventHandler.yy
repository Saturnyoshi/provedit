{
    "id": "a1ae8bf0-9adc-4940-a2ce-8017d5a8d754",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objBrushEventHandler",
    "eventList": [
        {
            "id": "347b0781-9fec-4478-85e7-9604323f6e58",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a1ae8bf0-9adc-4940-a2ce-8017d5a8d754"
        },
        {
            "id": "6bfa6e6a-8426-4e74-b582-7344f00a14aa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 13,
            "eventtype": 7,
            "m_owner": "a1ae8bf0-9adc-4940-a2ce-8017d5a8d754"
        },
        {
            "id": "8e73849f-5597-4740-bbc6-8b693a575112",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "a1ae8bf0-9adc-4940-a2ce-8017d5a8d754"
        },
        {
            "id": "35b0763d-4136-4b8d-9173-4f7c325fbb23",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "a1ae8bf0-9adc-4940-a2ce-8017d5a8d754"
        },
        {
            "id": "5150f0c1-066b-4ca1-aff8-d6247362a8cc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "a1ae8bf0-9adc-4940-a2ce-8017d5a8d754"
        },
        {
            "id": "2ed3b899-6724-4d8e-a1ee-e7d1cfce921a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "a1ae8bf0-9adc-4940-a2ce-8017d5a8d754"
        },
        {
            "id": "13413dd7-06a4-41ac-959a-306553809958",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 14,
            "eventtype": 7,
            "m_owner": "a1ae8bf0-9adc-4940-a2ce-8017d5a8d754"
        },
        {
            "id": "a790dd68-d46b-4c46-9397-9cfa00724de5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 15,
            "eventtype": 7,
            "m_owner": "a1ae8bf0-9adc-4940-a2ce-8017d5a8d754"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}