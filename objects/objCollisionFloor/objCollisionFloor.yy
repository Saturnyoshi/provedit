{
    "id": "90565ff3-62e6-4689-9c19-9b17c5791629",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objCollisionFloor",
    "eventList": [
        {
            "id": "e34ac779-8c4a-41fb-8330-728f93ab906a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "90565ff3-62e6-4689-9c19-9b17c5791629"
        }
    ],
    "maskSpriteId": "b2dd7c49-639d-447c-bfe3-e1f7c3fbc8f6",
    "overriddenProperties": null,
    "parentObjectId": "be25b882-01d3-4e8b-a49a-99faf016cf94",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "484e9ed8-8b86-43cd-bbd6-5b0b6ae9af22",
    "visible": false
}