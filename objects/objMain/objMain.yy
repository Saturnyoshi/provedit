{
    "id": "17659947-94ec-4d80-8c7a-fbc5a66d1bbe",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objMain",
    "eventList": [
        {
            "id": "6325a80d-32f8-4360-abb2-6a3efd43c734",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 76,
            "eventtype": 8,
            "m_owner": "17659947-94ec-4d80-8c7a-fbc5a66d1bbe"
        },
        {
            "id": "96a6ea04-0025-4f9c-ae44-61f67f0e0db4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "17659947-94ec-4d80-8c7a-fbc5a66d1bbe"
        },
        {
            "id": "c11ceadc-74a1-48ee-9beb-b83b93c93b1f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 73,
            "eventtype": 8,
            "m_owner": "17659947-94ec-4d80-8c7a-fbc5a66d1bbe"
        },
        {
            "id": "2e3ebb59-a8d4-4a3e-9d8f-51fc0d28ad03",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "17659947-94ec-4d80-8c7a-fbc5a66d1bbe"
        },
        {
            "id": "899a5c2b-358e-4a5e-866b-50192610c65b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "17659947-94ec-4d80-8c7a-fbc5a66d1bbe"
        },
        {
            "id": "b83b9cec-a6e7-4681-85fd-cacdef7a02d5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "17659947-94ec-4d80-8c7a-fbc5a66d1bbe"
        },
        {
            "id": "5d1ddc49-9eb9-4fc8-9862-8a8e700b9c44",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 17,
            "eventtype": 5,
            "m_owner": "17659947-94ec-4d80-8c7a-fbc5a66d1bbe"
        },
        {
            "id": "957b39ed-950b-4405-9827-10a75a85a233",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 75,
            "eventtype": 8,
            "m_owner": "17659947-94ec-4d80-8c7a-fbc5a66d1bbe"
        },
        {
            "id": "d4f48672-56b8-4893-b6c2-85d17c34fbf7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 3,
            "eventtype": 7,
            "m_owner": "17659947-94ec-4d80-8c7a-fbc5a66d1bbe"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d3a7757f-04a5-4aec-8244-f32fad0e7ee2",
    "visible": true
}