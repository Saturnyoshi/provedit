{
    "id": "84c37668-bd8d-4799-932b-60802c04468f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objBackground",
    "eventList": [
        {
            "id": "eed89128-6f45-4ef2-96ce-921822b6bb11",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "84c37668-bd8d-4799-932b-60802c04468f"
        },
        {
            "id": "64471ae9-c036-4f04-aa94-17a8b961748d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "84c37668-bd8d-4799-932b-60802c04468f"
        },
        {
            "id": "6f6e7217-62dd-4da5-b68a-1c6d136c6045",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "84c37668-bd8d-4799-932b-60802c04468f"
        },
        {
            "id": "2e5fc147-2766-4b86-99c7-ea2d940c6b43",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "84c37668-bd8d-4799-932b-60802c04468f"
        },
        {
            "id": "64c520af-8fa4-46f9-bac3-fd10e1c04a87",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "84c37668-bd8d-4799-932b-60802c04468f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}