{
    "id": "cfe11747-4b9f-48b3-99f0-1abef05bb736",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objConsole",
    "eventList": [
        {
            "id": "aa2002aa-b45b-482e-9b65-5dc39708ec47",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "cfe11747-4b9f-48b3-99f0-1abef05bb736"
        },
        {
            "id": "093bbab7-83ad-45f6-88e1-08ae04cc2fac",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "cfe11747-4b9f-48b3-99f0-1abef05bb736"
        },
        {
            "id": "9cf11095-5a4e-4148-9912-e7c03684377b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "cfe11747-4b9f-48b3-99f0-1abef05bb736"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d3a7757f-04a5-4aec-8244-f32fad0e7ee2",
    "visible": true
}