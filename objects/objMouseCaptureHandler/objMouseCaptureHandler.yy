{
    "id": "2eb6b596-3dfd-4b96-882d-305379307a71",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objMouseCaptureHandler",
    "eventList": [
        {
            "id": "495d6c0c-bb27-49b6-a86e-46dbe76afae3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2eb6b596-3dfd-4b96-882d-305379307a71"
        },
        {
            "id": "be99c0da-7810-48c4-b4f5-d1e01615a1a4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "2eb6b596-3dfd-4b96-882d-305379307a71"
        },
        {
            "id": "fa791f8a-b2aa-4f8b-a4d9-a2cd1a37ae13",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 75,
            "eventtype": 8,
            "m_owner": "2eb6b596-3dfd-4b96-882d-305379307a71"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d3a7757f-04a5-4aec-8244-f32fad0e7ee2",
    "visible": true
}