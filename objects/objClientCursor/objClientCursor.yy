{
    "id": "eccf573a-a15b-49d3-9c1a-1c6ef3bf76c3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objClientCursor",
    "eventList": [
        {
            "id": "932643a6-0fe0-421f-8987-e2b6cb8cf3ca",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "eccf573a-a15b-49d3-9c1a-1c6ef3bf76c3"
        },
        {
            "id": "ab86335f-dd61-4927-82e9-e979a0fcb601",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "eccf573a-a15b-49d3-9c1a-1c6ef3bf76c3"
        },
        {
            "id": "c9e5a6cd-f1aa-4627-8e9c-0911afcffa9d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "eccf573a-a15b-49d3-9c1a-1c6ef3bf76c3"
        },
        {
            "id": "9c3ddf1c-d5f6-423d-a2c0-081b33a2f981",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "eccf573a-a15b-49d3-9c1a-1c6ef3bf76c3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}