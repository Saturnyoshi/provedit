{
    "id": "46ec0144-95cc-491b-b42d-1db1f89cd1dc",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objPencilEventHandler",
    "eventList": [
        {
            "id": "dbc4bec1-8be9-4722-9eca-e2b33b961105",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "46ec0144-95cc-491b-b42d-1db1f89cd1dc"
        },
        {
            "id": "d73b55d8-c6d6-48ab-a29b-3ed67a220117",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 13,
            "eventtype": 7,
            "m_owner": "46ec0144-95cc-491b-b42d-1db1f89cd1dc"
        },
        {
            "id": "02cffd8c-2bf4-4d2c-9dd3-48a287b92e5e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "46ec0144-95cc-491b-b42d-1db1f89cd1dc"
        },
        {
            "id": "5d7593d9-48fd-4a67-9bad-8c33587eec62",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "46ec0144-95cc-491b-b42d-1db1f89cd1dc"
        },
        {
            "id": "0d23f4e9-d692-42c6-b2e1-5c5e88fa4722",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "46ec0144-95cc-491b-b42d-1db1f89cd1dc"
        },
        {
            "id": "ab5d6610-ebdc-4c09-8e04-c0b3b28ca281",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "46ec0144-95cc-491b-b42d-1db1f89cd1dc"
        },
        {
            "id": "9b70d63a-4710-4704-bd77-d2fe5bf48ba3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "46ec0144-95cc-491b-b42d-1db1f89cd1dc"
        },
        {
            "id": "aae7dae8-f9c5-4d4d-928b-afed9a7015d8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 14,
            "eventtype": 7,
            "m_owner": "46ec0144-95cc-491b-b42d-1db1f89cd1dc"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}