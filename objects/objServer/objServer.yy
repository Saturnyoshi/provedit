{
    "id": "1e685ce1-bae0-4239-be6b-d74ea7858a59",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objServer",
    "eventList": [
        {
            "id": "5ba0f549-b96c-4245-af63-6a7501088322",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 68,
            "eventtype": 7,
            "m_owner": "1e685ce1-bae0-4239-be6b-d74ea7858a59"
        },
        {
            "id": "61e235b8-3e1c-4451-b3ae-dac6fdedf891",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1e685ce1-bae0-4239-be6b-d74ea7858a59"
        },
        {
            "id": "29813576-2e4a-4c16-96c2-49b53bc47dbe",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "1e685ce1-bae0-4239-be6b-d74ea7858a59"
        },
        {
            "id": "8a6135f3-013e-4383-960d-f5f74a2db5e1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "1e685ce1-bae0-4239-be6b-d74ea7858a59"
        },
        {
            "id": "0f91ce6a-ff3d-4cf3-a1a6-9508da6e4f6e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "1e685ce1-bae0-4239-be6b-d74ea7858a59"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d3a7757f-04a5-4aec-8244-f32fad0e7ee2",
    "visible": true
}