{
    "id": "f352628b-4b44-4da7-b9cc-723f84cdea64",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objCollisionLava",
    "eventList": [
        {
            "id": "1b32769a-703e-4d67-8541-1821dbbc2a68",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f352628b-4b44-4da7-b9cc-723f84cdea64"
        }
    ],
    "maskSpriteId": "b2dd7c49-639d-447c-bfe3-e1f7c3fbc8f6",
    "overriddenProperties": null,
    "parentObjectId": "be25b882-01d3-4e8b-a49a-99faf016cf94",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "12b67e59-5940-4ce1-88e4-e142c8583023",
    "visible": false
}