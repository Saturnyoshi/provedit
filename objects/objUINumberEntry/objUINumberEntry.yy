{
    "id": "574c12f8-7286-4895-bebd-386873e549e4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objUINumberEntry",
    "eventList": [
        {
            "id": "a36918fe-a0db-4271-9f25-ae946c354b32",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "574c12f8-7286-4895-bebd-386873e549e4"
        },
        {
            "id": "1db8b3ac-1d62-429c-a349-25a08c414b91",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "574c12f8-7286-4895-bebd-386873e549e4"
        },
        {
            "id": "b0f06e7c-5537-43c4-8ce2-4a286c26bda8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 25,
            "eventtype": 7,
            "m_owner": "574c12f8-7286-4895-bebd-386873e549e4"
        },
        {
            "id": "7debdfa8-4f55-4ab7-9d89-b566e540636b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "574c12f8-7286-4895-bebd-386873e549e4"
        },
        {
            "id": "3efeedfb-5b79-4b25-ba5a-28f3e8858717",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "574c12f8-7286-4895-bebd-386873e549e4"
        },
        {
            "id": "0daaa335-6b8a-4b58-8133-ba7460869d0b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "574c12f8-7286-4895-bebd-386873e549e4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "f4a9cfe3-3ffd-4ff4-94e8-4f8bd9f4eea8",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}