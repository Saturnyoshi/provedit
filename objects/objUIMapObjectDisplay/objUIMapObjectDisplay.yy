{
    "id": "88d05a86-95af-4b29-b66d-1de6e7b622e0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objUIMapObjectDisplay",
    "eventList": [
        {
            "id": "68c5a77a-a4a1-470f-9105-33435a4d7d27",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "88d05a86-95af-4b29-b66d-1de6e7b622e0"
        },
        {
            "id": "f4cc835f-79f1-4045-a7bc-a04d58c542cf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "88d05a86-95af-4b29-b66d-1de6e7b622e0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "f4a9cfe3-3ffd-4ff4-94e8-4f8bd9f4eea8",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}