{
    "id": "94752209-29f5-42ae-955c-55e98de3b893",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objSelectorEventHandler",
    "eventList": [
        {
            "id": "c815b247-5583-4b55-8ada-2036a48baffb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "94752209-29f5-42ae-955c-55e98de3b893"
        },
        {
            "id": "944bba8b-fd3f-4869-9d3c-82651349c9ff",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 13,
            "eventtype": 7,
            "m_owner": "94752209-29f5-42ae-955c-55e98de3b893"
        },
        {
            "id": "87cb42bc-4191-4ee3-a722-4bc308a12621",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "94752209-29f5-42ae-955c-55e98de3b893"
        },
        {
            "id": "e57ca129-b9d6-4bf4-96ef-cd937e8816a4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "94752209-29f5-42ae-955c-55e98de3b893"
        },
        {
            "id": "34ed3450-d331-4e56-8e88-3e96b2ee302d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "94752209-29f5-42ae-955c-55e98de3b893"
        },
        {
            "id": "04929017-ccee-4549-befa-dffb8004c4ce",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "94752209-29f5-42ae-955c-55e98de3b893"
        },
        {
            "id": "fe33a9b9-2204-4c3a-aedf-c0c830b376c1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "94752209-29f5-42ae-955c-55e98de3b893"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}