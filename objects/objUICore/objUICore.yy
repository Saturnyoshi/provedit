{
    "id": "6101734e-0ccc-4f9a-92e8-67bb7bf7cdf9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objUICore",
    "eventList": [
        {
            "id": "a8e82665-4044-48c5-9b89-6adaafb4af24",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6101734e-0ccc-4f9a-92e8-67bb7bf7cdf9"
        },
        {
            "id": "049b50a4-fdef-47cc-af4d-f86a46dc6f13",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "6101734e-0ccc-4f9a-92e8-67bb7bf7cdf9"
        },
        {
            "id": "d9f8884f-a3fa-407b-b4f7-7d5390b09c5f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "6101734e-0ccc-4f9a-92e8-67bb7bf7cdf9"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d3a7757f-04a5-4aec-8244-f32fad0e7ee2",
    "visible": true
}