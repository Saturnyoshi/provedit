{
    "id": "be25b882-01d3-4e8b-a49a-99faf016cf94",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "parCollisionSolid",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "7f75f91a-83b9-4224-96f7-0fb782d9e553",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "fc835c85-edbe-4d08-af84-db3c8637e581",
    "visible": true
}