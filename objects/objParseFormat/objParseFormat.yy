{
    "id": "7395f3a7-bd8a-4be9-b81d-71f189f5744c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objParseFormat",
    "eventList": [
        {
            "id": "0dd7f9ad-8225-4270-95f8-7b9599932b60",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7395f3a7-bd8a-4be9-b81d-71f189f5744c"
        },
        {
            "id": "61328b13-3a75-47f1-be23-6077c4142c54",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "7395f3a7-bd8a-4be9-b81d-71f189f5744c"
        },
        {
            "id": "ab6fe27b-0168-45cc-81c5-c78fc02728d6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "7395f3a7-bd8a-4be9-b81d-71f189f5744c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "efb17bec-9b21-4822-961e-9ed90a7e7867",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}