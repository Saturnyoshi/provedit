{
    "id": "26f9407c-853e-4a23-b5a1-bcb05378b386",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprCheckbox",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 12,
    "bbox_left": 0,
    "bbox_right": 12,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "140c46c9-b361-400b-b784-34e863562d87",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "26f9407c-853e-4a23-b5a1-bcb05378b386",
            "compositeImage": {
                "id": "8e53f7ad-ce78-4327-b107-599980c83dcd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "140c46c9-b361-400b-b784-34e863562d87",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46a6101b-0f6e-4e72-9f2e-d9be91bd971f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "140c46c9-b361-400b-b784-34e863562d87",
                    "LayerId": "c94abbe0-e813-4093-b45e-c261d79dddc6"
                }
            ]
        },
        {
            "id": "01fc33c6-35bc-4ba7-8311-cc4fa6ca777b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "26f9407c-853e-4a23-b5a1-bcb05378b386",
            "compositeImage": {
                "id": "d5143433-8f46-4b40-afdf-7218f3a7a093",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01fc33c6-35bc-4ba7-8311-cc4fa6ca777b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee6d5937-3f1c-4b32-804b-d31d3881102b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01fc33c6-35bc-4ba7-8311-cc4fa6ca777b",
                    "LayerId": "c94abbe0-e813-4093-b45e-c261d79dddc6"
                }
            ]
        },
        {
            "id": "e20e5231-6722-4900-9d8c-2fa575c9dbdb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "26f9407c-853e-4a23-b5a1-bcb05378b386",
            "compositeImage": {
                "id": "a9fef6da-4994-4b4c-b491-bbdc3db8d18b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e20e5231-6722-4900-9d8c-2fa575c9dbdb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "673e13e3-0863-4759-8511-9860ecc92f1f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e20e5231-6722-4900-9d8c-2fa575c9dbdb",
                    "LayerId": "c94abbe0-e813-4093-b45e-c261d79dddc6"
                }
            ]
        },
        {
            "id": "7c35edbe-a962-46dd-b457-d2c1ef5412e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "26f9407c-853e-4a23-b5a1-bcb05378b386",
            "compositeImage": {
                "id": "8eda3824-4908-4310-90e5-8508f7a0eeb8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c35edbe-a962-46dd-b457-d2c1ef5412e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4e21db87-7d59-4cd6-b5ef-72eb5ca17937",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c35edbe-a962-46dd-b457-d2c1ef5412e2",
                    "LayerId": "c94abbe0-e813-4093-b45e-c261d79dddc6"
                }
            ]
        },
        {
            "id": "880c2078-0b7c-4ef0-96de-9658ad689141",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "26f9407c-853e-4a23-b5a1-bcb05378b386",
            "compositeImage": {
                "id": "0a91a544-84f0-4521-94d3-aea30ce70f42",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "880c2078-0b7c-4ef0-96de-9658ad689141",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c75fcfdf-54a4-4b01-a3ff-51b119b6c992",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "880c2078-0b7c-4ef0-96de-9658ad689141",
                    "LayerId": "c94abbe0-e813-4093-b45e-c261d79dddc6"
                }
            ]
        },
        {
            "id": "c48290c4-caca-4848-8430-553b1707a4fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "26f9407c-853e-4a23-b5a1-bcb05378b386",
            "compositeImage": {
                "id": "28338ab9-ab44-4cf5-a8d0-2538ee891553",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c48290c4-caca-4848-8430-553b1707a4fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "896d2a83-d175-4278-9c34-dffb029e27f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c48290c4-caca-4848-8430-553b1707a4fe",
                    "LayerId": "c94abbe0-e813-4093-b45e-c261d79dddc6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 13,
    "layers": [
        {
            "id": "c94abbe0-e813-4093-b45e-c261d79dddc6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "26f9407c-853e-4a23-b5a1-bcb05378b386",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 13,
    "xorig": 0,
    "yorig": 0
}