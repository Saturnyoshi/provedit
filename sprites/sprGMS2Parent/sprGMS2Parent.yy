{
    "id": "fc835c85-edbe-4d08-af84-db3c8637e581",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprGMS2Parent",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 0,
    "bbox_right": 19,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "114161ee-33ca-47ae-b04e-4bcb50973dce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fc835c85-edbe-4d08-af84-db3c8637e581",
            "compositeImage": {
                "id": "275d009c-970a-4610-9140-fe8989056f11",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "114161ee-33ca-47ae-b04e-4bcb50973dce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f1dbcd59-77b1-4c62-b315-0957bb217023",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "114161ee-33ca-47ae-b04e-4bcb50973dce",
                    "LayerId": "621ca69d-83ed-4018-8a5b-186e8bbb3f05"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 20,
    "layers": [
        {
            "id": "621ca69d-83ed-4018-8a5b-186e8bbb3f05",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fc835c85-edbe-4d08-af84-db3c8637e581",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 20,
    "xorig": 0,
    "yorig": 0
}