{
    "id": "7200b93d-d32f-42e5-b3f5-6a7a61f7da2b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprButtonLayer",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6cfc913a-18bf-45c9-86e4-be1a2941f5dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7200b93d-d32f-42e5-b3f5-6a7a61f7da2b",
            "compositeImage": {
                "id": "f8c4abe3-5c7a-4a55-9bb3-2c2974ae84c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6cfc913a-18bf-45c9-86e4-be1a2941f5dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "41630c97-1e6c-4230-abeb-96e063e3af1d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6cfc913a-18bf-45c9-86e4-be1a2941f5dc",
                    "LayerId": "53f26e03-4c09-442b-b439-c51cb3e2c81e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "53f26e03-4c09-442b-b439-c51cb3e2c81e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7200b93d-d32f-42e5-b3f5-6a7a61f7da2b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}