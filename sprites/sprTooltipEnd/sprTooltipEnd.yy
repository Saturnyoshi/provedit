{
    "id": "84be07d1-f192-478b-9ea8-c7bafc5ed428",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprTooltipEnd",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8901949f-8a9f-44c9-a4ae-06bc0e95d4a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "84be07d1-f192-478b-9ea8-c7bafc5ed428",
            "compositeImage": {
                "id": "5324eea7-7943-45f6-9b26-94022ba51ed4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8901949f-8a9f-44c9-a4ae-06bc0e95d4a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1358c338-108e-44e2-976f-00e7a78b5e50",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8901949f-8a9f-44c9-a4ae-06bc0e95d4a2",
                    "LayerId": "0ad8c24f-ad7e-4454-a75f-04bb999bd4c2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 15,
    "layers": [
        {
            "id": "0ad8c24f-ad7e-4454-a75f-04bb999bd4c2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "84be07d1-f192-478b-9ea8-c7bafc5ed428",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}