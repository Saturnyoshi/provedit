{
    "id": "362c390e-4590-4603-9cb7-5288cba9ad98",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprBottomLeftButtonChoice",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 0,
    "bbox_right": 19,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "73e9d8ac-b2b4-4479-9991-35ce8e284816",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "362c390e-4590-4603-9cb7-5288cba9ad98",
            "compositeImage": {
                "id": "1b06b149-8bcc-4a37-ba10-6a09e7cc609d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "73e9d8ac-b2b4-4479-9991-35ce8e284816",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c77ab496-752c-4782-a279-14ba07c40e02",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "73e9d8ac-b2b4-4479-9991-35ce8e284816",
                    "LayerId": "2d248ad2-2e46-4e3d-bdb0-cf57c70846bf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 25,
    "layers": [
        {
            "id": "2d248ad2-2e46-4e3d-bdb0-cf57c70846bf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "362c390e-4590-4603-9cb7-5288cba9ad98",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 20,
    "xorig": 0,
    "yorig": 0
}