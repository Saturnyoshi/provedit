{
    "id": "be896344-5158-4e7e-93ca-fbe346b8b331",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprFormHueSelector",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 255,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "30c1289b-b660-45bb-8a5a-7efcf721f9f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be896344-5158-4e7e-93ca-fbe346b8b331",
            "compositeImage": {
                "id": "67493320-e311-4823-83e1-fdffb46a7e4a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "30c1289b-b660-45bb-8a5a-7efcf721f9f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb3adacf-cc29-4453-9f54-df687b422e51",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "30c1289b-b660-45bb-8a5a-7efcf721f9f1",
                    "LayerId": "7a89a24e-f0b2-4e26-a824-42592ab5d874"
                }
            ]
        },
        {
            "id": "b894406d-67ed-4a4c-b52f-be9b7f64995f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be896344-5158-4e7e-93ca-fbe346b8b331",
            "compositeImage": {
                "id": "e72b053c-44ec-4256-8452-718e18e28233",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b894406d-67ed-4a4c-b52f-be9b7f64995f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ab13370-354d-47a9-b523-f2238cec1bef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b894406d-67ed-4a4c-b52f-be9b7f64995f",
                    "LayerId": "7a89a24e-f0b2-4e26-a824-42592ab5d874"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1,
    "layers": [
        {
            "id": "7a89a24e-f0b2-4e26-a824-42592ab5d874",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "be896344-5158-4e7e-93ca-fbe346b8b331",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 0,
    "yorig": 0
}