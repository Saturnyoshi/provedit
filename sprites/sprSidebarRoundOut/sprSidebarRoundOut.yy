{
    "id": "133608e4-cc54-4d24-bd91-9ff63f93c04f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprSidebarRoundOut",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2b799df5-a585-439e-b256-5e23444b6c48",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "133608e4-cc54-4d24-bd91-9ff63f93c04f",
            "compositeImage": {
                "id": "dc0681d6-7e03-4bcb-8086-027ce372501f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b799df5-a585-439e-b256-5e23444b6c48",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec7adab0-7c73-40cd-a12f-77351b5e470a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b799df5-a585-439e-b256-5e23444b6c48",
                    "LayerId": "67499e6f-4ff1-4cf5-b200-6fd653152f3d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "67499e6f-4ff1-4cf5-b200-6fd653152f3d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "133608e4-cc54-4d24-bd91-9ff63f93c04f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}