{
    "id": "d0994106-416c-4c3b-9476-2fcdd48516fc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprCollisionBoss",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2d4582f8-8f32-43c6-98b1-c9945c242184",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d0994106-416c-4c3b-9476-2fcdd48516fc",
            "compositeImage": {
                "id": "a1508515-f72c-48b4-9e14-b9ff0956039f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d4582f8-8f32-43c6-98b1-c9945c242184",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e4e53bf7-578e-46c2-a5c0-3903c200f67d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d4582f8-8f32-43c6-98b1-c9945c242184",
                    "LayerId": "125a33a3-2829-4122-9905-d8a6caf2a69f"
                }
            ]
        },
        {
            "id": "def8b49b-de80-4b52-a542-9a58ba75976b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d0994106-416c-4c3b-9476-2fcdd48516fc",
            "compositeImage": {
                "id": "2294c31b-9255-4615-8f58-681888bd31c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "def8b49b-de80-4b52-a542-9a58ba75976b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "612d7d98-c65a-4362-a182-3033e36a41b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "def8b49b-de80-4b52-a542-9a58ba75976b",
                    "LayerId": "125a33a3-2829-4122-9905-d8a6caf2a69f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "125a33a3-2829-4122-9905-d8a6caf2a69f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d0994106-416c-4c3b-9476-2fcdd48516fc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}