{
    "id": "c7be625a-75e2-4f74-95d7-ae4245b68bed",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprFormDropdownList",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 0,
    "bbox_right": 11,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1d27ffa9-3591-4751-8c72-a6e632cbe9c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c7be625a-75e2-4f74-95d7-ae4245b68bed",
            "compositeImage": {
                "id": "5601e70a-9460-47be-827b-7f8039ebaf9a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d27ffa9-3591-4751-8c72-a6e632cbe9c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c36844a-0deb-48cf-92b4-134b2350a9a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d27ffa9-3591-4751-8c72-a6e632cbe9c1",
                    "LayerId": "c8d0b132-993e-43bf-825b-f8ea73e6ef44"
                }
            ]
        },
        {
            "id": "ae47ffb3-dbc0-4fc7-9e52-bc192dce9577",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c7be625a-75e2-4f74-95d7-ae4245b68bed",
            "compositeImage": {
                "id": "8bf00847-7f3e-4c25-91d1-882fcb71b712",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae47ffb3-dbc0-4fc7-9e52-bc192dce9577",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "04287a6f-5da2-40f6-8d1e-51c080eed935",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae47ffb3-dbc0-4fc7-9e52-bc192dce9577",
                    "LayerId": "c8d0b132-993e-43bf-825b-f8ea73e6ef44"
                }
            ]
        },
        {
            "id": "03c33a86-59ea-4877-9532-c450b89bac7e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c7be625a-75e2-4f74-95d7-ae4245b68bed",
            "compositeImage": {
                "id": "7f57e85c-a20c-4b9f-b7ca-2afc80b0d905",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03c33a86-59ea-4877-9532-c450b89bac7e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e7ed2104-0f36-42e6-acda-164a1ed8c451",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03c33a86-59ea-4877-9532-c450b89bac7e",
                    "LayerId": "c8d0b132-993e-43bf-825b-f8ea73e6ef44"
                }
            ]
        },
        {
            "id": "88cf5c7b-c621-434b-bbd8-81c42f97645b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c7be625a-75e2-4f74-95d7-ae4245b68bed",
            "compositeImage": {
                "id": "72a783d0-727a-464c-aca7-3fdb02330c2d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88cf5c7b-c621-434b-bbd8-81c42f97645b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "24e5b78c-7909-4f98-9672-eeec40f60be5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88cf5c7b-c621-434b-bbd8-81c42f97645b",
                    "LayerId": "c8d0b132-993e-43bf-825b-f8ea73e6ef44"
                }
            ]
        },
        {
            "id": "334688e3-a4d1-4305-8c8d-c51c170ff3f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c7be625a-75e2-4f74-95d7-ae4245b68bed",
            "compositeImage": {
                "id": "f8e21db6-1d03-4d1f-8123-79d23a144208",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "334688e3-a4d1-4305-8c8d-c51c170ff3f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "51118dcc-dcc4-44a5-8fdf-4b60c581ce67",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "334688e3-a4d1-4305-8c8d-c51c170ff3f4",
                    "LayerId": "c8d0b132-993e-43bf-825b-f8ea73e6ef44"
                }
            ]
        },
        {
            "id": "7da781d8-6289-472b-91f3-a563d9c3bea7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c7be625a-75e2-4f74-95d7-ae4245b68bed",
            "compositeImage": {
                "id": "b3f18c82-a90d-465e-945a-d4bf36ccba20",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7da781d8-6289-472b-91f3-a563d9c3bea7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0ee07b8e-0687-4862-9b15-b5097e0a141f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7da781d8-6289-472b-91f3-a563d9c3bea7",
                    "LayerId": "c8d0b132-993e-43bf-825b-f8ea73e6ef44"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 15,
    "layers": [
        {
            "id": "c8d0b132-993e-43bf-825b-f8ea73e6ef44",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c7be625a-75e2-4f74-95d7-ae4245b68bed",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 12,
    "xorig": 0,
    "yorig": 0
}