{
    "id": "d70982f1-d68b-4bc7-a47d-530e379bcc54",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprWindowMiddle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b2832376-7f5f-4687-8d6d-f179321fde3f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d70982f1-d68b-4bc7-a47d-530e379bcc54",
            "compositeImage": {
                "id": "16bb9017-2845-4e53-9492-d5b41ed5be11",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b2832376-7f5f-4687-8d6d-f179321fde3f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "753b6208-09c3-4599-a68d-64216cf6cca8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b2832376-7f5f-4687-8d6d-f179321fde3f",
                    "LayerId": "bd484706-affd-48fc-b036-be18cdf6eeea"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "bd484706-affd-48fc-b036-be18cdf6eeea",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d70982f1-d68b-4bc7-a47d-530e379bcc54",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}