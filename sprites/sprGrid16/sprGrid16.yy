{
    "id": "d711b322-9ba3-43c2-97b2-db4d6fb3badd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprGrid16",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 255,
    "bbox_left": 0,
    "bbox_right": 255,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2409ad0e-4140-45ab-a5e8-ec9c01958cc2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d711b322-9ba3-43c2-97b2-db4d6fb3badd",
            "compositeImage": {
                "id": "718e3a55-0404-44bb-9093-9b6e884a05cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2409ad0e-4140-45ab-a5e8-ec9c01958cc2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e58fb08b-9bfc-4ee1-b23b-09fbc0e2309e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2409ad0e-4140-45ab-a5e8-ec9c01958cc2",
                    "LayerId": "c75d35fc-0f7c-4f49-9f81-dedf5bfa1786"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "c75d35fc-0f7c-4f49-9f81-dedf5bfa1786",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d711b322-9ba3-43c2-97b2-db4d6fb3badd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 389,
    "yorig": -166
}