{
    "id": "484e9ed8-8b86-43cd-bbd6-5b0b6ae9af22",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprCollisionFloor",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6a5c378d-6232-4c18-a4cd-ca42177d3245",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "484e9ed8-8b86-43cd-bbd6-5b0b6ae9af22",
            "compositeImage": {
                "id": "c527276f-f94e-4251-9262-121ab68b7b72",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a5c378d-6232-4c18-a4cd-ca42177d3245",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a6c5ab12-4c57-4243-9ff2-1c8bebf340c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a5c378d-6232-4c18-a4cd-ca42177d3245",
                    "LayerId": "a669229f-1e90-49fd-bc4f-ba4004005f11"
                }
            ]
        },
        {
            "id": "bf00fd2f-1cbe-424a-abbd-762e39d8482e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "484e9ed8-8b86-43cd-bbd6-5b0b6ae9af22",
            "compositeImage": {
                "id": "063384b1-0876-4359-8ac4-b5f44c73910f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf00fd2f-1cbe-424a-abbd-762e39d8482e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "365b377f-a6a7-4c04-b8cc-9880843993de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf00fd2f-1cbe-424a-abbd-762e39d8482e",
                    "LayerId": "a669229f-1e90-49fd-bc4f-ba4004005f11"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "a669229f-1e90-49fd-bc4f-ba4004005f11",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "484e9ed8-8b86-43cd-bbd6-5b0b6ae9af22",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}