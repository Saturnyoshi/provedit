{
    "id": "d1b07300-4d99-4f32-aead-cf5a3cd23f12",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprFormButton",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 16,
    "bbox_left": 0,
    "bbox_right": 11,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a2c1adde-d90e-4bdf-bd39-8110c60b321c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d1b07300-4d99-4f32-aead-cf5a3cd23f12",
            "compositeImage": {
                "id": "efcbdf05-2733-4dd4-a549-9c06909d9818",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a2c1adde-d90e-4bdf-bd39-8110c60b321c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d72b87c7-ad13-4606-8e42-0ccfaf7b36ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a2c1adde-d90e-4bdf-bd39-8110c60b321c",
                    "LayerId": "41ee50d0-0bd3-4228-b416-14f1958a341c"
                }
            ]
        },
        {
            "id": "fcdf341f-fa88-4774-8a74-17ff6719dd81",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d1b07300-4d99-4f32-aead-cf5a3cd23f12",
            "compositeImage": {
                "id": "aa3341d7-a2c8-4a7f-b1bd-83e9ff40d82c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fcdf341f-fa88-4774-8a74-17ff6719dd81",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1dfccdfd-5223-4bae-9f9c-94cd55c1a02e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fcdf341f-fa88-4774-8a74-17ff6719dd81",
                    "LayerId": "41ee50d0-0bd3-4228-b416-14f1958a341c"
                }
            ]
        },
        {
            "id": "40ee5930-207a-4cb5-a4c2-49c65ce5de22",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d1b07300-4d99-4f32-aead-cf5a3cd23f12",
            "compositeImage": {
                "id": "4f40efdb-5f5b-4369-bd96-3e82d8f5d0db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "40ee5930-207a-4cb5-a4c2-49c65ce5de22",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9950d8fd-ca2f-4b7e-b65e-2e9c214e01e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "40ee5930-207a-4cb5-a4c2-49c65ce5de22",
                    "LayerId": "41ee50d0-0bd3-4228-b416-14f1958a341c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 17,
    "layers": [
        {
            "id": "41ee50d0-0bd3-4228-b416-14f1958a341c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d1b07300-4d99-4f32-aead-cf5a3cd23f12",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 3,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 12,
    "xorig": 0,
    "yorig": 8
}