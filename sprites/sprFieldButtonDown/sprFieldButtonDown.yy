{
    "id": "4979100f-2b04-4ebe-a673-22e19fd5b10f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprFieldButtonDown",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 0,
    "bbox_right": 12,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ba797ba6-cacc-4d58-bbb7-b6993a64b5b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4979100f-2b04-4ebe-a673-22e19fd5b10f",
            "compositeImage": {
                "id": "3545ad2e-2668-4f42-b5ac-b2af6ed40030",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba797ba6-cacc-4d58-bbb7-b6993a64b5b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7cb8a3aa-3e03-4c32-b83f-518516e971ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba797ba6-cacc-4d58-bbb7-b6993a64b5b4",
                    "LayerId": "c1a77da3-ebd4-4012-abbd-c87304d51657"
                }
            ]
        },
        {
            "id": "2a65d9fa-cbc2-4d18-985f-751d231df835",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4979100f-2b04-4ebe-a673-22e19fd5b10f",
            "compositeImage": {
                "id": "97578b0e-d5ec-4007-8f37-5146481372ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a65d9fa-cbc2-4d18-985f-751d231df835",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cfa7a9a1-fd6c-419c-86bb-914a336f6431",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a65d9fa-cbc2-4d18-985f-751d231df835",
                    "LayerId": "c1a77da3-ebd4-4012-abbd-c87304d51657"
                }
            ]
        },
        {
            "id": "34167364-df08-4fc2-a731-857af72acc1d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4979100f-2b04-4ebe-a673-22e19fd5b10f",
            "compositeImage": {
                "id": "afe2934a-967d-4594-a5d2-e386e9f2697b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "34167364-df08-4fc2-a731-857af72acc1d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48279b44-b64e-4954-9a08-f3bdd96685be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34167364-df08-4fc2-a731-857af72acc1d",
                    "LayerId": "c1a77da3-ebd4-4012-abbd-c87304d51657"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 15,
    "layers": [
        {
            "id": "c1a77da3-ebd4-4012-abbd-c87304d51657",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4979100f-2b04-4ebe-a673-22e19fd5b10f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 13,
    "xorig": 0,
    "yorig": 0
}