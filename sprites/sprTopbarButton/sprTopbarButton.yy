{
    "id": "b7300a97-c75c-4af2-86ba-93d4b712f4b1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprTopbarButton",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 12,
    "bbox_left": 0,
    "bbox_right": 32,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1cb8abb6-c0c4-4354-a35c-e24695b3f885",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b7300a97-c75c-4af2-86ba-93d4b712f4b1",
            "compositeImage": {
                "id": "c8c24990-dc29-4a19-a2b0-80a5b9b13b24",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1cb8abb6-c0c4-4354-a35c-e24695b3f885",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bfe9bcc2-aaab-4261-afa2-9e1c09800aba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1cb8abb6-c0c4-4354-a35c-e24695b3f885",
                    "LayerId": "fb88f88d-8cc2-4f18-b9ab-952274e93f80"
                }
            ]
        },
        {
            "id": "1e97d1e8-4ab3-40cc-889d-15cce5de04b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b7300a97-c75c-4af2-86ba-93d4b712f4b1",
            "compositeImage": {
                "id": "0c81ab9d-0a47-4932-b42b-6e3c00e944dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e97d1e8-4ab3-40cc-889d-15cce5de04b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b4fd7946-703e-4c00-bb93-c8cbaf7bd8fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e97d1e8-4ab3-40cc-889d-15cce5de04b0",
                    "LayerId": "fb88f88d-8cc2-4f18-b9ab-952274e93f80"
                }
            ]
        },
        {
            "id": "9ee96a82-490b-4d75-944d-fa6636c3a91a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b7300a97-c75c-4af2-86ba-93d4b712f4b1",
            "compositeImage": {
                "id": "af8c5a51-2ddf-4ab8-9f02-7e13edcda670",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ee96a82-490b-4d75-944d-fa6636c3a91a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "948e7d77-15b1-463b-8122-03f82768275d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ee96a82-490b-4d75-944d-fa6636c3a91a",
                    "LayerId": "fb88f88d-8cc2-4f18-b9ab-952274e93f80"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 13,
    "layers": [
        {
            "id": "fb88f88d-8cc2-4f18-b9ab-952274e93f80",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b7300a97-c75c-4af2-86ba-93d4b712f4b1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 33,
    "xorig": 0,
    "yorig": 0
}