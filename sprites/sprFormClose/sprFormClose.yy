{
    "id": "418b47b1-7f00-47c5-aff0-c1f3059bde0a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprFormClose",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b88b6202-bbf7-4dc0-aa73-750dc0cf9029",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "418b47b1-7f00-47c5-aff0-c1f3059bde0a",
            "compositeImage": {
                "id": "559733c2-f591-4fde-85ef-e57242d7819e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b88b6202-bbf7-4dc0-aa73-750dc0cf9029",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f86efcb6-e971-43ff-ac99-e10d9304d203",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b88b6202-bbf7-4dc0-aa73-750dc0cf9029",
                    "LayerId": "8099bc97-590f-4ea5-9fd3-62e89c41d81f"
                }
            ]
        },
        {
            "id": "1ddbb9f2-bba7-4cf2-b801-f1d932f78da1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "418b47b1-7f00-47c5-aff0-c1f3059bde0a",
            "compositeImage": {
                "id": "21a560ed-d6d9-4f73-b0b4-5ca9746f1f6c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ddbb9f2-bba7-4cf2-b801-f1d932f78da1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e7be4e8-305b-4f5c-ba04-37a77a138a41",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ddbb9f2-bba7-4cf2-b801-f1d932f78da1",
                    "LayerId": "8099bc97-590f-4ea5-9fd3-62e89c41d81f"
                }
            ]
        },
        {
            "id": "a4677d13-b9ec-4cf8-94fa-051cf2952995",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "418b47b1-7f00-47c5-aff0-c1f3059bde0a",
            "compositeImage": {
                "id": "d5a34c12-d8cc-4b7a-82d2-2aa1603c7183",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a4677d13-b9ec-4cf8-94fa-051cf2952995",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46209aef-42c9-41f0-a203-826ce5eb133b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4677d13-b9ec-4cf8-94fa-051cf2952995",
                    "LayerId": "8099bc97-590f-4ea5-9fd3-62e89c41d81f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "8099bc97-590f-4ea5-9fd3-62e89c41d81f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "418b47b1-7f00-47c5-aff0-c1f3059bde0a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}