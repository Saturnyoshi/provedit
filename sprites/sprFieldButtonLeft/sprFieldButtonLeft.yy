{
    "id": "cfc7f7d8-c94f-4085-aa11-3f3be4f34b5f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprFieldButtonLeft",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d2eadaba-3847-433d-8527-54763e59f4ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cfc7f7d8-c94f-4085-aa11-3f3be4f34b5f",
            "compositeImage": {
                "id": "0169f1d0-4435-4634-9288-34c1ab7e57a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2eadaba-3847-433d-8527-54763e59f4ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "083343e1-72ab-4b91-9353-65db5ae49c21",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2eadaba-3847-433d-8527-54763e59f4ea",
                    "LayerId": "ceefb4f6-04b7-4f80-918c-14c72a97a79e"
                }
            ]
        },
        {
            "id": "a4f389d8-6b2d-4c0b-b975-081cee9663cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cfc7f7d8-c94f-4085-aa11-3f3be4f34b5f",
            "compositeImage": {
                "id": "a7ad927d-99fe-478c-964e-86f6e60d1acb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a4f389d8-6b2d-4c0b-b975-081cee9663cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ddd9cee9-8c71-424f-8854-8bfbbd01f991",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4f389d8-6b2d-4c0b-b975-081cee9663cd",
                    "LayerId": "ceefb4f6-04b7-4f80-918c-14c72a97a79e"
                }
            ]
        },
        {
            "id": "3290c455-5ba7-4cdd-bc18-9d34eaf123ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cfc7f7d8-c94f-4085-aa11-3f3be4f34b5f",
            "compositeImage": {
                "id": "5a27d651-8ffc-4aba-b732-c446de81da30",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3290c455-5ba7-4cdd-bc18-9d34eaf123ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d34b96ee-edca-48e3-b075-2f0cc6cc98af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3290c455-5ba7-4cdd-bc18-9d34eaf123ef",
                    "LayerId": "ceefb4f6-04b7-4f80-918c-14c72a97a79e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 15,
    "layers": [
        {
            "id": "ceefb4f6-04b7-4f80-918c-14c72a97a79e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cfc7f7d8-c94f-4085-aa11-3f3be4f34b5f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 8,
    "yorig": 0
}