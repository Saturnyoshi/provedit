{
    "id": "4c054dc6-1fde-43aa-9f9c-6b9d8afe78de",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprSnapCornersBackdrop",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 71,
    "bbox_left": 0,
    "bbox_right": 17,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bfe94629-ed60-4c79-847d-028de4f26293",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4c054dc6-1fde-43aa-9f9c-6b9d8afe78de",
            "compositeImage": {
                "id": "6b0998ca-de9b-4e70-91c4-5e9172b97efd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bfe94629-ed60-4c79-847d-028de4f26293",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f15c5776-dbab-4d2c-9af2-d2a8fb354cb2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bfe94629-ed60-4c79-847d-028de4f26293",
                    "LayerId": "71db349d-2a2e-41a8-8f00-4af4a87f32cd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 72,
    "layers": [
        {
            "id": "71db349d-2a2e-41a8-8f00-4af4a87f32cd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4c054dc6-1fde-43aa-9f9c-6b9d8afe78de",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 18,
    "xorig": 9,
    "yorig": 72
}