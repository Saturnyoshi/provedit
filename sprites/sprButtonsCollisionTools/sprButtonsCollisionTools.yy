{
    "id": "199fbbe9-5f51-4aa0-9860-29d83e74a742",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprButtonsCollisionTools",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "25105190-df1b-4769-806c-cfdff8e62c31",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "199fbbe9-5f51-4aa0-9860-29d83e74a742",
            "compositeImage": {
                "id": "a4083ab7-0704-4e9f-9141-19c634328280",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25105190-df1b-4769-806c-cfdff8e62c31",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d36ca478-9da9-4d08-8ae4-42d2bc23e352",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25105190-df1b-4769-806c-cfdff8e62c31",
                    "LayerId": "8897d539-0ae6-473f-9be3-c7f161656fd7"
                }
            ]
        },
        {
            "id": "24154f0c-c10e-4b63-915e-8417c3f497dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "199fbbe9-5f51-4aa0-9860-29d83e74a742",
            "compositeImage": {
                "id": "2948593d-bf85-4d31-927d-740eccdbf6da",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24154f0c-c10e-4b63-915e-8417c3f497dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc3040cb-8e52-4dbd-98be-81bbbc1c4138",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24154f0c-c10e-4b63-915e-8417c3f497dc",
                    "LayerId": "8897d539-0ae6-473f-9be3-c7f161656fd7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "8897d539-0ae6-473f-9be3-c7f161656fd7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "199fbbe9-5f51-4aa0-9860-29d83e74a742",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}