{
    "id": "092c744c-95d3-4dea-a100-f3ca66ecd530",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprGMS2Unused",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 0,
    "bbox_right": 19,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ecbe3d90-be0d-4fc6-b309-d180d9750c32",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "092c744c-95d3-4dea-a100-f3ca66ecd530",
            "compositeImage": {
                "id": "995f699b-59a2-4b8e-a196-b9af0225e4c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ecbe3d90-be0d-4fc6-b309-d180d9750c32",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dcda594c-8788-4b26-88d7-198ebf27d2b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ecbe3d90-be0d-4fc6-b309-d180d9750c32",
                    "LayerId": "f7a6dd4d-9878-4af2-9a06-85c3d35fbb6c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 20,
    "layers": [
        {
            "id": "f7a6dd4d-9878-4af2-9a06-85c3d35fbb6c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "092c744c-95d3-4dea-a100-f3ca66ecd530",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 20,
    "xorig": 0,
    "yorig": 0
}