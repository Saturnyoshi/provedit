{
    "id": "4931f102-704a-4137-ba0a-1f3258b76d8d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprWindowSliceTop",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 0,
    "bbox_right": 467,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5e599e05-8d14-4c1d-b257-5e3c4c021492",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4931f102-704a-4137-ba0a-1f3258b76d8d",
            "compositeImage": {
                "id": "f1d28e99-16fd-4a5c-8ffe-2399b5e15f33",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e599e05-8d14-4c1d-b257-5e3c4c021492",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7633ef24-ef04-48b5-a3ff-54002b633b25",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e599e05-8d14-4c1d-b257-5e3c4c021492",
                    "LayerId": "a264fa88-e4ce-403f-9fdf-0bd1451a38a4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 30,
    "layers": [
        {
            "id": "a264fa88-e4ce-403f-9fdf-0bd1451a38a4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4931f102-704a-4137-ba0a-1f3258b76d8d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 468,
    "xorig": 0,
    "yorig": 0
}