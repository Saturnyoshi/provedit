{
    "id": "33872d7e-c415-40a6-a8b2-1172ac9ab1f8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprButtonsObjectTools",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "606e58e2-dfa3-484a-8649-4ba73c5a6750",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "33872d7e-c415-40a6-a8b2-1172ac9ab1f8",
            "compositeImage": {
                "id": "14dcbfb9-2ef6-49b7-9320-e8cffe5f4af6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "606e58e2-dfa3-484a-8649-4ba73c5a6750",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "327c0082-2897-4963-99b1-0d2dc2a468ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "606e58e2-dfa3-484a-8649-4ba73c5a6750",
                    "LayerId": "ee135ae0-7227-4e0f-8a0e-327143b42958"
                }
            ]
        },
        {
            "id": "df1b1ccb-f91b-412d-96b3-52747e2d3168",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "33872d7e-c415-40a6-a8b2-1172ac9ab1f8",
            "compositeImage": {
                "id": "decdca51-5655-4aae-93af-04dccde81b4f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df1b1ccb-f91b-412d-96b3-52747e2d3168",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "17bf867b-20dc-431a-a041-50304469535e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df1b1ccb-f91b-412d-96b3-52747e2d3168",
                    "LayerId": "ee135ae0-7227-4e0f-8a0e-327143b42958"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "ee135ae0-7227-4e0f-8a0e-327143b42958",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "33872d7e-c415-40a6-a8b2-1172ac9ab1f8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}