{
    "id": "b44619f2-695e-4c52-b301-690386cecad8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprFormDropdownButton",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 16,
    "bbox_left": 0,
    "bbox_right": 16,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "55c821ca-b01f-4483-b982-bf4394bbf988",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b44619f2-695e-4c52-b301-690386cecad8",
            "compositeImage": {
                "id": "5bd724d2-720a-43a3-9bb8-d22dde05d223",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "55c821ca-b01f-4483-b982-bf4394bbf988",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d65dbdef-ee53-4374-a8ea-9b7563b8094c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "55c821ca-b01f-4483-b982-bf4394bbf988",
                    "LayerId": "03908221-461e-4009-b81a-2e059a25f702"
                }
            ]
        },
        {
            "id": "396ef633-2505-4725-b4b5-04461e86342b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b44619f2-695e-4c52-b301-690386cecad8",
            "compositeImage": {
                "id": "1aad0b3d-8fd7-4ed2-a2ed-dedc87c56a9a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "396ef633-2505-4725-b4b5-04461e86342b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f4dea3c5-5f6b-4da4-8c8f-1173ff78a2b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "396ef633-2505-4725-b4b5-04461e86342b",
                    "LayerId": "03908221-461e-4009-b81a-2e059a25f702"
                }
            ]
        },
        {
            "id": "a113784f-0d8b-49d1-bf21-3416be4c6bf5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b44619f2-695e-4c52-b301-690386cecad8",
            "compositeImage": {
                "id": "b8668126-6051-44cb-8f9b-0d00ed55e78e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a113784f-0d8b-49d1-bf21-3416be4c6bf5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9cbe9de6-2b8b-408b-9b29-a4a9e6ca610f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a113784f-0d8b-49d1-bf21-3416be4c6bf5",
                    "LayerId": "03908221-461e-4009-b81a-2e059a25f702"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 17,
    "layers": [
        {
            "id": "03908221-461e-4009-b81a-2e059a25f702",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b44619f2-695e-4c52-b301-690386cecad8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 17,
    "xorig": 0,
    "yorig": 0
}