{
    "id": "565a5776-3427-40c8-87e2-a269ccca2e1b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprMapObject",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "82584c8b-f7fb-458a-a530-9f586f22a2b1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "565a5776-3427-40c8-87e2-a269ccca2e1b",
            "compositeImage": {
                "id": "e9118d11-c15e-49f8-a9a8-03c39954cc98",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82584c8b-f7fb-458a-a530-9f586f22a2b1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5bdc5742-d6b2-422d-af2c-267be6d5f1db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82584c8b-f7fb-458a-a530-9f586f22a2b1",
                    "LayerId": "a78fcac6-33dc-4cab-bfcc-f5b3abb05bf3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "a78fcac6-33dc-4cab-bfcc-f5b3abb05bf3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "565a5776-3427-40c8-87e2-a269ccca2e1b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}