{
    "id": "ca1c0e69-0d01-4e09-884d-fe98e5f0a357",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprButtonsMode",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6d3ac09c-6d24-4f1d-92eb-bbc4e0ffd215",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ca1c0e69-0d01-4e09-884d-fe98e5f0a357",
            "compositeImage": {
                "id": "f57d2192-8a85-4d8f-af39-39cd3b10c420",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d3ac09c-6d24-4f1d-92eb-bbc4e0ffd215",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "09447a90-4300-4f4a-a304-a224850f6336",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d3ac09c-6d24-4f1d-92eb-bbc4e0ffd215",
                    "LayerId": "e9c86be4-1a3b-4aba-9594-f91273b4ae64"
                }
            ]
        },
        {
            "id": "5a6ee794-252d-4078-8d5d-878dbe0325ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ca1c0e69-0d01-4e09-884d-fe98e5f0a357",
            "compositeImage": {
                "id": "3e0e4ba0-0aa7-4882-a3ea-8a654a34fd24",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a6ee794-252d-4078-8d5d-878dbe0325ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de291531-8a90-42e3-93fb-474f06181dab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a6ee794-252d-4078-8d5d-878dbe0325ba",
                    "LayerId": "e9c86be4-1a3b-4aba-9594-f91273b4ae64"
                }
            ]
        },
        {
            "id": "439e727b-3811-4381-b35d-d39d6d036ccc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ca1c0e69-0d01-4e09-884d-fe98e5f0a357",
            "compositeImage": {
                "id": "96de67b3-ffef-4546-b15e-8b38fb0892ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "439e727b-3811-4381-b35d-d39d6d036ccc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f567871-c744-4cb5-af5b-b7d8447c2ff0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "439e727b-3811-4381-b35d-d39d6d036ccc",
                    "LayerId": "e9c86be4-1a3b-4aba-9594-f91273b4ae64"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "e9c86be4-1a3b-4aba-9594-f91273b4ae64",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ca1c0e69-0d01-4e09-884d-fe98e5f0a357",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}