{
    "id": "80be3922-d992-41e5-8013-e8d11c6f4f97",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprFormTab",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 0,
    "bbox_right": 73,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "80da2c79-7904-42b2-98d4-85821131cc85",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "80be3922-d992-41e5-8013-e8d11c6f4f97",
            "compositeImage": {
                "id": "1f77778e-c350-48e9-a304-de04987ef647",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "80da2c79-7904-42b2-98d4-85821131cc85",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa2b1b71-d024-4701-ae0e-0f48e3fb682f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "80da2c79-7904-42b2-98d4-85821131cc85",
                    "LayerId": "8f23b5e0-8106-41cf-96c8-856ca68130f2"
                }
            ]
        },
        {
            "id": "34c1ae2f-9804-43ac-a5a0-76d8103a5ecf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "80be3922-d992-41e5-8013-e8d11c6f4f97",
            "compositeImage": {
                "id": "c6f8fdf4-ea46-48b7-aa05-6fead3c9ddb0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "34c1ae2f-9804-43ac-a5a0-76d8103a5ecf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b48771f-d0c6-4ba2-84e6-f44fd4d19f0e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34c1ae2f-9804-43ac-a5a0-76d8103a5ecf",
                    "LayerId": "8f23b5e0-8106-41cf-96c8-856ca68130f2"
                }
            ]
        },
        {
            "id": "f984b025-6a96-4bf1-81ee-56a50a204666",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "80be3922-d992-41e5-8013-e8d11c6f4f97",
            "compositeImage": {
                "id": "ee2165d5-3f05-4e81-9b14-5e816c907e17",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f984b025-6a96-4bf1-81ee-56a50a204666",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd221fbf-7f88-47ca-9304-e8534125ace0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f984b025-6a96-4bf1-81ee-56a50a204666",
                    "LayerId": "8f23b5e0-8106-41cf-96c8-856ca68130f2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 20,
    "layers": [
        {
            "id": "8f23b5e0-8106-41cf-96c8-856ca68130f2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "80be3922-d992-41e5-8013-e8d11c6f4f97",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 3,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 74,
    "xorig": 0,
    "yorig": 10
}