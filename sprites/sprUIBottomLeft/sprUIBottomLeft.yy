{
    "id": "e2d4ed52-9f49-4cfa-a58b-cf88cd9951b2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprUIBottomLeft",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 35,
    "bbox_left": 0,
    "bbox_right": 131,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "eaacb905-69cf-4695-a9cd-450d02f23d3e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e2d4ed52-9f49-4cfa-a58b-cf88cd9951b2",
            "compositeImage": {
                "id": "a0437998-77f1-4f04-8a5b-ef9b3ecc1f81",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eaacb905-69cf-4695-a9cd-450d02f23d3e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e071509f-1b4f-4239-8886-14f8bfb5bddd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eaacb905-69cf-4695-a9cd-450d02f23d3e",
                    "LayerId": "366ad482-28a7-4b70-85da-6ab4378628dc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 36,
    "layers": [
        {
            "id": "366ad482-28a7-4b70-85da-6ab4378628dc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e2d4ed52-9f49-4cfa-a58b-cf88cd9951b2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 169,
    "xorig": 0,
    "yorig": 36
}