{
    "id": "cd389a9d-7581-46e3-a209-202cdd0c78d4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprFormDropdown",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 16,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a417a5c2-1df7-41a6-9f86-b49c38d972ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd389a9d-7581-46e3-a209-202cdd0c78d4",
            "compositeImage": {
                "id": "48f8e0c0-0b62-40af-bcab-173d46ade46c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a417a5c2-1df7-41a6-9f86-b49c38d972ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1c1d2a10-9aa2-48d2-8896-8e810df98ce3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a417a5c2-1df7-41a6-9f86-b49c38d972ec",
                    "LayerId": "b46f3874-d55e-43ce-a2d2-10fcf1f2470c"
                }
            ]
        },
        {
            "id": "0b7cd1cc-1b02-4259-b167-23050f352468",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd389a9d-7581-46e3-a209-202cdd0c78d4",
            "compositeImage": {
                "id": "5d720a44-828f-479c-afb5-69b923863bdb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b7cd1cc-1b02-4259-b167-23050f352468",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a1e11f5-2423-46b0-bc83-939d69bfc5db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b7cd1cc-1b02-4259-b167-23050f352468",
                    "LayerId": "b46f3874-d55e-43ce-a2d2-10fcf1f2470c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 17,
    "layers": [
        {
            "id": "b46f3874-d55e-43ce-a2d2-10fcf1f2470c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cd389a9d-7581-46e3-a209-202cdd0c78d4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}