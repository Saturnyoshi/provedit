{
    "id": "6635f5e0-8ed6-4c35-9a9d-53b4b08cf185",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprSnapCornersButtonHover",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 13,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e3658c74-e20b-4acc-a4e3-7b5b704c74a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6635f5e0-8ed6-4c35-9a9d-53b4b08cf185",
            "compositeImage": {
                "id": "b7080653-c020-478f-9450-237da912728b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3658c74-e20b-4acc-a4e3-7b5b704c74a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25e97b96-26bd-45c4-a2ab-8cc1789432f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3658c74-e20b-4acc-a4e3-7b5b704c74a5",
                    "LayerId": "27db0945-f2c3-48c3-b087-41f816037c27"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "27db0945-f2c3-48c3-b087-41f816037c27",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6635f5e0-8ed6-4c35-9a9d-53b4b08cf185",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 14,
    "xorig": 0,
    "yorig": 0
}