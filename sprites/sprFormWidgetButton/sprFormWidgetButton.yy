{
    "id": "e60dec73-385a-4a03-ac9b-60b740092c84",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprFormWidgetButton",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 0,
    "bbox_right": 13,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2d255cf5-11ac-437d-9d97-708da38d5db6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e60dec73-385a-4a03-ac9b-60b740092c84",
            "compositeImage": {
                "id": "8449432a-8b9f-4191-91ab-310614175586",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d255cf5-11ac-437d-9d97-708da38d5db6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b367675d-4df2-4e74-a8ba-af45c65ea700",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d255cf5-11ac-437d-9d97-708da38d5db6",
                    "LayerId": "417017a7-304a-446a-ba88-a96246c303d8"
                }
            ]
        },
        {
            "id": "e4593ac7-0029-4587-b8fc-4a34b0ce6c9b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e60dec73-385a-4a03-ac9b-60b740092c84",
            "compositeImage": {
                "id": "e2d56668-5087-415d-b3a2-8b2993ab7b51",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e4593ac7-0029-4587-b8fc-4a34b0ce6c9b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "88d067ff-d2d3-461a-a655-c33b4a8fd7aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e4593ac7-0029-4587-b8fc-4a34b0ce6c9b",
                    "LayerId": "417017a7-304a-446a-ba88-a96246c303d8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 14,
    "layers": [
        {
            "id": "417017a7-304a-446a-ba88-a96246c303d8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e60dec73-385a-4a03-ac9b-60b740092c84",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 14,
    "xorig": 0,
    "yorig": 0
}