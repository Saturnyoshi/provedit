{
    "id": "d7680b7c-7219-4629-ae63-a5a817764afc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprCursor",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 5,
    "bbox_right": 30,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1aaec5a8-03e7-4161-89f8-68fc5a32d341",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7680b7c-7219-4629-ae63-a5a817764afc",
            "compositeImage": {
                "id": "670f9e6e-d971-4f64-9c47-8430e2fe2db7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1aaec5a8-03e7-4161-89f8-68fc5a32d341",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f71c53a4-ad95-4cdb-b7fc-89259d04201e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1aaec5a8-03e7-4161-89f8-68fc5a32d341",
                    "LayerId": "5339481f-cc87-4e79-81a1-22e5edcc3cd8"
                }
            ]
        },
        {
            "id": "55c120c3-b86d-4cfe-9e6d-fe754d19ebe7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7680b7c-7219-4629-ae63-a5a817764afc",
            "compositeImage": {
                "id": "60d65546-01c2-435b-b26e-6a2cb7f7d616",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "55c120c3-b86d-4cfe-9e6d-fe754d19ebe7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "37f37b1c-5ef6-4875-bb79-3949b3bef9fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "55c120c3-b86d-4cfe-9e6d-fe754d19ebe7",
                    "LayerId": "5339481f-cc87-4e79-81a1-22e5edcc3cd8"
                }
            ]
        },
        {
            "id": "bd1c2421-730f-4999-83cd-2dc53348e1c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7680b7c-7219-4629-ae63-a5a817764afc",
            "compositeImage": {
                "id": "2cc4f0d4-b40d-4e29-9715-f5b028b494dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd1c2421-730f-4999-83cd-2dc53348e1c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "10d9dabf-3d26-4106-a998-52e7f6c144aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd1c2421-730f-4999-83cd-2dc53348e1c0",
                    "LayerId": "5339481f-cc87-4e79-81a1-22e5edcc3cd8"
                }
            ]
        },
        {
            "id": "9a5e357c-cc46-495b-bf6a-acbef502706f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7680b7c-7219-4629-ae63-a5a817764afc",
            "compositeImage": {
                "id": "1c3926e5-e12b-47f0-8151-81e8d6ee7faa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a5e357c-cc46-495b-bf6a-acbef502706f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2a34fad4-982b-4d0f-b302-18dedea88e7c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a5e357c-cc46-495b-bf6a-acbef502706f",
                    "LayerId": "5339481f-cc87-4e79-81a1-22e5edcc3cd8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 31,
    "layers": [
        {
            "id": "5339481f-cc87-4e79-81a1-22e5edcc3cd8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d7680b7c-7219-4629-ae63-a5a817764afc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 31,
    "xorig": 15,
    "yorig": 15
}