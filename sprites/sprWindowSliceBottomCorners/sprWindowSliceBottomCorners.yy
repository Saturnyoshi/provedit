{
    "id": "97d34405-55ae-43f0-a4a8-71ad45eee5a8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprWindowSliceBottomCorners",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 5,
    "bbox_left": 0,
    "bbox_right": 5,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0210116b-5a52-4345-877c-2dbac135a561",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "97d34405-55ae-43f0-a4a8-71ad45eee5a8",
            "compositeImage": {
                "id": "fdb962f6-dd31-474c-9af9-aa8fc47e1de8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0210116b-5a52-4345-877c-2dbac135a561",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d1e56820-49e5-42cb-9697-3e5db1c2a4ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0210116b-5a52-4345-877c-2dbac135a561",
                    "LayerId": "9b9cef24-0373-4646-b2af-ce356031e5d5"
                }
            ]
        },
        {
            "id": "4ca47876-a75e-4a73-8c6c-209e1dc34b40",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "97d34405-55ae-43f0-a4a8-71ad45eee5a8",
            "compositeImage": {
                "id": "d6cdcfbf-17fd-4fe4-885f-bb561c271f0c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4ca47876-a75e-4a73-8c6c-209e1dc34b40",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d16a2501-5710-41d5-9ce6-b696398f3a3f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ca47876-a75e-4a73-8c6c-209e1dc34b40",
                    "LayerId": "9b9cef24-0373-4646-b2af-ce356031e5d5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 6,
    "layers": [
        {
            "id": "9b9cef24-0373-4646-b2af-ce356031e5d5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "97d34405-55ae-43f0-a4a8-71ad45eee5a8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 6,
    "xorig": 0,
    "yorig": 0
}