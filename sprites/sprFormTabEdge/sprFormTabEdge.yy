{
    "id": "90829171-ff1a-45e7-a89b-f4cf02963e4a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprFormTabEdge",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 12,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "35e94bf9-d832-4ee0-bb1f-a764b9064140",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "90829171-ff1a-45e7-a89b-f4cf02963e4a",
            "compositeImage": {
                "id": "ae0e179b-3516-462b-8312-9516a1b19d4f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "35e94bf9-d832-4ee0-bb1f-a764b9064140",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a8ac7de-5b0f-4adc-8ce0-c989243f074a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35e94bf9-d832-4ee0-bb1f-a764b9064140",
                    "LayerId": "c0aff27e-379c-4090-bef5-3c81fabbf181"
                }
            ]
        },
        {
            "id": "bc1e7e5c-10c5-4e7c-ab64-56f0929ae654",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "90829171-ff1a-45e7-a89b-f4cf02963e4a",
            "compositeImage": {
                "id": "53865f79-3968-4e28-a430-050089b646ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc1e7e5c-10c5-4e7c-ab64-56f0929ae654",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f6487715-03c2-4a21-81ad-95631f103c78",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc1e7e5c-10c5-4e7c-ab64-56f0929ae654",
                    "LayerId": "c0aff27e-379c-4090-bef5-3c81fabbf181"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1,
    "layers": [
        {
            "id": "c0aff27e-379c-4090-bef5-3c81fabbf181",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "90829171-ff1a-45e7-a89b-f4cf02963e4a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 13,
    "xorig": 0,
    "yorig": 0
}