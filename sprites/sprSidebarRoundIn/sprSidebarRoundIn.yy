{
    "id": "2966dfe2-8987-4bd4-97e9-e1c891992153",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprSidebarRoundIn",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 2,
    "bbox_right": 7,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f64feca2-da54-45fd-9bca-e60460722a7f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2966dfe2-8987-4bd4-97e9-e1c891992153",
            "compositeImage": {
                "id": "542e421c-f8c7-42cb-a532-b25c2e09b50e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f64feca2-da54-45fd-9bca-e60460722a7f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95401bc6-fbbe-4f3e-86a3-0e3aa28d4b03",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f64feca2-da54-45fd-9bca-e60460722a7f",
                    "LayerId": "e8fe77dc-da00-4bfc-8d9c-662d8006aeff"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "e8fe77dc-da00-4bfc-8d9c-662d8006aeff",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2966dfe2-8987-4bd4-97e9-e1c891992153",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 8,
    "yorig": 8
}