{
    "id": "3dbabe5a-6689-4ef0-a64a-2f3e9a8f631c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprButtonShadow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0cbe836e-8d7d-4d31-964d-5e3425f6c250",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3dbabe5a-6689-4ef0-a64a-2f3e9a8f631c",
            "compositeImage": {
                "id": "05405aa1-8c5f-48c5-8fc5-dbd07b5a377b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0cbe836e-8d7d-4d31-964d-5e3425f6c250",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e5fd2246-51f8-4049-8129-2fee7ff82d1d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0cbe836e-8d7d-4d31-964d-5e3425f6c250",
                    "LayerId": "4c1f0483-3c19-4bb0-948b-ba56de768b96"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "4c1f0483-3c19-4bb0-948b-ba56de768b96",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3dbabe5a-6689-4ef0-a64a-2f3e9a8f631c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}