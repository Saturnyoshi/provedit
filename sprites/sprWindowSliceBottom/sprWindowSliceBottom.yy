{
    "id": "ca0fbe1c-36de-40b6-abed-31f6f30bc483",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprWindowSliceBottom",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 5,
    "bbox_left": 0,
    "bbox_right": 1,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c5984de3-4ad4-4af0-95e9-631283e0a5dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ca0fbe1c-36de-40b6-abed-31f6f30bc483",
            "compositeImage": {
                "id": "363ad8ef-75d4-4bc4-bfd8-b0e6153e645c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c5984de3-4ad4-4af0-95e9-631283e0a5dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a4b8445-8ef1-4d71-94ef-001a70f27ef4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5984de3-4ad4-4af0-95e9-631283e0a5dd",
                    "LayerId": "d2957d68-ee9c-4135-bc94-71b9a17c8b1b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 6,
    "layers": [
        {
            "id": "d2957d68-ee9c-4135-bc94-71b9a17c8b1b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ca0fbe1c-36de-40b6-abed-31f6f30bc483",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 2,
    "xorig": 0,
    "yorig": 0
}