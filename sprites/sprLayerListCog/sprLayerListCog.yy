{
    "id": "679cbb4a-d6db-4588-82e4-cb3f3bda1edf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprLayerListCog",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 15,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "53a827b1-e269-42f8-96fd-38e31d7380b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "679cbb4a-d6db-4588-82e4-cb3f3bda1edf",
            "compositeImage": {
                "id": "ea794440-e2ad-4ef0-baad-13f1ffd1a97c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "53a827b1-e269-42f8-96fd-38e31d7380b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "520dc5f1-eda1-4518-b556-5a71465eb2de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "53a827b1-e269-42f8-96fd-38e31d7380b2",
                    "LayerId": "8ee2b6cd-bbcd-4456-861a-0a3ab21259fd"
                }
            ]
        },
        {
            "id": "3bd5b2ff-eb26-4e6f-ab0a-f953b2f72548",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "679cbb4a-d6db-4588-82e4-cb3f3bda1edf",
            "compositeImage": {
                "id": "57fbed98-a99b-420f-b1cd-5458777e4f85",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3bd5b2ff-eb26-4e6f-ab0a-f953b2f72548",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a62946d2-f0ab-4d47-ac03-e660729b7933",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3bd5b2ff-eb26-4e6f-ab0a-f953b2f72548",
                    "LayerId": "8ee2b6cd-bbcd-4456-861a-0a3ab21259fd"
                }
            ]
        },
        {
            "id": "25179286-fae5-4225-b098-5dfc67fde715",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "679cbb4a-d6db-4588-82e4-cb3f3bda1edf",
            "compositeImage": {
                "id": "9d3b9022-e5d5-48f4-8a79-3119b3e5ba4c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25179286-fae5-4225-b098-5dfc67fde715",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f685adb7-8fe1-4a80-8438-57b1cf1f563f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25179286-fae5-4225-b098-5dfc67fde715",
                    "LayerId": "8ee2b6cd-bbcd-4456-861a-0a3ab21259fd"
                }
            ]
        },
        {
            "id": "caca63d6-fb6c-40e9-a3f7-451dd104110d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "679cbb4a-d6db-4588-82e4-cb3f3bda1edf",
            "compositeImage": {
                "id": "bf027d6d-914d-4ba7-ae86-da5e8a32dae5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "caca63d6-fb6c-40e9-a3f7-451dd104110d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f5fb85b-386f-4e2e-9015-b8881add129b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "caca63d6-fb6c-40e9-a3f7-451dd104110d",
                    "LayerId": "8ee2b6cd-bbcd-4456-861a-0a3ab21259fd"
                }
            ]
        },
        {
            "id": "44935eef-4866-4fe8-8be8-b123c2b04d8a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "679cbb4a-d6db-4588-82e4-cb3f3bda1edf",
            "compositeImage": {
                "id": "c62ccdbe-e49e-4d35-8367-06f6c42448c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "44935eef-4866-4fe8-8be8-b123c2b04d8a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "22868e39-9afc-4a1f-aef0-04ba10a97480",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "44935eef-4866-4fe8-8be8-b123c2b04d8a",
                    "LayerId": "8ee2b6cd-bbcd-4456-861a-0a3ab21259fd"
                }
            ]
        },
        {
            "id": "873e67ff-5506-4b9b-b245-cf43fff7f382",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "679cbb4a-d6db-4588-82e4-cb3f3bda1edf",
            "compositeImage": {
                "id": "47db91a3-f33c-4a21-86dd-576a862b1e4d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "873e67ff-5506-4b9b-b245-cf43fff7f382",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c99e5952-17a2-40da-a66f-0299434eeac5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "873e67ff-5506-4b9b-b245-cf43fff7f382",
                    "LayerId": "8ee2b6cd-bbcd-4456-861a-0a3ab21259fd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 17,
    "layers": [
        {
            "id": "8ee2b6cd-bbcd-4456-861a-0a3ab21259fd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "679cbb4a-d6db-4588-82e4-cb3f3bda1edf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 17,
    "xorig": 1,
    "yorig": 1
}