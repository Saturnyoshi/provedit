{
    "id": "71063e05-b008-472f-bb20-3c06336be75a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprButtonChoice",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 35,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b3faa2eb-7a45-4c16-985a-4c2f01012543",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "71063e05-b008-472f-bb20-3c06336be75a",
            "compositeImage": {
                "id": "e6da1ddf-503d-4c4b-93a4-8792dbe36b4a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b3faa2eb-7a45-4c16-985a-4c2f01012543",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "11d90eb4-4811-4e38-8b92-d4d63bdb48b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b3faa2eb-7a45-4c16-985a-4c2f01012543",
                    "LayerId": "4a5a96b8-a097-4d62-b6c1-da8efaf70174"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "4a5a96b8-a097-4d62-b6c1-da8efaf70174",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "71063e05-b008-472f-bb20-3c06336be75a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 36,
    "xorig": 0,
    "yorig": 0
}