{
    "id": "5f6dfce9-3c2e-48bc-aecc-716d693eec79",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprLayerListButton",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 60,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5d2fd8fe-8a04-41ed-8fda-2919e560c38d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f6dfce9-3c2e-48bc-aecc-716d693eec79",
            "compositeImage": {
                "id": "974cb79e-d1c4-4569-bb95-85edc3be591a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d2fd8fe-8a04-41ed-8fda-2919e560c38d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd3b5b04-dc15-4438-bdab-ff8f864c5672",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d2fd8fe-8a04-41ed-8fda-2919e560c38d",
                    "LayerId": "353851f0-a0a0-4b2a-aded-310b5dd464c8"
                }
            ]
        },
        {
            "id": "fd9e4e1e-b7b2-4f57-a02f-9148eb572d91",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f6dfce9-3c2e-48bc-aecc-716d693eec79",
            "compositeImage": {
                "id": "513a39d7-758a-4cc1-8c43-f988b830dbcb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd9e4e1e-b7b2-4f57-a02f-9148eb572d91",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48910a75-5820-480a-a716-c3c06df9fde1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd9e4e1e-b7b2-4f57-a02f-9148eb572d91",
                    "LayerId": "353851f0-a0a0-4b2a-aded-310b5dd464c8"
                }
            ]
        },
        {
            "id": "85c5dab7-b53e-4102-81ea-1e8c0e40423f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f6dfce9-3c2e-48bc-aecc-716d693eec79",
            "compositeImage": {
                "id": "d74ae7b1-0761-42dd-be3e-ff25fa2d6c77",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85c5dab7-b53e-4102-81ea-1e8c0e40423f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "746c7472-e223-4162-939e-347b26290844",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85c5dab7-b53e-4102-81ea-1e8c0e40423f",
                    "LayerId": "353851f0-a0a0-4b2a-aded-310b5dd464c8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "353851f0-a0a0-4b2a-aded-310b5dd464c8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5f6dfce9-3c2e-48bc-aecc-716d693eec79",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 61,
    "xorig": 0,
    "yorig": 2
}