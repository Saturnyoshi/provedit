{
    "id": "ab455d14-5d16-445b-a5ce-04ad2cd26add",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprFieldLeft",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 0,
    "bbox_right": 2,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "757403dc-7d59-4f3e-917c-e7783ffb5919",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ab455d14-5d16-445b-a5ce-04ad2cd26add",
            "compositeImage": {
                "id": "a63c9354-633b-41f6-88f9-7d7a6467b67f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "757403dc-7d59-4f3e-917c-e7783ffb5919",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aaa1b315-1df8-4cc8-ba5a-76d7ed9731dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "757403dc-7d59-4f3e-917c-e7783ffb5919",
                    "LayerId": "31e86eb8-272d-43c1-9491-e5ac65e0df4d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 15,
    "layers": [
        {
            "id": "31e86eb8-272d-43c1-9491-e5ac65e0df4d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ab455d14-5d16-445b-a5ce-04ad2cd26add",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 3,
    "xorig": 0,
    "yorig": 0
}