{
    "id": "d3a7757f-04a5-4aec-8244-f32fad0e7ee2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprGMS2Singleton",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 0,
    "bbox_right": 19,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7de44cb7-5837-446e-af83-c58b10c6a576",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d3a7757f-04a5-4aec-8244-f32fad0e7ee2",
            "compositeImage": {
                "id": "650ef41a-bfac-420d-8cbe-35664ab13092",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7de44cb7-5837-446e-af83-c58b10c6a576",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c6f04c32-3159-4ceb-823b-c2fb1b03d7ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7de44cb7-5837-446e-af83-c58b10c6a576",
                    "LayerId": "2e1f3e5a-ccb2-4ee2-90b5-a587cf3a8963"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 20,
    "layers": [
        {
            "id": "2e1f3e5a-ccb2-4ee2-90b5-a587cf3a8963",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d3a7757f-04a5-4aec-8244-f32fad0e7ee2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 20,
    "xorig": 0,
    "yorig": 0
}