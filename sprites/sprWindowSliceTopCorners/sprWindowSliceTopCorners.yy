{
    "id": "1b14727e-9842-4dfb-96e2-7603ad4f7e6d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprWindowSliceTopCorners",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 0,
    "bbox_right": 4,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e90bbe88-ce91-4ddf-8160-3dabb9daeaac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b14727e-9842-4dfb-96e2-7603ad4f7e6d",
            "compositeImage": {
                "id": "a793fd6f-0c63-4a27-95fc-ca921b8e1f97",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e90bbe88-ce91-4ddf-8160-3dabb9daeaac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b29f939-8dd2-47f1-b146-0a755bac3093",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e90bbe88-ce91-4ddf-8160-3dabb9daeaac",
                    "LayerId": "6e225dd9-7b5b-46e1-9c0e-3ca79f08b5a8"
                }
            ]
        },
        {
            "id": "c288e4df-ffe7-4fb1-8ad9-60e4a2b3367b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b14727e-9842-4dfb-96e2-7603ad4f7e6d",
            "compositeImage": {
                "id": "c47468a7-c3a4-499c-a21a-5a96536aa077",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c288e4df-ffe7-4fb1-8ad9-60e4a2b3367b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3b71653-5198-463a-841a-eaae40a992b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c288e4df-ffe7-4fb1-8ad9-60e4a2b3367b",
                    "LayerId": "6e225dd9-7b5b-46e1-9c0e-3ca79f08b5a8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 30,
    "layers": [
        {
            "id": "6e225dd9-7b5b-46e1-9c0e-3ca79f08b5a8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1b14727e-9842-4dfb-96e2-7603ad4f7e6d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 5,
    "xorig": 0,
    "yorig": 0
}