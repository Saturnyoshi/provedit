{
    "id": "c6af5d9a-9dcc-457b-9c1a-45294ca6de26",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprSidebarHeaders",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 6,
    "bbox_left": 1,
    "bbox_right": 46,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5352705d-a1a8-46ff-a5e2-13335c823c4d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c6af5d9a-9dcc-457b-9c1a-45294ca6de26",
            "compositeImage": {
                "id": "963fb1b7-ebc5-4cbb-9a3b-41b9ef968b8b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5352705d-a1a8-46ff-a5e2-13335c823c4d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2712917f-ecbe-4b52-b170-031a1ed955bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5352705d-a1a8-46ff-a5e2-13335c823c4d",
                    "LayerId": "0f89f898-3e66-42b6-8e16-d80928d1191e"
                }
            ]
        },
        {
            "id": "63fb62a2-1cc5-427c-aed8-983c498e78d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c6af5d9a-9dcc-457b-9c1a-45294ca6de26",
            "compositeImage": {
                "id": "207907ed-cc06-4c73-8c1c-4e0346a73738",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "63fb62a2-1cc5-427c-aed8-983c498e78d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "43e0e5b9-22a5-4a9b-bd1d-c5e337654870",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "63fb62a2-1cc5-427c-aed8-983c498e78d6",
                    "LayerId": "0f89f898-3e66-42b6-8e16-d80928d1191e"
                }
            ]
        },
        {
            "id": "08a51043-1b8f-47b5-87c0-abcc9f536890",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c6af5d9a-9dcc-457b-9c1a-45294ca6de26",
            "compositeImage": {
                "id": "f52aab4b-ace4-4e7f-8e5b-cf526eb76c72",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "08a51043-1b8f-47b5-87c0-abcc9f536890",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "03bfa171-583a-4ab4-b129-c978db834280",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08a51043-1b8f-47b5-87c0-abcc9f536890",
                    "LayerId": "0f89f898-3e66-42b6-8e16-d80928d1191e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "0f89f898-3e66-42b6-8e16-d80928d1191e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c6af5d9a-9dcc-457b-9c1a-45294ca6de26",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 0,
    "yorig": -1
}