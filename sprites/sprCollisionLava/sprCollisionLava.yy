{
    "id": "12b67e59-5940-4ce1-88e4-e142c8583023",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprCollisionLava",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b6d67977-174d-4b8b-8972-7b821b79f91a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "12b67e59-5940-4ce1-88e4-e142c8583023",
            "compositeImage": {
                "id": "d3686957-5902-449a-8f1d-c17f02da6586",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6d67977-174d-4b8b-8972-7b821b79f91a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd719637-0ec5-4023-9ad7-e56021d6688d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6d67977-174d-4b8b-8972-7b821b79f91a",
                    "LayerId": "97ad3b10-c674-46ea-a06f-e3b7057322d9"
                }
            ]
        },
        {
            "id": "6d2e3d33-bf72-47c1-abcd-ff3d02e2ba66",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "12b67e59-5940-4ce1-88e4-e142c8583023",
            "compositeImage": {
                "id": "05180327-9d00-4a15-ae65-342a4ad31809",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d2e3d33-bf72-47c1-abcd-ff3d02e2ba66",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7d4a2f4d-16ae-4bf9-844d-4de6d02e0fcb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d2e3d33-bf72-47c1-abcd-ff3d02e2ba66",
                    "LayerId": "97ad3b10-c674-46ea-a06f-e3b7057322d9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "97ad3b10-c674-46ea-a06f-e3b7057322d9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "12b67e59-5940-4ce1-88e4-e142c8583023",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}