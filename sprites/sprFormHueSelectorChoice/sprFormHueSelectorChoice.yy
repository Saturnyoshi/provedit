{
    "id": "8c745aa4-f5ca-4e21-818d-b4f901acec69",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprFormHueSelectorChoice",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 0,
    "bbox_right": 10,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8fd1cd22-a65c-4e96-b510-dbad48001224",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8c745aa4-f5ca-4e21-818d-b4f901acec69",
            "compositeImage": {
                "id": "0ffbc784-ca79-4b46-9373-8781b464cba9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8fd1cd22-a65c-4e96-b510-dbad48001224",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "23e6230f-cc85-4f6f-b460-5ee7cb46c1fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8fd1cd22-a65c-4e96-b510-dbad48001224",
                    "LayerId": "3cab9eba-b4be-4c0e-a26b-7e90e16fddb5"
                }
            ]
        },
        {
            "id": "1e91138d-6df5-4ac7-bbfc-717ca8df9f38",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8c745aa4-f5ca-4e21-818d-b4f901acec69",
            "compositeImage": {
                "id": "d645dc13-b1b5-4b3b-8f61-060bf6d209a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e91138d-6df5-4ac7-bbfc-717ca8df9f38",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8727cd31-3675-48bc-9916-f3786b7f3938",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e91138d-6df5-4ac7-bbfc-717ca8df9f38",
                    "LayerId": "3cab9eba-b4be-4c0e-a26b-7e90e16fddb5"
                }
            ]
        },
        {
            "id": "1e18c790-a5f8-42de-b996-736faba6bfa2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8c745aa4-f5ca-4e21-818d-b4f901acec69",
            "compositeImage": {
                "id": "2322c3ba-d99e-4510-83b5-c89c26cb9c83",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e18c790-a5f8-42de-b996-736faba6bfa2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c11c84df-de5b-466c-9e8e-1089e372f8be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e18c790-a5f8-42de-b996-736faba6bfa2",
                    "LayerId": "3cab9eba-b4be-4c0e-a26b-7e90e16fddb5"
                }
            ]
        },
        {
            "id": "0caa5206-7710-42e3-82d1-2582fc600294",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8c745aa4-f5ca-4e21-818d-b4f901acec69",
            "compositeImage": {
                "id": "83a8820d-9c0c-4390-855d-477e6ee46888",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0caa5206-7710-42e3-82d1-2582fc600294",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a525d5f2-0d3b-4702-b6ba-a731122d3a39",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0caa5206-7710-42e3-82d1-2582fc600294",
                    "LayerId": "3cab9eba-b4be-4c0e-a26b-7e90e16fddb5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 15,
    "layers": [
        {
            "id": "3cab9eba-b4be-4c0e-a26b-7e90e16fddb5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8c745aa4-f5ca-4e21-818d-b4f901acec69",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 1,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": [
        4278190335,
        4278255615,
        4278255360,
        4294967040,
        4294901760,
        4294902015,
        2164260863,
        4293717228,
        4293059298,
        4292335575,
        4291677645,
        4290230199,
        4287993237,
        4280556782,
        4278252287,
        4283540992,
        4293963264,
        4287770926,
        4287365357,
        4287203721,
        4286414205,
        4285558896,
        4284703587,
        4283782485,
        4281742902,
        4278190080,
        4286158839,
        4286688762,
        4287219453,
        4288280831,
        4288405444,
        4288468131,
        4288465538,
        4291349882,
        4294430829,
        4292454269,
        4291466115,
        4290675079,
        4290743485,
        4290943732,
        4288518390,
        4283395315,
        4283862775,
        4284329979,
        4285068799,
        4285781164,
        4285973884,
        4286101564,
        4290034460,
        4294164224,
        4291529796,
        4289289312,
        4289290373,
        4289291432,
        4289359601,
        4286410226,
        4280556782,
        4280444402,
        4280128760,
        4278252287,
        4282369933,
        4283086137,
        4283540992,
        4288522496,
        4293963264,
        4290540032,
        4289423360,
        4289090560,
        4287770926,
        4287704422,
        4287571858,
        4287365357,
        4284159214,
        4279176094,
        4279058848,
        4278870691,
        4278231211,
        4281367321
    ],
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 11,
    "xorig": 5,
    "yorig": 0
}