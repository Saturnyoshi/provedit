{
    "id": "b76df348-0e3a-4a6a-971b-c3feb64500c3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprFormHueSelectorBackdrop",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 17,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "125b9e83-31a9-4ac9-84e3-e7f707c4b8fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b76df348-0e3a-4a6a-971b-c3feb64500c3",
            "compositeImage": {
                "id": "bca1c1a6-a31c-4e81-8bd5-32e00b4a3e06",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "125b9e83-31a9-4ac9-84e3-e7f707c4b8fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "474dcc09-2692-46ab-ae1b-18fddfda692b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "125b9e83-31a9-4ac9-84e3-e7f707c4b8fe",
                    "LayerId": "0418b5e0-aebd-42e3-9b8a-3c2988ed6664"
                }
            ]
        },
        {
            "id": "f5d27d4d-ea79-488e-b193-f69fa253f071",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b76df348-0e3a-4a6a-971b-c3feb64500c3",
            "compositeImage": {
                "id": "f28b14d4-4144-428b-a8b9-ac62c63b3a73",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f5d27d4d-ea79-488e-b193-f69fa253f071",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca6f406f-a840-4c26-a32a-56804028b172",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5d27d4d-ea79-488e-b193-f69fa253f071",
                    "LayerId": "0418b5e0-aebd-42e3-9b8a-3c2988ed6664"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 18,
    "layers": [
        {
            "id": "0418b5e0-aebd-42e3-9b8a-3c2988ed6664",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b76df348-0e3a-4a6a-971b-c3feb64500c3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1,
    "xorig": 0,
    "yorig": 0
}