{
    "id": "46833096-4252-4a97-a164-44036ac2c349",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprCollisionWall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e1d3aa63-699c-4e47-b31b-95a172aadb3a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "46833096-4252-4a97-a164-44036ac2c349",
            "compositeImage": {
                "id": "01aec4f0-b3d7-4eaa-b7d8-eeaf04a49436",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1d3aa63-699c-4e47-b31b-95a172aadb3a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ffa44939-aede-4b27-80b2-0c97d3786f3b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1d3aa63-699c-4e47-b31b-95a172aadb3a",
                    "LayerId": "78e7cb7e-96c8-47dc-b922-ac35e3b32b99"
                }
            ]
        },
        {
            "id": "14e93dcb-dbf1-47cc-96f4-8a59aaad4ab8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "46833096-4252-4a97-a164-44036ac2c349",
            "compositeImage": {
                "id": "fd971eee-7aee-45fb-8cfe-202edef51646",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "14e93dcb-dbf1-47cc-96f4-8a59aaad4ab8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "64caa6c8-fc31-4d38-9131-c646edbfaefb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14e93dcb-dbf1-47cc-96f4-8a59aaad4ab8",
                    "LayerId": "78e7cb7e-96c8-47dc-b922-ac35e3b32b99"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "78e7cb7e-96c8-47dc-b922-ac35e3b32b99",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "46833096-4252-4a97-a164-44036ac2c349",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}