{
    "id": "49fab134-f9bf-468c-b475-bfbd416807b9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprCollisionSlow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1aa276a7-dca3-4a95-9b0c-03b4c83629aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "49fab134-f9bf-468c-b475-bfbd416807b9",
            "compositeImage": {
                "id": "49ad4e4d-0106-4da5-88c0-aa14d1c30b45",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1aa276a7-dca3-4a95-9b0c-03b4c83629aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1abcdeed-d524-4f6e-a49b-8669038dba27",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1aa276a7-dca3-4a95-9b0c-03b4c83629aa",
                    "LayerId": "7243d837-e3d9-4e23-968f-179ad01103e4"
                }
            ]
        },
        {
            "id": "a4871920-2597-4302-beb2-4715400ebc9c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "49fab134-f9bf-468c-b475-bfbd416807b9",
            "compositeImage": {
                "id": "ea3b345a-2004-49b3-9f79-0ba219ec2fc4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a4871920-2597-4302-beb2-4715400ebc9c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef84409c-4bc0-4b39-ad92-ec895c056fc4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4871920-2597-4302-beb2-4715400ebc9c",
                    "LayerId": "7243d837-e3d9-4e23-968f-179ad01103e4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "7243d837-e3d9-4e23-968f-179ad01103e4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "49fab134-f9bf-468c-b475-bfbd416807b9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}