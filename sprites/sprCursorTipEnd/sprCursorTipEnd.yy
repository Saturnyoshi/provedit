{
    "id": "e801411e-6883-4afc-884f-cbd1c6f7b624",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprCursorTipEnd",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 10,
    "bbox_left": 0,
    "bbox_right": 11,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cffae2fe-0b82-4615-b0d2-8940d8274c61",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e801411e-6883-4afc-884f-cbd1c6f7b624",
            "compositeImage": {
                "id": "6a24def8-ce19-49fd-ae08-70bfad84a882",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cffae2fe-0b82-4615-b0d2-8940d8274c61",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf1cfe55-cc33-4739-a4c2-a949468c6084",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cffae2fe-0b82-4615-b0d2-8940d8274c61",
                    "LayerId": "07ccb36e-4b6c-4598-9949-1f225e5c73d3"
                }
            ]
        },
        {
            "id": "bbdc823b-42b4-45a8-83c1-386d93a853d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e801411e-6883-4afc-884f-cbd1c6f7b624",
            "compositeImage": {
                "id": "3d53a327-45b8-4f51-b6c8-371f94103eaf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bbdc823b-42b4-45a8-83c1-386d93a853d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3576756e-0fc1-4af1-9f1d-2b1027c2cb9e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bbdc823b-42b4-45a8-83c1-386d93a853d1",
                    "LayerId": "07ccb36e-4b6c-4598-9949-1f225e5c73d3"
                }
            ]
        },
        {
            "id": "8dc94b95-9e56-4311-bf6b-879a6c769f67",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e801411e-6883-4afc-884f-cbd1c6f7b624",
            "compositeImage": {
                "id": "49afb15b-1a02-4d0d-92b5-b45138416488",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8dc94b95-9e56-4311-bf6b-879a6c769f67",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "11755352-8192-4b2e-b08d-ccfa3dadd7ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8dc94b95-9e56-4311-bf6b-879a6c769f67",
                    "LayerId": "07ccb36e-4b6c-4598-9949-1f225e5c73d3"
                }
            ]
        },
        {
            "id": "711d971a-8c77-41f9-b420-8cc8a18d73a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e801411e-6883-4afc-884f-cbd1c6f7b624",
            "compositeImage": {
                "id": "4d51b264-9193-4248-823a-9b1e191fbd28",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "711d971a-8c77-41f9-b420-8cc8a18d73a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "db1126c7-c40c-40ef-9628-0e267770ad1b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "711d971a-8c77-41f9-b420-8cc8a18d73a3",
                    "LayerId": "07ccb36e-4b6c-4598-9949-1f225e5c73d3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 15,
    "layers": [
        {
            "id": "07ccb36e-4b6c-4598-9949-1f225e5c73d3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e801411e-6883-4afc-884f-cbd1c6f7b624",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 12,
    "xorig": 8,
    "yorig": 0
}