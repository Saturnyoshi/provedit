{
    "id": "c604b83b-0c61-4777-96ca-0ee62903ad0d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprToolPanelHead",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e3e212fc-f86b-4089-bac7-103ea335d68f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c604b83b-0c61-4777-96ca-0ee62903ad0d",
            "compositeImage": {
                "id": "0da48079-a464-4bf9-b7a1-7a0d5b6c46b5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3e212fc-f86b-4089-bac7-103ea335d68f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ba20087-ec5e-458b-b76a-aa622b756ebf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3e212fc-f86b-4089-bac7-103ea335d68f",
                    "LayerId": "da064b15-c59d-4f32-8867-d707a7877bb7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 28,
    "layers": [
        {
            "id": "da064b15-c59d-4f32-8867-d707a7877bb7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c604b83b-0c61-4777-96ca-0ee62903ad0d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}