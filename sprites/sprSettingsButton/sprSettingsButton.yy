{
    "id": "daefad57-ea8d-4166-916c-a91a753c60ba",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprSettingsButton",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 0,
    "bbox_right": 19,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2dfc1ce3-047a-4ebd-92aa-35ecee3eb83a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "daefad57-ea8d-4166-916c-a91a753c60ba",
            "compositeImage": {
                "id": "ff2c2dc0-1790-4004-a337-25f31fe32434",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2dfc1ce3-047a-4ebd-92aa-35ecee3eb83a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d82bde4c-e4ac-45bd-86da-4bd33e1eb5e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2dfc1ce3-047a-4ebd-92aa-35ecee3eb83a",
                    "LayerId": "c8b45b30-e315-448e-bbac-f7fc7c5ca879"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 23,
    "layers": [
        {
            "id": "c8b45b30-e315-448e-bbac-f7fc7c5ca879",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "daefad57-ea8d-4166-916c-a91a753c60ba",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 20,
    "xorig": 0,
    "yorig": 0
}