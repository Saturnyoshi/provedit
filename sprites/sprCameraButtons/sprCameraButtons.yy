{
    "id": "4f136e8f-5518-4023-a287-1f69cd78d001",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprCameraButtons",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 0,
    "bbox_right": 19,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3e56abcd-6eb6-4714-a491-585f4643840f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f136e8f-5518-4023-a287-1f69cd78d001",
            "compositeImage": {
                "id": "31f7da77-7ace-492a-b4f2-fe4de86f8b75",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e56abcd-6eb6-4714-a491-585f4643840f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3f54cd45-52bd-4996-a065-d4840bc56d10",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e56abcd-6eb6-4714-a491-585f4643840f",
                    "LayerId": "130026a4-7262-4bb0-a734-1144fe27f495"
                }
            ]
        },
        {
            "id": "110cb4d5-142e-4314-a0b4-80bd669253f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f136e8f-5518-4023-a287-1f69cd78d001",
            "compositeImage": {
                "id": "5bbfaa2e-5445-4e0c-ba18-09fe5d3b3347",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "110cb4d5-142e-4314-a0b4-80bd669253f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "451e540d-8115-4a59-9ddd-b66761219aac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "110cb4d5-142e-4314-a0b4-80bd669253f3",
                    "LayerId": "130026a4-7262-4bb0-a734-1144fe27f495"
                }
            ]
        },
        {
            "id": "eba04b17-961d-4b41-b09b-ca053f0c0429",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f136e8f-5518-4023-a287-1f69cd78d001",
            "compositeImage": {
                "id": "2efa78f7-b94a-4712-b7d6-efc50bb0c297",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eba04b17-961d-4b41-b09b-ca053f0c0429",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b87d460-f6f8-4bca-8d02-26f0a7acc361",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eba04b17-961d-4b41-b09b-ca053f0c0429",
                    "LayerId": "130026a4-7262-4bb0-a734-1144fe27f495"
                }
            ]
        },
        {
            "id": "7c231ed1-d1e1-44f3-acad-219f8f2f7bcb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f136e8f-5518-4023-a287-1f69cd78d001",
            "compositeImage": {
                "id": "be4377a2-480c-41a9-95dd-dc09b1931c3c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c231ed1-d1e1-44f3-acad-219f8f2f7bcb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "074d985b-5eb1-4481-a001-ab9f0c08bb92",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c231ed1-d1e1-44f3-acad-219f8f2f7bcb",
                    "LayerId": "130026a4-7262-4bb0-a734-1144fe27f495"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 23,
    "layers": [
        {
            "id": "130026a4-7262-4bb0-a734-1144fe27f495",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4f136e8f-5518-4023-a287-1f69cd78d001",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 20,
    "xorig": 0,
    "yorig": 0
}