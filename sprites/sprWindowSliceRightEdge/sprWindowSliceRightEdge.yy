{
    "id": "7e053c23-bd07-4ca2-be97-a166aa733b43",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprWindowSliceRightEdge",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1,
    "bbox_left": 0,
    "bbox_right": 5,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3dc6bab8-022c-48af-b93d-566536e0499c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7e053c23-bd07-4ca2-be97-a166aa733b43",
            "compositeImage": {
                "id": "f9dcf546-dd84-4d17-9e70-4a0ae3fe9f5d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3dc6bab8-022c-48af-b93d-566536e0499c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a7af116-f98c-4b59-821d-c3796352b4ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3dc6bab8-022c-48af-b93d-566536e0499c",
                    "LayerId": "d167a895-0d92-4e23-b7aa-0ba51fc89e2b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 2,
    "layers": [
        {
            "id": "d167a895-0d92-4e23-b7aa-0ba51fc89e2b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7e053c23-bd07-4ca2-be97-a166aa733b43",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 6,
    "xorig": 0,
    "yorig": 0
}