{
    "id": "e5e8cf70-a5de-4c31-af78-69d01013ee44",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprButtonsTileTools",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f21b6917-9429-46e7-ab17-ac00b329d1d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5e8cf70-a5de-4c31-af78-69d01013ee44",
            "compositeImage": {
                "id": "269e629b-4f9d-49b1-953f-0edc15f6da37",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f21b6917-9429-46e7-ab17-ac00b329d1d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de3136a7-9260-4121-9c8e-fc6976e0d35f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f21b6917-9429-46e7-ab17-ac00b329d1d3",
                    "LayerId": "1d9e987a-820b-4851-bdb4-fa151c49f74f"
                }
            ]
        },
        {
            "id": "7ff8284d-2812-4b79-91f7-7844f3314037",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5e8cf70-a5de-4c31-af78-69d01013ee44",
            "compositeImage": {
                "id": "691a5892-9d3a-4d6c-8378-61f3f93a0497",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ff8284d-2812-4b79-91f7-7844f3314037",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2b378e77-cf50-42a3-b045-af1cad0d718c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ff8284d-2812-4b79-91f7-7844f3314037",
                    "LayerId": "1d9e987a-820b-4851-bdb4-fa151c49f74f"
                }
            ]
        },
        {
            "id": "800881c0-c46e-4aff-a293-2f26a9ae817b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5e8cf70-a5de-4c31-af78-69d01013ee44",
            "compositeImage": {
                "id": "b9505347-5155-497e-937f-1a92df2e9102",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "800881c0-c46e-4aff-a293-2f26a9ae817b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "580a187f-cf64-40f8-80ea-e86a5e6e22d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "800881c0-c46e-4aff-a293-2f26a9ae817b",
                    "LayerId": "1d9e987a-820b-4851-bdb4-fa151c49f74f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "1d9e987a-820b-4851-bdb4-fa151c49f74f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e5e8cf70-a5de-4c31-af78-69d01013ee44",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}