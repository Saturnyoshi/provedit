{
    "id": "3d8fadfe-392b-4bfd-a5c2-cb23def429a2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprSnapCornersButtons",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 0,
    "bbox_right": 13,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7c43874a-98d0-48fa-8843-1e0bb8844101",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3d8fadfe-392b-4bfd-a5c2-cb23def429a2",
            "compositeImage": {
                "id": "a13b2f18-f3cf-4abe-b1f5-cf2fe079d4ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c43874a-98d0-48fa-8843-1e0bb8844101",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c58fa9d-bb88-4d5f-b110-81840ea62f4c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c43874a-98d0-48fa-8843-1e0bb8844101",
                    "LayerId": "bc570a7c-db3a-4a06-bfd2-4c28cb2c0daa"
                }
            ]
        },
        {
            "id": "2f278e46-c9f1-4fc1-a83a-ddb5aa327da3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3d8fadfe-392b-4bfd-a5c2-cb23def429a2",
            "compositeImage": {
                "id": "db5ff997-c823-4377-8206-93e51c7c73ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f278e46-c9f1-4fc1-a83a-ddb5aa327da3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8ec6e093-6c7a-48da-b9ce-e3840d883f0b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f278e46-c9f1-4fc1-a83a-ddb5aa327da3",
                    "LayerId": "bc570a7c-db3a-4a06-bfd2-4c28cb2c0daa"
                }
            ]
        },
        {
            "id": "e7eecbb3-fe37-4997-a3ba-d51ea2516722",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3d8fadfe-392b-4bfd-a5c2-cb23def429a2",
            "compositeImage": {
                "id": "7c0490b2-7972-4995-8a32-24c4605940af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e7eecbb3-fe37-4997-a3ba-d51ea2516722",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9abb468a-b63a-47f6-b7e1-78a41fccdaf5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7eecbb3-fe37-4997-a3ba-d51ea2516722",
                    "LayerId": "bc570a7c-db3a-4a06-bfd2-4c28cb2c0daa"
                }
            ]
        },
        {
            "id": "4be4ef11-1a74-40f5-a4f3-2574451a5d8d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3d8fadfe-392b-4bfd-a5c2-cb23def429a2",
            "compositeImage": {
                "id": "753781e4-4a3f-4a91-a183-68d677d70f91",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4be4ef11-1a74-40f5-a4f3-2574451a5d8d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "962020b6-a148-4ff4-9ff6-050837693dfc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4be4ef11-1a74-40f5-a4f3-2574451a5d8d",
                    "LayerId": "bc570a7c-db3a-4a06-bfd2-4c28cb2c0daa"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 14,
    "layers": [
        {
            "id": "bc570a7c-db3a-4a06-bfd2-4c28cb2c0daa",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3d8fadfe-392b-4bfd-a5c2-cb23def429a2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 14,
    "xorig": 0,
    "yorig": 0
}