{
    "id": "4845339e-2527-48df-8fbc-8f58930054d7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprBottomLeftButtonShadow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 0,
    "bbox_right": 19,
    "bbox_top": 19,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d01ddf59-3ffd-4a8b-936c-03b9ab4761c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4845339e-2527-48df-8fbc-8f58930054d7",
            "compositeImage": {
                "id": "9e8792ba-aff7-48ce-96bc-a7d9ec6ab511",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d01ddf59-3ffd-4a8b-936c-03b9ab4761c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b9ed674-8cb9-411b-af08-3c5064d5f216",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d01ddf59-3ffd-4a8b-936c-03b9ab4761c0",
                    "LayerId": "0dd4636c-fd47-4385-9b61-3bbe3f46fff8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 25,
    "layers": [
        {
            "id": "0dd4636c-fd47-4385-9b61-3bbe3f46fff8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4845339e-2527-48df-8fbc-8f58930054d7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 20,
    "xorig": 0,
    "yorig": 0
}