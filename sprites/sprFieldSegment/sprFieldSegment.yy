{
    "id": "ea66c7d8-d33f-4e3a-8121-4ac9dd55549e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprFieldSegment",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f8d5a6d7-42a2-4845-b396-bdf40a7025cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ea66c7d8-d33f-4e3a-8121-4ac9dd55549e",
            "compositeImage": {
                "id": "1be07b72-5bb3-49b1-a914-926a4020bc64",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f8d5a6d7-42a2-4845-b396-bdf40a7025cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6eda812b-0882-4968-bb77-8d0f5328eeb5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f8d5a6d7-42a2-4845-b396-bdf40a7025cc",
                    "LayerId": "8b03b98c-2e52-47af-bf1f-fdc1adbe0bba"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 15,
    "layers": [
        {
            "id": "8b03b98c-2e52-47af-bf1f-fdc1adbe0bba",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ea66c7d8-d33f-4e3a-8121-4ac9dd55549e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1,
    "xorig": 0,
    "yorig": 0
}