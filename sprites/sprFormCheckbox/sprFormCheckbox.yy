{
    "id": "5b775036-9b0b-4897-8b8d-03a11679755a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprFormCheckbox",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 16,
    "bbox_left": 0,
    "bbox_right": 16,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "49ef9999-4144-4efe-99fd-3842fd0adad1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5b775036-9b0b-4897-8b8d-03a11679755a",
            "compositeImage": {
                "id": "a7d64d6d-d6a8-45ad-9d8f-212efd37a1c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "49ef9999-4144-4efe-99fd-3842fd0adad1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d62eccc-39ec-47d8-94a1-7da4375cb1e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "49ef9999-4144-4efe-99fd-3842fd0adad1",
                    "LayerId": "679e21dc-93c7-4fe2-8fda-ab0b4ed2e010"
                }
            ]
        },
        {
            "id": "9b62e860-f611-4e0d-8333-103df3034c34",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5b775036-9b0b-4897-8b8d-03a11679755a",
            "compositeImage": {
                "id": "e2b9e3a5-0690-4d75-a464-d08faca43c3d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b62e860-f611-4e0d-8333-103df3034c34",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "772e16b3-8652-410d-99b7-eb539750a805",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b62e860-f611-4e0d-8333-103df3034c34",
                    "LayerId": "679e21dc-93c7-4fe2-8fda-ab0b4ed2e010"
                }
            ]
        },
        {
            "id": "40b7dcac-02dd-43be-9827-cdcd7f39cc9b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5b775036-9b0b-4897-8b8d-03a11679755a",
            "compositeImage": {
                "id": "7695060c-7419-4e2b-ae95-b52472ef844d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "40b7dcac-02dd-43be-9827-cdcd7f39cc9b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce05cf49-22f2-4bb1-9f82-b76c54a952bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "40b7dcac-02dd-43be-9827-cdcd7f39cc9b",
                    "LayerId": "679e21dc-93c7-4fe2-8fda-ab0b4ed2e010"
                }
            ]
        },
        {
            "id": "497ac9ff-b37f-449a-b9a0-fe3b5ab62253",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5b775036-9b0b-4897-8b8d-03a11679755a",
            "compositeImage": {
                "id": "5dd53caf-6cfa-4fba-961b-a9bf76063057",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "497ac9ff-b37f-449a-b9a0-fe3b5ab62253",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b5443f42-7ffd-499b-a6a4-5bf6d0bfe249",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "497ac9ff-b37f-449a-b9a0-fe3b5ab62253",
                    "LayerId": "679e21dc-93c7-4fe2-8fda-ab0b4ed2e010"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 17,
    "layers": [
        {
            "id": "679e21dc-93c7-4fe2-8fda-ab0b4ed2e010",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5b775036-9b0b-4897-8b8d-03a11679755a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 17,
    "xorig": 8,
    "yorig": 8
}