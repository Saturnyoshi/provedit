{
    "id": "67b89c0b-04ff-46d4-9a99-0edc11935795",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprFormEntry",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 16,
    "bbox_left": 0,
    "bbox_right": 11,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "250ec97c-3779-4fe1-9034-3a2068618df9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "67b89c0b-04ff-46d4-9a99-0edc11935795",
            "compositeImage": {
                "id": "6a0694bb-70b6-44c9-9193-4cc77318d65c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "250ec97c-3779-4fe1-9034-3a2068618df9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2d7923a-cecf-4e6e-871e-867236a03eef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "250ec97c-3779-4fe1-9034-3a2068618df9",
                    "LayerId": "a1f3ba76-bfc9-4b46-8690-8501e5cddb5c"
                }
            ]
        },
        {
            "id": "e5a35c0b-4afa-4f84-91f2-f639f1803877",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "67b89c0b-04ff-46d4-9a99-0edc11935795",
            "compositeImage": {
                "id": "23e92cb8-9f14-471b-b606-eb3068ccac95",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e5a35c0b-4afa-4f84-91f2-f639f1803877",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "972027d8-1c72-4723-9790-b5214c2c7b07",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e5a35c0b-4afa-4f84-91f2-f639f1803877",
                    "LayerId": "a1f3ba76-bfc9-4b46-8690-8501e5cddb5c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 17,
    "layers": [
        {
            "id": "a1f3ba76-bfc9-4b46-8690-8501e5cddb5c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "67b89c0b-04ff-46d4-9a99-0edc11935795",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 3,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 12,
    "xorig": 0,
    "yorig": 8
}