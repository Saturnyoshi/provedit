{
    "id": "267f8e79-ef75-4c1d-beca-02677be76253",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprFieldButtonRight",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "463de8bb-204d-4c9b-a2a5-0bf3d791ef5b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "267f8e79-ef75-4c1d-beca-02677be76253",
            "compositeImage": {
                "id": "20afaf7c-a8f8-47a4-a17e-44dd2ae4a159",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "463de8bb-204d-4c9b-a2a5-0bf3d791ef5b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2dfbd24d-10c9-4f94-b7b3-197e09f01fb8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "463de8bb-204d-4c9b-a2a5-0bf3d791ef5b",
                    "LayerId": "bd54ffe0-6613-4543-a2a5-5489a00f883c"
                }
            ]
        },
        {
            "id": "cf15280c-63ab-4dd8-9c62-89eb2bcfe77a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "267f8e79-ef75-4c1d-beca-02677be76253",
            "compositeImage": {
                "id": "f90353c5-1744-4cda-ab6f-5b685b032a44",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf15280c-63ab-4dd8-9c62-89eb2bcfe77a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "67028b1a-0c7a-46ac-838c-a783cdc8202a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf15280c-63ab-4dd8-9c62-89eb2bcfe77a",
                    "LayerId": "bd54ffe0-6613-4543-a2a5-5489a00f883c"
                }
            ]
        },
        {
            "id": "22bf0c40-0538-4ead-9bec-ab025dc9ba2e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "267f8e79-ef75-4c1d-beca-02677be76253",
            "compositeImage": {
                "id": "c7d3a68f-d329-4d70-9c53-94790eb0621b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22bf0c40-0538-4ead-9bec-ab025dc9ba2e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f8e4353e-40d6-4bf4-9358-cae34f9c371a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22bf0c40-0538-4ead-9bec-ab025dc9ba2e",
                    "LayerId": "bd54ffe0-6613-4543-a2a5-5489a00f883c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 15,
    "layers": [
        {
            "id": "bd54ffe0-6613-4543-a2a5-5489a00f883c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "267f8e79-ef75-4c1d-beca-02677be76253",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}