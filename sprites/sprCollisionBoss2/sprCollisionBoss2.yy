{
    "id": "6b72689d-1e7c-45df-924f-cc877b8e7ec3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprCollisionBoss2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3ef32bd6-1c7a-49ac-a2d7-1e9bf17dd707",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b72689d-1e7c-45df-924f-cc877b8e7ec3",
            "compositeImage": {
                "id": "2bd17d70-58ff-431e-8cf5-3afc48fb9d39",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3ef32bd6-1c7a-49ac-a2d7-1e9bf17dd707",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "098a5998-24da-4fa4-9eda-aef7534d7ba7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3ef32bd6-1c7a-49ac-a2d7-1e9bf17dd707",
                    "LayerId": "b8e948c9-007e-4a5e-b5c3-c2fb8c4c747f"
                }
            ]
        },
        {
            "id": "fdbca3f0-11d9-4d89-a38d-231cb2ccc8fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b72689d-1e7c-45df-924f-cc877b8e7ec3",
            "compositeImage": {
                "id": "a0a8b4d9-b75f-448f-9f2b-6232c11455fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fdbca3f0-11d9-4d89-a38d-231cb2ccc8fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f80d23f1-b455-4bae-95da-3418837550f3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fdbca3f0-11d9-4d89-a38d-231cb2ccc8fc",
                    "LayerId": "b8e948c9-007e-4a5e-b5c3-c2fb8c4c747f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "b8e948c9-007e-4a5e-b5c3-c2fb8c4c747f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6b72689d-1e7c-45df-924f-cc877b8e7ec3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}