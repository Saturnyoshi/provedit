{
    "id": "54a990db-1bb1-42ce-b02e-b1242e605632",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprTileRegionCorner",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 5,
    "bbox_left": 0,
    "bbox_right": 5,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "df595634-f47a-4a97-943f-0affa196557c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54a990db-1bb1-42ce-b02e-b1242e605632",
            "compositeImage": {
                "id": "8158cfe1-fd92-48fc-8834-20efa60a88a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df595634-f47a-4a97-943f-0affa196557c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bfc7371b-bb79-4104-ae3c-63caf7d44114",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df595634-f47a-4a97-943f-0affa196557c",
                    "LayerId": "a3192956-edd9-4feb-960e-7ff15fedc674"
                }
            ]
        },
        {
            "id": "b46bb7e2-2ef3-46d4-a96d-56f723ee6f76",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54a990db-1bb1-42ce-b02e-b1242e605632",
            "compositeImage": {
                "id": "a66615aa-35cb-4ade-bb89-ef61e831af44",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b46bb7e2-2ef3-46d4-a96d-56f723ee6f76",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f8ccb894-d110-45e0-a7ff-94102cf5c316",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b46bb7e2-2ef3-46d4-a96d-56f723ee6f76",
                    "LayerId": "a3192956-edd9-4feb-960e-7ff15fedc674"
                }
            ]
        },
        {
            "id": "fd85723d-f506-412e-aff4-52fed85a75c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54a990db-1bb1-42ce-b02e-b1242e605632",
            "compositeImage": {
                "id": "e96f3cda-4100-41f5-b9bc-b32725e71d7b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd85723d-f506-412e-aff4-52fed85a75c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7de33b72-7ec2-4eb6-953d-f9c25d36a932",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd85723d-f506-412e-aff4-52fed85a75c7",
                    "LayerId": "a3192956-edd9-4feb-960e-7ff15fedc674"
                }
            ]
        },
        {
            "id": "f55ecc2f-b4b0-426d-81c4-4f4f2415d992",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54a990db-1bb1-42ce-b02e-b1242e605632",
            "compositeImage": {
                "id": "f50e8020-4fe3-4c62-b51b-31a1aa17a0e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f55ecc2f-b4b0-426d-81c4-4f4f2415d992",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c4e14d79-0180-4261-a9c1-535b8934dc9a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f55ecc2f-b4b0-426d-81c4-4f4f2415d992",
                    "LayerId": "a3192956-edd9-4feb-960e-7ff15fedc674"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 7,
    "layers": [
        {
            "id": "a3192956-edd9-4feb-960e-7ff15fedc674",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "54a990db-1bb1-42ce-b02e-b1242e605632",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 7,
    "xorig": 3,
    "yorig": 3
}