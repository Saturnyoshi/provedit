{
    "id": "d12817f7-0959-4789-8e5d-dee2909cbf0b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprLevelResizeHandle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 16,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f0eaca6f-bce7-4fc0-a792-889f10ab2cc2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d12817f7-0959-4789-8e5d-dee2909cbf0b",
            "compositeImage": {
                "id": "58cbff1e-bb5b-49f2-b21c-3d3fdb04142d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0eaca6f-bce7-4fc0-a792-889f10ab2cc2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "abc0bc8a-a84e-4ff9-bf14-c8201941acd0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0eaca6f-bce7-4fc0-a792-889f10ab2cc2",
                    "LayerId": "d8984dee-097f-4fe7-9539-a5e82a62c735"
                }
            ]
        },
        {
            "id": "fae00cf6-94db-4001-86ee-0afae809d3e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d12817f7-0959-4789-8e5d-dee2909cbf0b",
            "compositeImage": {
                "id": "649fd77b-9aec-475c-b097-13f5e78e5e2e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fae00cf6-94db-4001-86ee-0afae809d3e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ab4dc14-cd91-448c-8133-a15aa980424d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fae00cf6-94db-4001-86ee-0afae809d3e1",
                    "LayerId": "d8984dee-097f-4fe7-9539-a5e82a62c735"
                }
            ]
        },
        {
            "id": "4cf83b2a-09c6-471e-a76c-ba26e566205d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d12817f7-0959-4789-8e5d-dee2909cbf0b",
            "compositeImage": {
                "id": "cef811ed-c2ca-471f-9a48-06aa81977d50",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4cf83b2a-09c6-471e-a76c-ba26e566205d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a0cb9fa2-d0a9-4b4d-89ab-f08510ae1814",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4cf83b2a-09c6-471e-a76c-ba26e566205d",
                    "LayerId": "d8984dee-097f-4fe7-9539-a5e82a62c735"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 17,
    "layers": [
        {
            "id": "d8984dee-097f-4fe7-9539-a5e82a62c735",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d12817f7-0959-4789-8e5d-dee2909cbf0b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 17,
    "xorig": 8,
    "yorig": 8
}