{
    "id": "b2dd7c49-639d-447c-bfe3-e1f7c3fbc8f6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprColMask",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "542149e7-5e83-415d-98eb-61c98a7a2736",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b2dd7c49-639d-447c-bfe3-e1f7c3fbc8f6",
            "compositeImage": {
                "id": "af37c3b4-7487-49a8-8035-d4f80feffbb1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "542149e7-5e83-415d-98eb-61c98a7a2736",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1c1b1513-a73a-4be6-9385-f721f4203a52",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "542149e7-5e83-415d-98eb-61c98a7a2736",
                    "LayerId": "c6cf37d6-780b-4cf3-8ced-b67a66821825"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "c6cf37d6-780b-4cf3-8ced-b67a66821825",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b2dd7c49-639d-447c-bfe3-e1f7c3fbc8f6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}