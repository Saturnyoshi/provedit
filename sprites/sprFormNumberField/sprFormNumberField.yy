{
    "id": "d7f5ec25-b58e-43b2-9267-2b35d2df0303",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprFormNumberField",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 16,
    "bbox_left": 0,
    "bbox_right": 12,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8db84aec-19fd-4e22-8770-906e6e83d5c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7f5ec25-b58e-43b2-9267-2b35d2df0303",
            "compositeImage": {
                "id": "eb01a1bb-9819-4813-9e33-d78519527ee3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8db84aec-19fd-4e22-8770-906e6e83d5c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1bc75456-ecfe-4561-9271-3f07020e6c3d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8db84aec-19fd-4e22-8770-906e6e83d5c3",
                    "LayerId": "639cd00f-db8d-4913-95da-765723ff1129"
                }
            ]
        },
        {
            "id": "00877181-83d7-4607-b5b8-3081183745f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7f5ec25-b58e-43b2-9267-2b35d2df0303",
            "compositeImage": {
                "id": "38321aef-6482-4d47-84ad-8bcce2c610f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00877181-83d7-4607-b5b8-3081183745f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ce8ce10-5b61-4306-b22c-5e113d2617ec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00877181-83d7-4607-b5b8-3081183745f9",
                    "LayerId": "639cd00f-db8d-4913-95da-765723ff1129"
                }
            ]
        },
        {
            "id": "f18323e4-e538-4669-98a2-142aafcf46f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7f5ec25-b58e-43b2-9267-2b35d2df0303",
            "compositeImage": {
                "id": "a0b05657-e792-4c2d-ad4d-738d540c94bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f18323e4-e538-4669-98a2-142aafcf46f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1312e14-7231-45ae-8029-6d3ebe8be6fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f18323e4-e538-4669-98a2-142aafcf46f5",
                    "LayerId": "639cd00f-db8d-4913-95da-765723ff1129"
                }
            ]
        },
        {
            "id": "23446f37-e966-41cb-8a37-bc66c8c00e68",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7f5ec25-b58e-43b2-9267-2b35d2df0303",
            "compositeImage": {
                "id": "c97a17cf-4fe6-46fa-b0df-17c40ef03f3e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "23446f37-e966-41cb-8a37-bc66c8c00e68",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "560665f1-2e69-47a5-85f4-bf236a1588ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "23446f37-e966-41cb-8a37-bc66c8c00e68",
                    "LayerId": "639cd00f-db8d-4913-95da-765723ff1129"
                }
            ]
        },
        {
            "id": "ebf9f1ac-4801-4b6c-a875-ea7eb2d58d64",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7f5ec25-b58e-43b2-9267-2b35d2df0303",
            "compositeImage": {
                "id": "6a357098-3533-40dd-aae8-f17d204636f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ebf9f1ac-4801-4b6c-a875-ea7eb2d58d64",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb72e4a5-c51a-44a1-ab23-4f15fb4e9868",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ebf9f1ac-4801-4b6c-a875-ea7eb2d58d64",
                    "LayerId": "639cd00f-db8d-4913-95da-765723ff1129"
                }
            ]
        },
        {
            "id": "51f1a52f-f4b4-411d-a45b-f9447df4a27a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7f5ec25-b58e-43b2-9267-2b35d2df0303",
            "compositeImage": {
                "id": "faaee9ec-c8ad-457d-85c8-76027f1b20ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51f1a52f-f4b4-411d-a45b-f9447df4a27a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "efff7ccb-d6b4-4e27-b4cd-77e68c857b32",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51f1a52f-f4b4-411d-a45b-f9447df4a27a",
                    "LayerId": "639cd00f-db8d-4913-95da-765723ff1129"
                }
            ]
        },
        {
            "id": "afd95a6b-ca1b-43ae-9326-ef6d5aa6277f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7f5ec25-b58e-43b2-9267-2b35d2df0303",
            "compositeImage": {
                "id": "b23f1998-e0c8-4122-b567-1e40115249f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "afd95a6b-ca1b-43ae-9326-ef6d5aa6277f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b2b1519-277b-4303-9239-913c2ad1c4e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "afd95a6b-ca1b-43ae-9326-ef6d5aa6277f",
                    "LayerId": "639cd00f-db8d-4913-95da-765723ff1129"
                }
            ]
        },
        {
            "id": "6436fd30-afd0-456b-a6e2-da347e8f076e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7f5ec25-b58e-43b2-9267-2b35d2df0303",
            "compositeImage": {
                "id": "dd3a9efc-0ee8-45aa-b97a-e435b45cd4bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6436fd30-afd0-456b-a6e2-da347e8f076e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d0844b24-6f4a-4307-9d8c-30005a0836ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6436fd30-afd0-456b-a6e2-da347e8f076e",
                    "LayerId": "639cd00f-db8d-4913-95da-765723ff1129"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 17,
    "layers": [
        {
            "id": "639cd00f-db8d-4913-95da-765723ff1129",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d7f5ec25-b58e-43b2-9267-2b35d2df0303",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 13,
    "xorig": 0,
    "yorig": 0
}