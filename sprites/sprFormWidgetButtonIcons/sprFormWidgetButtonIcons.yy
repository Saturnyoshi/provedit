{
    "id": "e470a6ff-670f-48fd-80b1-ecbd23666f7a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprFormWidgetButtonIcons",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 9,
    "bbox_left": 3,
    "bbox_right": 9,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b0fefc16-b435-41d0-80c3-959535bbaa7c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e470a6ff-670f-48fd-80b1-ecbd23666f7a",
            "compositeImage": {
                "id": "8dccbf40-ae90-48b2-85d3-ea3d5c567caf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b0fefc16-b435-41d0-80c3-959535bbaa7c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "69554575-e781-4f75-8ce7-448e0a5030bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b0fefc16-b435-41d0-80c3-959535bbaa7c",
                    "LayerId": "ef0c7888-7357-4a24-ae80-edaf9871908c"
                }
            ]
        },
        {
            "id": "1df4beb4-00d0-460e-bbaf-2705745b1b52",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e470a6ff-670f-48fd-80b1-ecbd23666f7a",
            "compositeImage": {
                "id": "24a2fb31-7ea4-4b95-846e-a2cf0c66727e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1df4beb4-00d0-460e-bbaf-2705745b1b52",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f27e0377-9b27-4ee4-8bd6-c7fa7dc18211",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1df4beb4-00d0-460e-bbaf-2705745b1b52",
                    "LayerId": "ef0c7888-7357-4a24-ae80-edaf9871908c"
                }
            ]
        },
        {
            "id": "647c8088-ecfd-4b99-9e1b-fd3a1caa054a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e470a6ff-670f-48fd-80b1-ecbd23666f7a",
            "compositeImage": {
                "id": "30bc2959-c56d-4416-992f-787b499dd897",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "647c8088-ecfd-4b99-9e1b-fd3a1caa054a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c914bd6-fc5f-4c91-b107-7572c1af4ffe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "647c8088-ecfd-4b99-9e1b-fd3a1caa054a",
                    "LayerId": "ef0c7888-7357-4a24-ae80-edaf9871908c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 14,
    "layers": [
        {
            "id": "ef0c7888-7357-4a24-ae80-edaf9871908c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e470a6ff-670f-48fd-80b1-ecbd23666f7a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 14,
    "xorig": 0,
    "yorig": 0
}