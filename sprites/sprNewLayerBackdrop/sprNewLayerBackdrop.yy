{
    "id": "b7eb0787-2441-4b25-ac77-bb76481231fc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprNewLayerBackdrop",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 279,
    "bbox_left": 0,
    "bbox_right": 196,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0729b4e4-90d7-4999-b502-3a18c75ef54c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b7eb0787-2441-4b25-ac77-bb76481231fc",
            "compositeImage": {
                "id": "9ac6d606-a516-4d65-a77e-28fb0d7e79ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0729b4e4-90d7-4999-b502-3a18c75ef54c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8456e5ab-cae5-4c02-90e8-f81f74b20a95",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0729b4e4-90d7-4999-b502-3a18c75ef54c",
                    "LayerId": "192a516c-cffd-4938-8161-3038e7cb591e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 280,
    "layers": [
        {
            "id": "192a516c-cffd-4938-8161-3038e7cb591e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b7eb0787-2441-4b25-ac77-bb76481231fc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 197,
    "xorig": 0,
    "yorig": 0
}