{
    "id": "49d3190c-243a-4aa8-b258-86f40e8187a3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprLogo",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 53,
    "bbox_left": 8,
    "bbox_right": 119,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "760a8252-ffc8-46e7-8ded-a8f2c55f7af7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "49d3190c-243a-4aa8-b258-86f40e8187a3",
            "compositeImage": {
                "id": "f60d9aa6-1f0e-4083-949e-4f97c16e38ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "760a8252-ffc8-46e7-8ded-a8f2c55f7af7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "041d5670-2f15-4ca5-983a-de70e5d86cb3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "760a8252-ffc8-46e7-8ded-a8f2c55f7af7",
                    "LayerId": "289111d7-5470-4ab8-a165-6ba3b844fe24"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "289111d7-5470-4ab8-a165-6ba3b844fe24",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "49d3190c-243a-4aa8-b258-86f40e8187a3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 32
}