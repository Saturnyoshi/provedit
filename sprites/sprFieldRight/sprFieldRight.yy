{
    "id": "a438d673-2b08-4780-b006-d2c46b3c09b6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprFieldRight",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 0,
    "bbox_right": 3,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "02c3a8c2-2cd8-4d15-a5e7-268782f8adcb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a438d673-2b08-4780-b006-d2c46b3c09b6",
            "compositeImage": {
                "id": "3a5ee51b-385b-431e-9044-3dadfb759114",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "02c3a8c2-2cd8-4d15-a5e7-268782f8adcb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1420a583-d2da-487a-8400-75c130057c85",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "02c3a8c2-2cd8-4d15-a5e7-268782f8adcb",
                    "LayerId": "684c338d-0f46-4250-ac8e-2deb980d2d9e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 15,
    "layers": [
        {
            "id": "684c338d-0f46-4250-ac8e-2deb980d2d9e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a438d673-2b08-4780-b006-d2c46b3c09b6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 4,
    "xorig": 0,
    "yorig": 0
}