{
    "id": "33456d11-fe20-498b-b317-62a3d341e0e0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprLayerListScrollbar",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 4,
    "bbox_left": 0,
    "bbox_right": 8,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9b70eb11-323f-4134-a6d9-f00b3e3ce333",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "33456d11-fe20-498b-b317-62a3d341e0e0",
            "compositeImage": {
                "id": "c5bb3a4a-5d67-48d1-b59a-d6babb124aaf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b70eb11-323f-4134-a6d9-f00b3e3ce333",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "93d53391-fb69-49a2-9a03-26c8dc50cead",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b70eb11-323f-4134-a6d9-f00b3e3ce333",
                    "LayerId": "16af3ba5-6188-4c8e-9a47-7d183fe00889"
                }
            ]
        },
        {
            "id": "96ec8805-2770-49c3-bbc6-ef5c79fae375",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "33456d11-fe20-498b-b317-62a3d341e0e0",
            "compositeImage": {
                "id": "e4ee86e8-d18c-4137-9730-cdf999732072",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96ec8805-2770-49c3-bbc6-ef5c79fae375",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4dbf3236-558a-4ab2-969d-f4382c602259",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96ec8805-2770-49c3-bbc6-ef5c79fae375",
                    "LayerId": "16af3ba5-6188-4c8e-9a47-7d183fe00889"
                }
            ]
        },
        {
            "id": "22d72519-a81e-48cb-9aa8-7064d042829a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "33456d11-fe20-498b-b317-62a3d341e0e0",
            "compositeImage": {
                "id": "1bf84200-38ff-421a-b11c-b0b602a0a8cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22d72519-a81e-48cb-9aa8-7064d042829a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d73a406b-1acb-4a76-b6b3-249548f22c0f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22d72519-a81e-48cb-9aa8-7064d042829a",
                    "LayerId": "16af3ba5-6188-4c8e-9a47-7d183fe00889"
                }
            ]
        },
        {
            "id": "dc0bbeb2-28da-4b18-adf5-abcb36e151c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "33456d11-fe20-498b-b317-62a3d341e0e0",
            "compositeImage": {
                "id": "6d586c40-2c47-43e4-a9b9-ec05d58ffebd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dc0bbeb2-28da-4b18-adf5-abcb36e151c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf5dde6b-cb37-45ec-8995-8dd3f8c90e99",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dc0bbeb2-28da-4b18-adf5-abcb36e151c8",
                    "LayerId": "16af3ba5-6188-4c8e-9a47-7d183fe00889"
                }
            ]
        },
        {
            "id": "8ea340fd-1120-442e-8ecb-bbb3f1c74bdf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "33456d11-fe20-498b-b317-62a3d341e0e0",
            "compositeImage": {
                "id": "d995c146-9f86-4a26-862f-e8e5edfe1c50",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ea340fd-1120-442e-8ecb-bbb3f1c74bdf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6622f825-a15a-4dbe-8ecb-cba2df874478",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ea340fd-1120-442e-8ecb-bbb3f1c74bdf",
                    "LayerId": "16af3ba5-6188-4c8e-9a47-7d183fe00889"
                }
            ]
        },
        {
            "id": "94a72b87-9a83-4a08-8e49-88dbb4c28ce8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "33456d11-fe20-498b-b317-62a3d341e0e0",
            "compositeImage": {
                "id": "5f4b0fea-f73b-41fa-8a54-c6d32ac4b3ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "94a72b87-9a83-4a08-8e49-88dbb4c28ce8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8ef11f0b-591a-4da2-b3d5-a253cabdd3b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "94a72b87-9a83-4a08-8e49-88dbb4c28ce8",
                    "LayerId": "16af3ba5-6188-4c8e-9a47-7d183fe00889"
                }
            ]
        },
        {
            "id": "2660c3b3-acd8-4d53-aec9-a7601637375e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "33456d11-fe20-498b-b317-62a3d341e0e0",
            "compositeImage": {
                "id": "5e810106-ae4d-489a-80b6-c53274e8d375",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2660c3b3-acd8-4d53-aec9-a7601637375e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dbbe8a65-66b3-433e-9805-16f9adad97a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2660c3b3-acd8-4d53-aec9-a7601637375e",
                    "LayerId": "16af3ba5-6188-4c8e-9a47-7d183fe00889"
                }
            ]
        },
        {
            "id": "ec46474a-7382-4a4c-923f-70e94f60b41b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "33456d11-fe20-498b-b317-62a3d341e0e0",
            "compositeImage": {
                "id": "528ff3f4-9032-4999-b639-ecf857cf35a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec46474a-7382-4a4c-923f-70e94f60b41b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9942d871-6e70-450a-a000-1f054a852b20",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec46474a-7382-4a4c-923f-70e94f60b41b",
                    "LayerId": "16af3ba5-6188-4c8e-9a47-7d183fe00889"
                }
            ]
        },
        {
            "id": "3e061ec5-500f-4fab-bde9-8666e45a6123",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "33456d11-fe20-498b-b317-62a3d341e0e0",
            "compositeImage": {
                "id": "8d44e6c1-8f64-4425-9c76-8d203da95ab1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e061ec5-500f-4fab-bde9-8666e45a6123",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "61411208-0f2b-405c-ae1f-2157fa1c6ba4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e061ec5-500f-4fab-bde9-8666e45a6123",
                    "LayerId": "16af3ba5-6188-4c8e-9a47-7d183fe00889"
                }
            ]
        },
        {
            "id": "bceccca8-26ef-4e7b-a837-58e2b08620b9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "33456d11-fe20-498b-b317-62a3d341e0e0",
            "compositeImage": {
                "id": "880b5fc9-44f3-47a5-8e9e-913ef5ed83b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bceccca8-26ef-4e7b-a837-58e2b08620b9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de552fa2-14eb-40be-b6fc-2a503d0e4b12",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bceccca8-26ef-4e7b-a837-58e2b08620b9",
                    "LayerId": "16af3ba5-6188-4c8e-9a47-7d183fe00889"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 5,
    "layers": [
        {
            "id": "16af3ba5-6188-4c8e-9a47-7d183fe00889",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "33456d11-fe20-498b-b317-62a3d341e0e0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 9,
    "xorig": 0,
    "yorig": 0
}