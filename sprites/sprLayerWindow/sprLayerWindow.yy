{
    "id": "d4a5d2a1-692e-4aa2-9e0c-659da69838c9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprLayerWindow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 0,
    "bbox_right": 191,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d6260533-1f86-4554-9771-7230ca8e18f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d4a5d2a1-692e-4aa2-9e0c-659da69838c9",
            "compositeImage": {
                "id": "35485604-d2fd-4518-b2ef-1395a53c1abe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6260533-1f86-4554-9771-7230ca8e18f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef19619a-bb29-461c-8dce-d32697c943b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6260533-1f86-4554-9771-7230ca8e18f4",
                    "LayerId": "678651b4-84b0-4935-aedd-1278995b538f"
                }
            ]
        },
        {
            "id": "d8fe70cf-37bd-4b11-b12b-34c3039cd4ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d4a5d2a1-692e-4aa2-9e0c-659da69838c9",
            "compositeImage": {
                "id": "768b602c-e159-4ece-b7c7-6666fcec33b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8fe70cf-37bd-4b11-b12b-34c3039cd4ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c01125b7-30f0-436b-b65e-80036542524b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8fe70cf-37bd-4b11-b12b-34c3039cd4ea",
                    "LayerId": "678651b4-84b0-4935-aedd-1278995b538f"
                }
            ]
        },
        {
            "id": "a8a4ce9b-1de5-4fc3-85cc-278f6bcb4222",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d4a5d2a1-692e-4aa2-9e0c-659da69838c9",
            "compositeImage": {
                "id": "f95da891-3064-4836-8c17-1e360f6bc9d0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a8a4ce9b-1de5-4fc3-85cc-278f6bcb4222",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "06da5733-b8fe-4c95-b1f0-5ae8efff0870",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a8a4ce9b-1de5-4fc3-85cc-278f6bcb4222",
                    "LayerId": "678651b4-84b0-4935-aedd-1278995b538f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 15,
    "layers": [
        {
            "id": "678651b4-84b0-4935-aedd-1278995b538f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d4a5d2a1-692e-4aa2-9e0c-659da69838c9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 192,
    "xorig": 0,
    "yorig": 0
}