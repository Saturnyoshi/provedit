{
    "id": "2ba414c6-8b61-4a5c-b992-32a4e5f46cea",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprCollisionRope",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 5,
    "bbox_right": 10,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5e029b67-8d11-431d-82ed-0cc497455e5c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2ba414c6-8b61-4a5c-b992-32a4e5f46cea",
            "compositeImage": {
                "id": "b8a4f466-0607-4873-a385-c9975e3eb7b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e029b67-8d11-431d-82ed-0cc497455e5c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b9deb0bd-c251-4fb2-b860-1bcd97d7a42b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e029b67-8d11-431d-82ed-0cc497455e5c",
                    "LayerId": "be759a1f-e5df-400b-924f-b988d37bc26e"
                }
            ]
        },
        {
            "id": "923bd07b-cf5a-4e14-9725-87b480519aec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2ba414c6-8b61-4a5c-b992-32a4e5f46cea",
            "compositeImage": {
                "id": "bd01427e-e945-41a5-9f9a-c123c500cc96",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "923bd07b-cf5a-4e14-9725-87b480519aec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ff885ae-0412-4905-96b3-2b29be19bd6a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "923bd07b-cf5a-4e14-9725-87b480519aec",
                    "LayerId": "be759a1f-e5df-400b-924f-b988d37bc26e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "be759a1f-e5df-400b-924f-b988d37bc26e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2ba414c6-8b61-4a5c-b992-32a4e5f46cea",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}