{
    "id": "1628ea2b-3142-421b-801d-d7b9d46d7eb5",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fntSmallSquare",
    "AntiAlias": 0,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Kenney Rocket Square",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "4a7f01d7-8cc6-46a6-b27b-594d92ca2f78",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 10,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 95,
                "y": 62
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "a86fd3df-670f-41dc-a216-471cfeea737a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 10,
                "offset": 0,
                "shift": 2,
                "w": 1,
                "x": 26,
                "y": 74
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "e43cecab-645a-45ee-b963-5e22f14d1942",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 10,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 85,
                "y": 62
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "cb463ef5-c93c-4bc8-945c-180693617d6b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 47,
                "y": 50
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "40901592-b122-4f6c-9805-a2b90fc341a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 38,
                "y": 50
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "d21ef9c1-6578-4fe5-a2f2-af470e183a8b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 10,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 68,
                "y": 62
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "424787b1-53b6-4f1a-bb42-2cbdaae797b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 10,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "aeaf2c83-c81d-48f1-80b5-3b23d481f8c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 10,
                "offset": 0,
                "shift": 2,
                "w": 1,
                "x": 14,
                "y": 74
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "7503150d-886d-40be-90d7-44c75590edb4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 10,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 10,
                "y": 74
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "295747dd-5a15-421e-a947-066c4038569b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 10,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 6,
                "y": 74
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "a51b01a4-1c88-465d-9a3e-b742910bb31c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 10,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 75,
                "y": 62
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "23ccec52-e39b-4cfe-a244-74a9138b52c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 20,
                "y": 50
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "9b09eff1-2f03-4a55-97dd-b88c037778e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 10,
                "offset": 0,
                "shift": 2,
                "w": 1,
                "x": 23,
                "y": 74
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "73c7d968-e161-4f38-86d5-ab5f05806706",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 11,
                "y": 50
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "d16c3131-32c6-4f1e-a38f-4f820c40e2c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 10,
                "offset": 0,
                "shift": 2,
                "w": 1,
                "x": 35,
                "y": 74
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "40b72e7f-ae35-4d25-8b22-61da7688392f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 10,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 47,
                "y": 62
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "1b10b7c5-2611-4394-8211-521d3aeffd9e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 56,
                "y": 50
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "02e9b90e-0e77-422c-be7a-84e8f596e7e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 29,
                "y": 50
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "d9f8fcce-1d90-479a-9d51-4b83d5660e34",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 56,
                "y": 38
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "4d3deb75-882f-4dbe-9392-74f950b8a81e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 65,
                "y": 38
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "9218e73d-9b70-43ff-95f3-2af62f543816",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 74,
                "y": 38
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "a3ec4ac5-8abf-4d54-b700-aadcedc5b08d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 83,
                "y": 38
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "52468add-596a-4463-a084-d37acd5fdc62",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 47,
                "y": 38
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "817138df-5120-4bb6-ac20-14fe284153c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 101,
                "y": 38
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "d6e4067f-d510-4f02-b787-5fb872e269f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 110,
                "y": 38
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "cfc7100f-7cb1-43be-af0a-a9214b22ada9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 2,
                "y": 50
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "3b2676da-5e2c-4860-842c-a856d0d0d3e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 10,
                "offset": 0,
                "shift": 2,
                "w": 1,
                "x": 29,
                "y": 74
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "ac8fdc5a-81c5-49bc-b3fd-3df169b13fcf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 10,
                "offset": 0,
                "shift": 2,
                "w": 1,
                "x": 17,
                "y": 74
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "39e2ffb8-b0fd-4a43-9d59-bea821f9f652",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 10,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 110,
                "y": 62
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "988d3f21-532b-4990-be36-1092bf49380b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 11,
                "y": 62
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "719b5ec2-c52d-4285-8e19-46e800742f2a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 10,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 105,
                "y": 62
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "9c46cf37-4f07-4831-a5df-04b09d57b578",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 74,
                "y": 50
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "da58b513-137a-48a1-83a0-0884158fc3e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 101,
                "y": 50
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "522c53d0-5913-49fc-8c42-bc9b8cbda3b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 110,
                "y": 50
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "4446bf40-e83a-49e4-a145-a9de66e237c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 2,
                "y": 62
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "b8fb45b0-171c-4143-8aba-593c833d30b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 65,
                "y": 50
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "47ebb6cf-251c-4c56-b2b8-a3d4af8082bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 20,
                "y": 62
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "db38e4c9-9e2e-46d9-ba38-4954c307c243",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 29,
                "y": 62
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "5482a7ef-688c-482b-8463-9243e4b2f07e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 38,
                "y": 62
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "511f3fb3-7c08-4738-99e8-95fccfbce560",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 92,
                "y": 50
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "78542783-40bc-4568-bd03-9258126cc0ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 83,
                "y": 50
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "8416863a-47c8-4b50-882d-b5339ecf891f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 10,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 90,
                "y": 62
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "4575507b-6f6c-4a14-8710-30766994aef8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 92,
                "y": 38
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "baefb780-c094-4231-8633-fdf78077e382",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 29,
                "y": 38
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "bf1fbd02-e63b-411a-9e8d-c2e35449da9a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 47,
                "y": 26
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "a99a4bde-abc5-485f-827f-d10c6430bb7d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 47,
                "y": 14
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "9fb071d6-fed2-4dbb-b2ff-29f8bc5d26f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 38,
                "y": 14
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "7310742b-7f55-4285-ac46-d6c0bed4a020",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 12,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "50299a3b-e881-4bc7-a4b8-56ef2eacbbbf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 29,
                "y": 14
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "d879d6b2-fab9-49de-8838-a40499fe9e42",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 20,
                "y": 14
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "72526cba-9f46-4e8d-8ac6-aa43990838a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 11,
                "y": 14
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "00b5b9b2-59c2-42bc-b578-23ae2f3c76cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 2,
                "y": 14
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "c4a76f02-8558-48d4-9173-3fff6c722283",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 111,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "815c8a3d-18bc-44e3-b271-0c465f69ef31",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 102,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "89daf486-e2f9-4136-ba1b-3a5137508a86",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 93,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "9785736e-cf75-4851-bb04-fb4f9866ab9a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 84,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "3134ff63-5f8e-4ee3-b7bc-92916bfce704",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 75,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "b3c5880f-00cb-42ab-9af1-49c21c76be07",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 66,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "efe27759-3a4a-4261-9e3b-f6823fe03b68",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 57,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "f9409b77-0409-48ea-98bd-314610cb7769",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 10,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 120,
                "y": 62
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "caaf53dd-5837-48e3-a54a-375281382ee3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 10,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 54,
                "y": 62
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "ded997c4-bc51-4033-8d29-c4373c31a641",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 10,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 2,
                "y": 74
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "f81a56fa-1472-4302-965f-4a8437dba799",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 10,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 61,
                "y": 62
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "de483052-4961-4aff-a081-c5d16f92b414",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 48,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "59430f91-d2b2-4f63-8a7f-1cbe2f77d268",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 10,
                "offset": 6,
                "shift": 8,
                "w": 1,
                "x": 20,
                "y": 74
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "1118b04a-087c-4d29-9adf-772c620eda72",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 39,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "36ae3d57-8ddc-476f-9382-ae7b7ea8271c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 30,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "181eae67-c00d-4ca6-8f5e-eb0afe07ec57",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 21,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "5cf728b3-bb50-4037-bf9c-355544716cba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 56,
                "y": 14
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "8894155b-3bd8-4132-b067-4104cb44b4d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 65,
                "y": 14
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "6aa43620-4118-4ac8-930f-90e1eaabb9f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 74,
                "y": 14
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "18b094af-92c4-4038-b8fc-4bdc9edc763d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 83,
                "y": 14
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "a6d50e94-e32c-4141-95ea-a9c9154d4f3e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 11,
                "y": 38
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "529aa0b6-1973-4c89-a2db-a03c364163ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 10,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 115,
                "y": 62
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "3d242331-9e68-483b-8856-31d2fae4eee5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 2,
                "y": 38
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "6d96586a-0c1d-4df0-80a0-89a0630f5fd4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 110,
                "y": 26
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "ed751ace-d713-4dfb-895d-16de883b8fe8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 101,
                "y": 26
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "dba8dce0-917a-4aaf-93e4-962ba6a4ca2f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 92,
                "y": 26
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "70268825-19a3-4aae-9ff3-6485b7a5c6b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 83,
                "y": 26
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "e779996e-aa84-4a48-81c8-8956e9654279",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 74,
                "y": 26
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "5c8db275-c73b-4082-af04-466b5dacd6a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 65,
                "y": 26
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "60028071-833b-4a1c-9ca4-9adcd9668347",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 20,
                "y": 38
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "074456a3-3158-4cd4-bcc9-c1f79439a77e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 56,
                "y": 26
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "b5530dce-76b8-4529-8da4-ed79db1f5f37",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 38,
                "y": 26
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "d33399c0-c107-4ae7-a6d3-8916fda7d098",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 29,
                "y": 26
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "7ad4fd80-5657-4e64-ab0a-f522d3b72c87",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 20,
                "y": 26
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "0b25bbb2-f73b-4e79-b226-1b03ff9f7cb2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 11,
                "y": 26
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "6821b0fb-7bdb-44dc-83dd-35bd2f92c296",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 2,
                "y": 26
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "b9a23730-12d0-440d-bab5-a1fca92f9a67",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 110,
                "y": 14
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "ec76d48b-e31b-466b-96aa-fa08264e70a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 101,
                "y": 14
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "61739261-7533-4a76-b54b-8bee47aabae5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 92,
                "y": 14
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "f9086a7a-cde5-4ad9-943c-b88b7dd3c892",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 10,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 80,
                "y": 62
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "a49817a2-8d1d-4744-9935-8607e0b79145",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 10,
                "offset": 0,
                "shift": 2,
                "w": 1,
                "x": 32,
                "y": 74
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "17a07337-1bfc-4d54-b534-db4afdb7b246",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 10,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 100,
                "y": 62
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "68bc1c27-e3c5-4949-a2d9-622577505ffc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 38,
                "y": 38
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 6,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}