{
    "id": "222b53fc-f66f-4fa4-9816-d67df44d926d",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fntTiny",
    "AntiAlias": 0,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Kenney Mini Square",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "6cfe1530-36f9-4ac5-971e-a1db7499fe4f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 10,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "b6c727a4-765a-41ca-9cf6-771826358f3b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 10,
                "offset": 0,
                "shift": 2,
                "w": 1,
                "x": 14,
                "y": 38
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "b2692fe3-dc9b-4542-89c0-355dfc1cc5ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 10,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 9,
                "y": 38
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "4a59d05b-0fb2-4750-bda1-1e63a38cfc8d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 10,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 2,
                "y": 38
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "10e112b0-4a61-427c-bd23-1629e6b1c094",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 10,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 116,
                "y": 26
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "b0ddb714-225e-488f-9dfe-ce05eaeb3122",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 10,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 109,
                "y": 26
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "2ba1bab6-39fe-4961-88c1-1bfeb273983e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 10,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 102,
                "y": 26
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "a59f7bc3-8fb8-4617-adc1-f566c6ddb62c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 10,
                "offset": 0,
                "shift": 2,
                "w": 1,
                "x": 99,
                "y": 26
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "59756f96-49dd-4ad4-a127-722596cbd8d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 10,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 95,
                "y": 26
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "432546d4-6ae6-4d51-af63-7e1bb30773aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 10,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 91,
                "y": 26
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "84bf4048-c6ed-45b5-a263-63e93faf89a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 10,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 17,
                "y": 38
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "c13ef9c6-d017-4940-82be-a6ca41d024f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 10,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 86,
                "y": 26
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "1dd62208-6186-4586-87b3-f937e2d36202",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 10,
                "offset": 0,
                "shift": 2,
                "w": 1,
                "x": 77,
                "y": 26
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "7093d00b-a400-4b72-a671-cc37630c7aa3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 10,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 72,
                "y": 26
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "0190c6bf-a79d-4e09-afd1-8fca8245eb43",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 10,
                "offset": 0,
                "shift": 2,
                "w": 1,
                "x": 69,
                "y": 26
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "0cdcd0b7-7d57-4cc2-82bf-e1f30ce1fe20",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 10,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 62,
                "y": 26
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "a08706f5-c6f3-43f9-8003-406ff27d95ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 10,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 56,
                "y": 26
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "df34c25d-f016-4760-9146-a3ce198d89fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 10,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 52,
                "y": 26
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "b665bfad-399f-4806-a698-7a0b545b6f0a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 10,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 46,
                "y": 26
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "7466c4a4-f823-4dfb-afd7-619f79f8c0bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 10,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 40,
                "y": 26
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "ac442d05-5116-434e-9d35-eaeb5d6b3e31",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 10,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 34,
                "y": 26
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "c13c739e-7c73-4d89-ad5c-339dc02c69a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 10,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 80,
                "y": 26
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "c6a00bae-facf-4b0a-b625-1da2b0785d35",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 10,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 28,
                "y": 38
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "09ccdaee-ecb0-40ef-9d31-e45eff372d50",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 10,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 89,
                "y": 38
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "6c0f9cff-dba2-433a-8d75-8d04390b1d23",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 10,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 34,
                "y": 38
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "82942080-30ff-470b-ae38-3e3f1a176566",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 10,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 18,
                "y": 50
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "c32eb1a1-7a24-4d8b-8c90-27cb5fa16d2a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 10,
                "offset": 0,
                "shift": 2,
                "w": 1,
                "x": 15,
                "y": 50
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "2273c687-d9fa-4348-bbaf-2cf1eaf5fc00",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 10,
                "offset": 0,
                "shift": 2,
                "w": 1,
                "x": 12,
                "y": 50
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "bb8a4b7d-0ab0-45b8-a202-b9accad74fb2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 10,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 7,
                "y": 50
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "0cf5d601-7e8c-4db3-a236-2f559d545910",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 10,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 2,
                "y": 50
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "df73d3a1-6197-43e7-b680-4ba6f59d77bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 10,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 119,
                "y": 38
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "bac226d7-8e65-4630-a21c-bcf54535a8ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 10,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 113,
                "y": 38
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "37173463-7454-47f0-82b0-889f82572f63",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 10,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 107,
                "y": 38
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "da1f78b1-7cef-4e9d-8b4b-3431b7963be1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 10,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 101,
                "y": 38
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "494a23e7-aa93-4f39-84dc-adfbb5a3ff3d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 10,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 24,
                "y": 50
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "3f9db6f0-1d01-4142-a288-302c63ad3a0c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 10,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 95,
                "y": 38
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "eda3c625-c584-4844-83ac-7da93dbccf12",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 10,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 83,
                "y": 38
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "32b0f62a-8cbb-495a-af30-925fd5ed800e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 10,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 77,
                "y": 38
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "bb035a6c-ccf9-48a8-b7aa-c014b3317bb3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 10,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 71,
                "y": 38
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "3078272f-42e3-4bf2-89e7-8669f8924a0f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 10,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 65,
                "y": 38
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "1363b3ac-323f-43cc-8e9b-e7bdcd60b4fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 10,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 59,
                "y": 38
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "95562ddb-c4c6-4ba8-a806-4a828d5cc6d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 10,
                "offset": 0,
                "shift": 2,
                "w": 1,
                "x": 56,
                "y": 38
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "e9b59577-62fa-4b43-9615-699b3c7c713b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 10,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 51,
                "y": 38
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "bf5cd399-53eb-4159-8325-dbbab6cc307e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 10,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 45,
                "y": 38
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "ab5af8e2-a4bf-42b2-a195-8d8d0cc4e812",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 10,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 40,
                "y": 38
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "2626915c-a219-4261-a44a-488d5517e48a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 10,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 27,
                "y": 26
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "e561c4c9-1b18-408e-9443-b624853a1c7c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 10,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 22,
                "y": 38
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "3d461399-f3cf-4140-99a9-802c14f30360",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 10,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 21,
                "y": 26
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "83c3b9ab-bb92-4d4e-a180-a33baedf2198",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 10,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 2,
                "y": 14
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "bfa4d367-2b25-4ea3-a8d1-0c3ea839a913",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 10,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 114,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "aa2038bc-1e84-4a0b-be82-0e148471e67a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 10,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 108,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "fa0eef33-95eb-4a95-a66e-c5f0964d4c69",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 10,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 102,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "2531ea18-4a72-4105-a045-e743564f4ec9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 10,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 97,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "b6064086-890e-4efd-9645-2618e3410a76",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 10,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 91,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "e5da3673-39fc-4be2-a80f-244af778c37e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 10,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 84,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "edd976f1-b1ec-4c38-b0da-358a84c41bfb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 10,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 77,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "4282fd8d-c7b1-4151-ada7-114ee9eb659c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 10,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 71,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "d625baff-9eb3-4ef9-844d-e9e95c38a0c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 10,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 65,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "516a64ca-2928-4369-9362-d6267f7083cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 10,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 120,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "aa12a013-6681-465c-914d-1a25457ecf1d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 10,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 61,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "f8bd4f36-9c62-40da-8c6d-4dde2dfa6c9a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 10,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 48,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "eaf9b980-9a0f-45a6-9f02-d52df7f98bf4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 10,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 44,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "4d91ce07-7d63-49ee-a34b-15b7678f56a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 10,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 39,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "37fbb051-0071-476d-99fa-c2f3eda68f30",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 10,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 33,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "ac52eb1d-803d-44f9-b496-c561ff562bed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 10,
                "offset": 3,
                "shift": 5,
                "w": 1,
                "x": 30,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "20b30622-3c1c-4e81-aafe-acf8f1044835",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 10,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 24,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "5bd8ca50-12f2-4206-a868-0d739a31ea02",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 10,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 18,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "1040c86b-9f2a-4cdf-ad9c-a203dd8437b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 10,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 12,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "200dfc86-6209-442c-91c4-e16574dd8e83",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 10,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 6,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "703204fb-408b-4d87-b67e-ad81a30773b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 10,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 55,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "66aced11-ff4c-43dc-bdd2-439c09f45c03",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 10,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 8,
                "y": 14
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "15e0b848-1a3a-486e-b38d-8394b0ff552a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 10,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 71,
                "y": 14
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "c2c70768-e698-4a98-b7a8-a487966d5895",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 10,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 14,
                "y": 14
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "858da1e9-1d5c-4fe2-82d3-0d79592a0812",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 10,
                "offset": 0,
                "shift": 2,
                "w": 1,
                "x": 7,
                "y": 26
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "e28f1930-73ff-46cb-b050-e45e106010bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 10,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 2,
                "y": 26
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "15218a2f-4d6c-4b07-878e-fe27524f1fd7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 10,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 119,
                "y": 14
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "3200bca6-d9de-48fd-80ab-4960064ee20d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 10,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 114,
                "y": 14
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "3aab9831-b408-4e01-ada8-0bd9236bb043",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 10,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 107,
                "y": 14
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "b051d1e8-0b26-484d-ac12-605b5c7ef69c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 10,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 101,
                "y": 14
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "62f0a30a-5a87-4670-91f3-cc83b0ff85f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 10,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 95,
                "y": 14
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "edc7ca2f-8a56-4c0e-bf41-29f785e2e0b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 10,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 89,
                "y": 14
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "b8f4d3b9-5709-4361-9cb9-5f29ee06c60d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 10,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 83,
                "y": 14
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "fdd6d59b-c8b4-4ec5-b720-cff9780d2741",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 10,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 10,
                "y": 26
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "84a07280-db41-486b-94fb-52a884232683",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 10,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 77,
                "y": 14
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "a64fac92-f537-4a3d-b55a-7e2949e59003",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 10,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 66,
                "y": 14
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "e794d5a9-0190-46aa-bd8b-62f920d9656d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 10,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 60,
                "y": 14
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "b58d742a-0b8f-47e0-92ee-26d45e9db992",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 10,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 53,
                "y": 14
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "72078ff3-c2b2-442c-96a4-2c8e1c564e03",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 10,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 46,
                "y": 14
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "83c2766a-5b28-4ae2-aa0a-b38b32a8df58",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 10,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 40,
                "y": 14
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "870a059c-a548-4468-9a87-203e5f39476f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 10,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 34,
                "y": 14
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "9cc15c4f-8335-4628-839d-5962f69a54c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 10,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 28,
                "y": 14
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "c37e7f46-ad2c-466c-a13d-53c2a0556e99",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 10,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 23,
                "y": 14
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "ad573b96-6b9a-4ad0-af7c-5ba5d9c18901",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 10,
                "offset": 0,
                "shift": 2,
                "w": 1,
                "x": 20,
                "y": 14
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "1c70166f-5f35-4142-9e6f-86161cdefc3e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 10,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 16,
                "y": 26
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "8409ffad-0373-49cc-8857-afe124e1ca10",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 10,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 30,
                "y": 50
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 6,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}