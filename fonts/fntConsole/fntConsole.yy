{
    "id": "324f8c60-7a61-42b4-aecf-35853ff45415",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fntConsole",
    "AntiAlias": 0,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Synchronizer NBP",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "496af3e8-859e-468b-9936-e4c298f462e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "65746db3-cdca-4668-a392-4862c60f6834",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 1,
                "x": 27,
                "y": 92
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "ba79d5a2-e8ea-4753-9a2e-33c78d011c1f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 3,
                "x": 32,
                "y": 83
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "cddbb3b2-6eb9-4d95-bdab-9d16f94fc63a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 51,
                "y": 56
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "0d1bbe71-9996-4df0-a31d-c98a011b84cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 58,
                "y": 56
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "d2bcc619-733f-4b22-b8a4-6bbc5dd025c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 65,
                "y": 56
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "c381452b-1897-49be-b319-e084f8448577",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 72,
                "y": 56
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "6a557839-3af0-4427-b4c2-01574678c880",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 2,
                "x": 99,
                "y": 83
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "c9e70047-766c-4f6a-ac16-9006eca27d96",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 2,
                "x": 103,
                "y": 83
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "4a227ee5-5a8c-4f18-b19f-fd586852a237",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 2,
                "x": 107,
                "y": 83
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "c8f9c823-6b78-4386-851b-0a9ac2ef2b3d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 79,
                "y": 56
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "e5b8a7ab-e804-4c54-a809-940df01baa44",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 86,
                "y": 56
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "09d51b52-9323-4ea8-8262-3e3a0f000316",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 2,
                "x": 87,
                "y": 83
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "0168368e-1f62-4230-ad5c-b4d22243bcd6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 93,
                "y": 56
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "c5db690d-2cd2-4d09-8dda-74104bda3c57",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 2,
                "x": 115,
                "y": 83
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "fbdc3512-8c21-4ee6-aa9c-e4fd945b4c9c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 100,
                "y": 56
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "f5f69cf4-bfb0-49a8-b628-88b3e7626cfe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 107,
                "y": 56
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "7ab53523-5ae5-40bb-a31f-285037ece1eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 7,
                "offset": 1,
                "shift": 7,
                "w": 2,
                "x": 111,
                "y": 83
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "5e34a96d-9cf9-4b0f-9268-a776e2337ea0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 114,
                "y": 56
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "c6063b12-49eb-43c3-8d2f-9d6f390c7a95",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 2,
                "y": 65
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "86911479-9d9f-408b-b72b-9927a1afdfb2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 9,
                "y": 65
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "9fb32636-bcff-40b4-b175-4562b77f52e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 16,
                "y": 65
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "fb91628e-5844-4674-9f9a-5dad52baead0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 23,
                "y": 65
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "e397877f-8192-4927-bc2d-9032fb2d5ff8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 30,
                "y": 65
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "cfdac808-527b-4d69-968f-c51b4928bad7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 37,
                "y": 65
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "6b5a466a-11b9-4a8c-86a9-458890d81c9d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 44,
                "y": 65
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "77ace28b-ec99-488a-8976-acae7f4b77b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 2,
                "x": 2,
                "y": 92
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "5adaf721-30fd-43ae-90df-e68d711a3dbe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 2,
                "x": 123,
                "y": 83
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "42e64542-f6bb-451b-89d3-388df23718b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 3,
                "x": 62,
                "y": 83
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "bade17fd-68f0-4b2c-b5b7-54248097dd09",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 51,
                "y": 65
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "2c624409-b1b2-480f-ad51-be1666686de4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 3,
                "x": 72,
                "y": 83
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "8a302709-fc6d-43c5-9a9d-b889dcded367",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 58,
                "y": 65
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "53d9a996-74a2-4c30-a9d2-86f2658ce3f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 44,
                "y": 56
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "a6b535dd-3dd7-4b68-b50c-388bf1ed678b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 37,
                "y": 56
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "f65c0cfa-3ccf-4c27-b80b-f1cd7941a35e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 30,
                "y": 56
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "ffd579d8-310d-4379-be79-232a0d41ebec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 72,
                "y": 47
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "74b8300f-3df1-4405-9426-8a374ec654ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 16,
                "y": 47
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "c67d4a62-8d34-4bcb-8e4c-6e478f5dcc6f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 23,
                "y": 47
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "b8a6ad09-3f1a-46a2-840e-131e5b1f987f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 30,
                "y": 47
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "93c9c39c-4e39-46d1-b21d-e8f4e771caed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 37,
                "y": 47
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "9932993f-dd5c-47de-aa84-e9628ac2c7be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 44,
                "y": 47
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "1ef44f99-a092-45fb-80e7-075d1b04597e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 51,
                "y": 47
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "665f4089-35ab-446a-a9aa-c3adaad2245e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 58,
                "y": 47
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "d1bf1a2a-8dce-4746-b94f-5ee054aabea2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 65,
                "y": 47
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "d732cc8c-fc56-42e6-abbb-7add2423efb5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 79,
                "y": 47
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "5e8d67e2-8175-4013-a29d-32d3434efc85",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 23,
                "y": 56
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "1f162cd6-1c27-4b1e-b9a6-8ff48df69e4d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 86,
                "y": 47
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "d105fc93-e932-4437-89d7-728a1a97fbc6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 79,
                "y": 65
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "07481d88-f4c3-4854-839f-1e3122b0721a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 100,
                "y": 47
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "2b2453ac-7cbd-4655-8272-c8deb54c1a3b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 107,
                "y": 47
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "70c1649b-5aab-47cb-8be9-2fda8a84702d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 114,
                "y": 47
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "9b156671-8803-466f-8a5a-e137d450fc8b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 28,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "07d67633-8449-4985-bd54-365f57fdcd17",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 2,
                "y": 56
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "516a0c3f-d893-43cd-ad81-f0d9671bbb4f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 9,
                "y": 56
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "39475fbe-8cdf-4682-95c2-a8aa44fcc598",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 11,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "6fe6f237-0a98-4d85-9b8f-65dfba937a73",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 16,
                "y": 56
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "1e8b6be8-e941-4982-bd7e-401735f3ec66",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 65,
                "y": 65
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "7f98bf48-0281-4bcd-a7a5-273c4f9162fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 72,
                "y": 65
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "e2cc2b98-c3ba-4887-b9ed-199c8c4d45a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 20,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "17d091e8-c906-494f-9514-25e8f984a69e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 2,
                "x": 10,
                "y": 92
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "6546545c-4a1f-4ffb-bcd0-2adfa3237e9b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 86,
                "y": 65
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "f4e782cd-2e26-4f5c-99e9-2528079c82ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 2,
                "x": 95,
                "y": 83
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "7c68f552-bddf-4719-a232-6040ee9e16d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 72,
                "y": 74
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "a55b4f03-315b-42f4-ac34-8e2e7798c0ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 93,
                "y": 74
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "9db11e85-9cbd-4ed9-882e-6956eb245b1f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 2,
                "x": 6,
                "y": 92
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "b2fe67bb-8a2a-4480-9b4f-70f6083d0614",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 86,
                "y": 74
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "e1a90dd1-8b12-4f39-8395-b368c4f9e541",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 79,
                "y": 74
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "77c30381-7293-4c5f-a585-581a86a0611a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 58,
                "y": 74
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "c84cc097-3a9e-4cfa-a377-417b30418fea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 51,
                "y": 74
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "e1ecf0c5-e401-4707-953e-605405548ddb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 44,
                "y": 74
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "4d250373-5e3a-4da4-851a-95435824f277",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 37,
                "y": 74
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "87932b7a-d7df-4218-b7a8-98a268484044",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 30,
                "y": 74
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "945277d3-7b74-44ec-852c-4ee0e3ef25b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 23,
                "y": 74
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "518bdfdc-5c3c-4bb7-b4ef-1247381ef352",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 7,
                "offset": 2,
                "shift": 7,
                "w": 1,
                "x": 24,
                "y": 92
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "e984e9f4-2758-4c52-8564-a0cc0a2e65e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 16,
                "y": 74
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "b80321b3-252f-4d44-8146-5227690d6ffc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 4,
                "x": 118,
                "y": 74
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "a7e4a2e4-c0ea-4846-a9f6-bffbef6d7662",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 7,
                "offset": 1,
                "shift": 7,
                "w": 2,
                "x": 14,
                "y": 92
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "1c9f3c7c-8d72-4541-8fb3-9aa84bf06771",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 9,
                "y": 74
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "9f320d15-d6c2-4f41-b4eb-9ac9fa9d627e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 2,
                "y": 74
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "85b1c104-97f8-41f3-9a4e-4db4dbf4ebdc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 114,
                "y": 65
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "d857718a-f291-46a5-bbaa-de9767a58994",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 107,
                "y": 65
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "47f53e7f-9e99-4a13-995a-247fbe50269f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 100,
                "y": 65
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "6f009978-2b73-412e-9b78-1b34febe5cfe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 93,
                "y": 65
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "a3ec58d5-0179-4db1-a73b-b155ee1fd4c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 9,
                "y": 47
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "44410557-7c2e-4a05-9357-33116a6d5c25",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 93,
                "y": 47
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "453425dc-c8eb-4130-adcb-16a9a9ee0b02",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 114,
                "y": 38
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "ed4de3fc-7d18-41ad-b033-22d83a8f111f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 37,
                "y": 38
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "c1e8e185-286b-4e35-89fc-6ae4d855a898",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 51,
                "y": 11
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "c15781d4-7b0e-4681-8cd7-939914b7a86b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 58,
                "y": 11
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "29671e6d-da44-47f4-a3f2-e5615f297463",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 65,
                "y": 11
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "762ef3bb-f75a-4e36-9001-e91abe8059d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 72,
                "y": 11
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "b03eab15-5bf2-4ec0-9ac9-41b837e7661b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 3,
                "x": 17,
                "y": 83
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "1d55506d-31c4-410f-9d5b-3b8840e898b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 7,
                "offset": 1,
                "shift": 7,
                "w": 1,
                "x": 30,
                "y": 92
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "d9b5186d-06ce-4799-95a8-5ad4667e9b25",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 3,
                "x": 7,
                "y": 83
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "f136b0c2-0813-4e31-8e8a-5154045487d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 2,
                "y": 47
            }
        },
        {
            "Key": 161,
            "Value": {
                "id": "c25e9a80-bb18-471d-ad78-e7ee8b286046",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 161,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 1,
                "x": 21,
                "y": 92
            }
        },
        {
            "Key": 162,
            "Value": {
                "id": "cd24e641-62b5-41dd-a441-454fecfb955d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 162,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 86,
                "y": 11
            }
        },
        {
            "Key": 163,
            "Value": {
                "id": "1ae309ac-546d-4c06-bfb9-35c06aac1b49",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 163,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 93,
                "y": 11
            }
        },
        {
            "Key": 164,
            "Value": {
                "id": "eb499424-6146-43e7-a2d7-87e8cd118225",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 164,
                "h": 7,
                "offset": 1,
                "shift": 7,
                "w": 4,
                "x": 100,
                "y": 74
            }
        },
        {
            "Key": 165,
            "Value": {
                "id": "5451ed5e-4f54-41da-91ea-dbc49dde5d59",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 165,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 107,
                "y": 11
            }
        },
        {
            "Key": 166,
            "Value": {
                "id": "92f1285b-3989-48b7-acfa-dcae2c77a91d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 166,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 1,
                "x": 33,
                "y": 92
            }
        },
        {
            "Key": 167,
            "Value": {
                "id": "f876ba98-f707-446d-b313-6e7c1081d7e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 167,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 36,
                "y": 2
            }
        },
        {
            "Key": 168,
            "Value": {
                "id": "a7f6aea4-08e4-4ff5-a788-e04d8b47ac58",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 168,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 3,
                "x": 47,
                "y": 83
            }
        },
        {
            "Key": 169,
            "Value": {
                "id": "f80f7fa8-8b5c-44a1-a2db-c11b814b8a19",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 169,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 44,
                "y": 20
            }
        },
        {
            "Key": 170,
            "Value": {
                "id": "dcffee90-7a3c-49a8-b730-bb613cbf68c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 170,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 3,
                "x": 77,
                "y": 83
            }
        },
        {
            "Key": 171,
            "Value": {
                "id": "d3f687b6-ba65-4e9c-ba89-a6c7732c6cf9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 171,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 114,
                "y": 11
            }
        },
        {
            "Key": 172,
            "Value": {
                "id": "4ad0c310-92ef-4309-9c13-9866b2618899",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 172,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 2,
                "y": 20
            }
        },
        {
            "Key": 173,
            "Value": {
                "id": "14daa87d-3b42-4c9b-b525-aab04e5eeee5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 173,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 3,
                "x": 57,
                "y": 83
            }
        },
        {
            "Key": 174,
            "Value": {
                "id": "0806e445-15cc-4a9d-90f4-c4b59bb4f27f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 174,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 9,
                "y": 20
            }
        },
        {
            "Key": 175,
            "Value": {
                "id": "d261cfab-8e42-44e2-be77-a52603df35b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 175,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 16,
                "y": 20
            }
        },
        {
            "Key": 176,
            "Value": {
                "id": "8bb30742-fdae-41da-a179-6457a40c05a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 176,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 3,
                "x": 52,
                "y": 83
            }
        },
        {
            "Key": 177,
            "Value": {
                "id": "a2a00218-7ec0-4f53-8fde-3da033c9c896",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 177,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 23,
                "y": 20
            }
        },
        {
            "Key": 178,
            "Value": {
                "id": "7a1472a8-2eec-431d-9023-35a3d26f44a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 178,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 3,
                "x": 42,
                "y": 83
            }
        },
        {
            "Key": 179,
            "Value": {
                "id": "6f22a4ce-57bd-4443-a4b2-b218f7397282",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 179,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 3,
                "x": 2,
                "y": 83
            }
        },
        {
            "Key": 180,
            "Value": {
                "id": "3f6e1b0f-342c-42ab-94a8-f65b04162c0a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 180,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 2,
                "x": 91,
                "y": 83
            }
        },
        {
            "Key": 181,
            "Value": {
                "id": "eba0c851-6a56-499c-ad50-a43d90af2408",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 181,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 30,
                "y": 20
            }
        },
        {
            "Key": 182,
            "Value": {
                "id": "b97bc624-2f16-40e0-a82b-7cf7410c64f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 182,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 37,
                "y": 20
            }
        },
        {
            "Key": 183,
            "Value": {
                "id": "6f70b9d3-d80a-4b71-b733-fda690ea72b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 183,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 1,
                "x": 18,
                "y": 92
            }
        },
        {
            "Key": 184,
            "Value": {
                "id": "9c3877db-1156-4fe2-be1d-96654f1414a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 184,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 3,
                "x": 67,
                "y": 83
            }
        },
        {
            "Key": 185,
            "Value": {
                "id": "d28c7471-9114-4d0c-a8db-1327963f6294",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 185,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 2,
                "x": 119,
                "y": 83
            }
        },
        {
            "Key": 186,
            "Value": {
                "id": "198113ec-c8de-4a4e-ae8c-78e9913eeb77",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 186,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 3,
                "x": 82,
                "y": 83
            }
        },
        {
            "Key": 187,
            "Value": {
                "id": "0a989b46-2d68-4c55-b22e-873ffa481d85",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 187,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 44,
                "y": 11
            }
        },
        {
            "Key": 188,
            "Value": {
                "id": "20e8bb2d-fae9-4255-919c-0f808a54b6dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 188,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 4,
                "x": 112,
                "y": 74
            }
        },
        {
            "Key": 189,
            "Value": {
                "id": "c9411d71-3e17-4bae-b0fd-879e7e41e3ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 189,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 4,
                "x": 106,
                "y": 74
            }
        },
        {
            "Key": 190,
            "Value": {
                "id": "6df6000d-75ff-452e-9cb8-9af30cd74ed9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 190,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 44,
                "y": 2
            }
        },
        {
            "Key": 191,
            "Value": {
                "id": "2fa31b97-38b8-4d96-b3d1-674a744edce5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 191,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 37,
                "y": 11
            }
        },
        {
            "Key": 192,
            "Value": {
                "id": "735b4e0e-b7f8-4eeb-9aad-c05c7ea8bc7d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 192,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 30,
                "y": 11
            }
        },
        {
            "Key": 193,
            "Value": {
                "id": "81b51af4-1762-46a1-bbf6-40f82e43a5ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 193,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 52,
                "y": 2
            }
        },
        {
            "Key": 194,
            "Value": {
                "id": "9964646c-84b4-488d-b433-aee047e87b0b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 194,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 59,
                "y": 2
            }
        },
        {
            "Key": 195,
            "Value": {
                "id": "c1fd447f-2447-45d7-b722-bfdfaf21e261",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 195,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 66,
                "y": 2
            }
        },
        {
            "Key": 196,
            "Value": {
                "id": "b1c9adea-2970-4243-8366-03bf4354ea9e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 196,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 73,
                "y": 2
            }
        },
        {
            "Key": 197,
            "Value": {
                "id": "4d435103-c341-4956-a66e-f06c92faa982",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 197,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 80,
                "y": 2
            }
        },
        {
            "Key": 198,
            "Value": {
                "id": "e1321f38-e61b-4e08-bdbe-4f907b2c3fd0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 198,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 23,
                "y": 11
            }
        },
        {
            "Key": 199,
            "Value": {
                "id": "003fd38e-3b02-41bc-959a-06eb2c8bf3ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 199,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 87,
                "y": 2
            }
        },
        {
            "Key": 200,
            "Value": {
                "id": "49ba817d-7328-4045-bc25-1c1e62f184e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 200,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 94,
                "y": 2
            }
        },
        {
            "Key": 201,
            "Value": {
                "id": "fef59cfb-6af2-4464-9a84-80c4bd5108e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 201,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 101,
                "y": 2
            }
        },
        {
            "Key": 202,
            "Value": {
                "id": "c453778b-8e2f-4832-b473-143b35d44d1c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 202,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 108,
                "y": 2
            }
        },
        {
            "Key": 203,
            "Value": {
                "id": "e51c0acc-9810-40bf-9fc5-98c382b14a36",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 203,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 115,
                "y": 2
            }
        },
        {
            "Key": 204,
            "Value": {
                "id": "0797171a-2402-4a05-aabf-91d7a8659ba7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 204,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 2,
                "y": 11
            }
        },
        {
            "Key": 205,
            "Value": {
                "id": "f3ba90a0-749d-4cde-af32-55f8e6373d5e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 205,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 9,
                "y": 11
            }
        },
        {
            "Key": 206,
            "Value": {
                "id": "bcc71b92-482d-4fb4-98d2-f68f6863e290",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 206,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 16,
                "y": 11
            }
        },
        {
            "Key": 207,
            "Value": {
                "id": "9073c9a2-e759-4d04-85f4-f547fb5d0f5e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 207,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 100,
                "y": 11
            }
        },
        {
            "Key": 208,
            "Value": {
                "id": "82e87b90-d47e-46ee-98b5-0f673ded8252",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 208,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 51,
                "y": 20
            }
        },
        {
            "Key": 209,
            "Value": {
                "id": "459e0ff9-c4fc-44d6-a627-a8bc7f66f9e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 209,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 58,
                "y": 20
            }
        },
        {
            "Key": 210,
            "Value": {
                "id": "5fedd244-75d5-460e-b64c-c4f52ee56556",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 210,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 65,
                "y": 20
            }
        },
        {
            "Key": 211,
            "Value": {
                "id": "fb8d7ff8-e6cc-44dc-802e-d48d3bc378cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 211,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 100,
                "y": 29
            }
        },
        {
            "Key": 212,
            "Value": {
                "id": "1216713f-1df0-49ab-8f2a-0479ddbeda46",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 212,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 107,
                "y": 29
            }
        },
        {
            "Key": 213,
            "Value": {
                "id": "b8132ae9-ea9b-4cb9-bd66-f2efba159a0b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 213,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 114,
                "y": 29
            }
        },
        {
            "Key": 214,
            "Value": {
                "id": "289603b6-5c46-48d6-81e9-81b1df917cd8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 214,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 2,
                "y": 38
            }
        },
        {
            "Key": 215,
            "Value": {
                "id": "1e0f1748-32cd-4f8c-8c22-41712f6402f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 215,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 9,
                "y": 38
            }
        },
        {
            "Key": 216,
            "Value": {
                "id": "9559b0a4-ef08-4915-ac69-bf3cb2bfb0d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 216,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 16,
                "y": 38
            }
        },
        {
            "Key": 217,
            "Value": {
                "id": "d4b6b4fd-a541-40e1-81dc-763cfa98d266",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 217,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 23,
                "y": 38
            }
        },
        {
            "Key": 218,
            "Value": {
                "id": "a8e184a0-4758-4215-85d8-2e732a7b8c81",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 218,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 30,
                "y": 38
            }
        },
        {
            "Key": 219,
            "Value": {
                "id": "897c91a8-2405-4aff-8556-e7c3a5426ebd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 219,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 44,
                "y": 38
            }
        },
        {
            "Key": 220,
            "Value": {
                "id": "846f18ce-da72-4c92-8b6f-f258b5d06e57",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 220,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 107,
                "y": 38
            }
        },
        {
            "Key": 221,
            "Value": {
                "id": "9283e832-eb41-4e29-a439-88a7729f8a92",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 221,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 51,
                "y": 38
            }
        },
        {
            "Key": 222,
            "Value": {
                "id": "2a644587-1a8b-47bc-bd53-13f89568887b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 222,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 58,
                "y": 38
            }
        },
        {
            "Key": 223,
            "Value": {
                "id": "2f0247da-266d-46fc-b3c3-ece2e5b63c1a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 223,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 65,
                "y": 38
            }
        },
        {
            "Key": 224,
            "Value": {
                "id": "a0d89b66-f7c4-49ee-aabf-99fca470158f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 224,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 72,
                "y": 38
            }
        },
        {
            "Key": 225,
            "Value": {
                "id": "27b874d8-cf4e-4dca-9307-a586680df96e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 225,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 79,
                "y": 38
            }
        },
        {
            "Key": 226,
            "Value": {
                "id": "0e7d9aff-599d-43e3-be87-f933e9fd7095",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 226,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 86,
                "y": 38
            }
        },
        {
            "Key": 227,
            "Value": {
                "id": "b498820c-28cb-4b39-a05f-7caab5de18cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 227,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 93,
                "y": 38
            }
        },
        {
            "Key": 228,
            "Value": {
                "id": "1022a914-e8b4-4c01-bf52-4bb69797addc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 228,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 100,
                "y": 38
            }
        },
        {
            "Key": 229,
            "Value": {
                "id": "ab049ae0-dbc3-4d82-ac93-cedff4698864",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 229,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 93,
                "y": 29
            }
        },
        {
            "Key": 230,
            "Value": {
                "id": "8f01d8af-9982-44c9-b23b-9d27cc11fd9d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 230,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 86,
                "y": 29
            }
        },
        {
            "Key": 231,
            "Value": {
                "id": "e5bbe548-de06-4e04-a60f-fd521fabd638",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 231,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 79,
                "y": 29
            }
        },
        {
            "Key": 232,
            "Value": {
                "id": "df768e16-0496-484e-9d7c-dea91e5ada7c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 232,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 72,
                "y": 29
            }
        },
        {
            "Key": 233,
            "Value": {
                "id": "e8c89b07-e4dd-49af-91e1-7b4db8789a29",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 233,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 72,
                "y": 20
            }
        },
        {
            "Key": 234,
            "Value": {
                "id": "7bd808fd-6e22-45e4-b79f-d909488b5813",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 234,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 79,
                "y": 20
            }
        },
        {
            "Key": 235,
            "Value": {
                "id": "e86e900c-c0c1-4657-a3be-c34a4dca0c44",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 235,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 86,
                "y": 20
            }
        },
        {
            "Key": 236,
            "Value": {
                "id": "0a170cf7-fe81-44b1-990f-7cf00e0c3858",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 236,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 3,
                "x": 12,
                "y": 83
            }
        },
        {
            "Key": 237,
            "Value": {
                "id": "a54d44b8-288b-4bcb-a0b7-99812ddd745c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 237,
                "h": 7,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 37,
                "y": 83
            }
        },
        {
            "Key": 238,
            "Value": {
                "id": "70ec8cb5-8351-4c8b-9cf3-442cc2f52e8f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 238,
                "h": 7,
                "offset": 1,
                "shift": 7,
                "w": 3,
                "x": 22,
                "y": 83
            }
        },
        {
            "Key": 239,
            "Value": {
                "id": "d69c3a4f-57ec-4c92-a6cf-a5288a37eb36",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 239,
                "h": 7,
                "offset": 1,
                "shift": 7,
                "w": 3,
                "x": 27,
                "y": 83
            }
        },
        {
            "Key": 240,
            "Value": {
                "id": "4da96ed1-13fa-40ee-8bdc-33c0cb1ce70a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 240,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 93,
                "y": 20
            }
        },
        {
            "Key": 241,
            "Value": {
                "id": "d1082e40-e6ec-4004-81f5-c6f0e01a8724",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 241,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 100,
                "y": 20
            }
        },
        {
            "Key": 242,
            "Value": {
                "id": "7bf5bb4d-a3f2-4105-8220-04db8f1844b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 242,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 107,
                "y": 20
            }
        },
        {
            "Key": 243,
            "Value": {
                "id": "9f30661b-74e0-4029-bc6e-fe9056f10696",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 243,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 114,
                "y": 20
            }
        },
        {
            "Key": 244,
            "Value": {
                "id": "8ff2c466-90ab-4108-b7f1-4ad5fc7817b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 244,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 2,
                "y": 29
            }
        },
        {
            "Key": 245,
            "Value": {
                "id": "082c4297-b209-42c2-b0b5-b96197c7d756",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 245,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 9,
                "y": 29
            }
        },
        {
            "Key": 246,
            "Value": {
                "id": "851bbcf8-bb10-43d5-a339-2db1b30be32d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 246,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 16,
                "y": 29
            }
        },
        {
            "Key": 247,
            "Value": {
                "id": "f00b1ae5-7f60-4669-a19f-d079b5f555d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 247,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 23,
                "y": 29
            }
        },
        {
            "Key": 248,
            "Value": {
                "id": "0d5f4973-4b33-4501-a8fa-cadb2737fc20",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 248,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 30,
                "y": 29
            }
        },
        {
            "Key": 249,
            "Value": {
                "id": "20c0559f-0af8-47f5-aae8-f8f4b1875ab0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 249,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 37,
                "y": 29
            }
        },
        {
            "Key": 250,
            "Value": {
                "id": "aba78a57-3847-4aa8-80ff-1b65c8570c90",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 250,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 44,
                "y": 29
            }
        },
        {
            "Key": 251,
            "Value": {
                "id": "f2d0dc11-5027-48c0-85e9-af2f6d47aa4d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 251,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 51,
                "y": 29
            }
        },
        {
            "Key": 252,
            "Value": {
                "id": "a9870d7c-92e3-4096-91ff-cd77d6027564",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 252,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 58,
                "y": 29
            }
        },
        {
            "Key": 253,
            "Value": {
                "id": "2c8ab9e0-fc89-4f5f-97b0-ae202dca0165",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 253,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 65,
                "y": 29
            }
        },
        {
            "Key": 254,
            "Value": {
                "id": "55dffe47-de2e-4370-ad2c-ec33a6845d8f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 254,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 79,
                "y": 11
            }
        },
        {
            "Key": 255,
            "Value": {
                "id": "37e819b7-4c6e-43ed-894a-5dd2309984f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 255,
                "h": 7,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 65,
                "y": 74
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 255
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 6,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}