{
    "id": "d5381bc3-859d-4167-a839-0c76e3daca95",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fntTinyCasable",
    "AntiAlias": 0,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "04b03",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "fbc0b5fc-d4d8-47f5-b7db-dcc847d4f556",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 8,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 90,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "0f257480-3d49-4a45-a763-a1c717162ab5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 8,
                "offset": 0,
                "shift": 2,
                "w": 1,
                "x": 15,
                "y": 42
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "764acea9-cbe5-4d73-8ab1-ac5cb1a34047",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 8,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 12,
                "y": 32
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "a873faa3-b924-4056-b18d-3fbb1171d4c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 8,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 16,
                "y": 2
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "71d2e058-b0f2-4e03-9163-4a2ffaa7c345",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 8,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 98,
                "y": 22
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "ea9de53e-e8b0-446a-b7d9-ba2ecef7f406",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 8,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 30,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "972584a4-8c24-4e75-ae7d-43f35fef9d54",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 8,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 37,
                "y": 2
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "b3cc3d0c-e572-4aa5-af8b-059a59815a88",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 8,
                "offset": 0,
                "shift": 2,
                "w": 1,
                "x": 9,
                "y": 42
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "775f88e7-29c0-4f4d-97ed-f9702d7f34e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 8,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 113,
                "y": 32
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "17b7c0e3-6878-4ecc-a7af-3c30be92a260",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 8,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 109,
                "y": 32
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "5c402a73-6b2b-44c8-832a-36e9e3e6bd6f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 8,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 110,
                "y": 22
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "8c4d8575-9928-439d-abd2-b819f275d2b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 8,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 27,
                "y": 32
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "59bf8927-1058-4e6d-ab10-7a16e6b91a71",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 8,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 2,
                "y": 42
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "a208d520-177a-432d-84cc-8fc678ca6562",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 8,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 115,
                "y": 22
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "084d6570-903e-4cd5-a746-56f570da3290",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 8,
                "offset": 0,
                "shift": 2,
                "w": 1,
                "x": 27,
                "y": 42
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "20305f20-1474-44d2-a0c5-c63b57a299dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 8,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 58,
                "y": 2
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "dd51a292-30e3-4558-8071-8f29b28ddece",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 8,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 62,
                "y": 22
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "f2c06e42-9f54-42be-a20a-e10745d9386d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 8,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 105,
                "y": 32
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "f47af6a0-1997-46e7-89bc-976fa264b7ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 8,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 56,
                "y": 22
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "00adbdef-7202-49a6-948b-0a894f4f2242",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 8,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 44,
                "y": 22
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "94d2776e-78c5-48d2-8569-dc470582b772",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 8,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 32,
                "y": 22
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "c1bcbf48-612b-4bc5-abdd-59842433a516",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 8,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 68,
                "y": 22
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "17118a46-4e1e-4b2a-bf28-cb120c39c436",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 8,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 74,
                "y": 22
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "3206ce6a-94a7-4ea7-b9d4-cb98bb238078",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 8,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 20,
                "y": 22
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "2158ffa6-c094-44fb-9f44-8f41f5da0bf6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 8,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 38,
                "y": 22
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "48636c59-122c-4e77-b596-17d7693055f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 8,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 50,
                "y": 22
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "7cdf2c66-b931-416d-ae36-e6c83399a4d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 8,
                "offset": 0,
                "shift": 2,
                "w": 1,
                "x": 12,
                "y": 42
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "14b70aa7-08c4-497f-a035-bd3f2c13f5b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 8,
                "offset": 0,
                "shift": 2,
                "w": 1,
                "x": 18,
                "y": 42
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "384554e7-a641-4565-8046-1b7ca2f012e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 8,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 77,
                "y": 32
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "6bc33358-49ba-495f-822b-6b87b7854942",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 8,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 82,
                "y": 32
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "99a7db4a-2ec3-48d3-a712-44ef41314901",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 8,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 67,
                "y": 32
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "81e7d46f-a89d-4213-aeb0-30a6b331fd2d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 8,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 80,
                "y": 22
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "1c2dcfe2-b648-410e-84ef-f8e5a2410359",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 8,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "131058d1-c81e-4f92-b329-1d9d920e0fec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 8,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 86,
                "y": 22
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "fdf19f83-299a-457b-9c76-000565dcf17c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 8,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 92,
                "y": 22
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "6846e849-1b24-42d5-861d-65ae4df5d209",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 8,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 57,
                "y": 32
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "7b27b114-25e1-40c9-a084-68173f422bb1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 8,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 104,
                "y": 22
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "736135e1-a4b2-49aa-9f6c-cfebc7bc4063",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 8,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 47,
                "y": 32
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "1c356a59-dc1a-4164-9830-c9e79e241b3e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 8,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 42,
                "y": 32
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "ede68155-e24c-4b58-899a-e75bb6d802e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 8,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 26,
                "y": 22
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "c49c9038-f2c5-40d1-9919-5e6d9dced8ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 8,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 2,
                "y": 22
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "871d3999-2b84-4ff0-9d35-eabc166c3c26",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 8,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 87,
                "y": 32
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "046a8d29-275d-4155-b43e-39488beb4f1f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 8,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 104,
                "y": 12
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "01c72f26-18a8-4bf2-9b2c-7dbe149bd78b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 8,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 2,
                "y": 12
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "3c9ac78e-739c-4870-b492-9347cda72ec5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 8,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 52,
                "y": 32
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "e6d04ae9-9374-4930-8d83-f1e2ee150f8f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 8,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 9,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "3df02e5a-e594-454f-a6b1-054a6a8a1def",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 8,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 120,
                "y": 2
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "cefa949b-7c6a-49ca-8997-0aae721b78d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 8,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 8,
                "y": 22
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "c5877cf5-b071-4a94-82ed-6f2b008aba67",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 8,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 114,
                "y": 2
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "79e2a953-4120-4ac8-a792-814106c86eaf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 8,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 108,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "79ffe87c-068d-448d-afeb-e3f921fc7062",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 8,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 102,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "6a06e846-10bf-46a8-9ea3-e9c5cef70650",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 8,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 96,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "b7018df1-9280-4ac2-996b-97d494730253",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 8,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 72,
                "y": 32
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "92246f0c-52b5-463b-91c1-3dbe3985c3d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 8,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 8,
                "y": 12
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "659789fb-bdd6-48ad-adad-f71e31489f23",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 8,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 84,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "32e063ca-8119-462b-9ca1-d256071c0b75",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 8,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 44,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "21f8f8cf-3c0b-421f-b7ca-3debc81305d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 8,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 78,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "653e4f0b-210b-4cca-b407-c521af0082df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 8,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 72,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "e27783b7-b68e-429c-8dfb-ee2bb352bdfe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 8,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 92,
                "y": 32
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "d940babe-18c2-4f33-b5c8-ea3135a0248e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 8,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 101,
                "y": 32
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "e60f2b3c-a8f5-4fa1-b514-56a4ced3db29",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 8,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 51,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "b6fb595f-fe47-4e60-81ec-2a6e1e935217",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 8,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 97,
                "y": 32
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "2ed9cdf0-7e66-4dbb-b79e-21c8e6ad173c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 8,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 37,
                "y": 32
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "f0d30997-b4e1-4b75-96ce-2dde615d4f6b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 8,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 14,
                "y": 12
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "2664509e-85aa-4ddc-8e50-92e8f1de081e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 8,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 121,
                "y": 32
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "d792032b-572f-4315-b065-29010fca3908",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 8,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 68,
                "y": 12
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "9250e1bb-a787-4725-8bab-fa31de5f96e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 8,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 26,
                "y": 12
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "d07a5d1e-75d0-4123-8333-babedf44dbf3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 8,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 32,
                "y": 32
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "f3cff788-1cf9-48e4-96f5-a91b633736f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 8,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 110,
                "y": 12
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "53b19c9a-d22b-404d-86de-543d539b2dfb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 8,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 20,
                "y": 12
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "ac7013fc-d474-4dbd-ab55-a9e0ce9add44",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 8,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 2,
                "y": 32
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "4d80679d-b98b-46df-94a4-a1e9eef26859",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 8,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 98,
                "y": 12
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "1cf5e911-724b-4ac3-a0e3-445e000e7fd9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 8,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 92,
                "y": 12
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "254a8a20-d3ba-4edb-8bb3-d5bb5476b681",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 8,
                "offset": 0,
                "shift": 2,
                "w": 1,
                "x": 24,
                "y": 42
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "ec1ce821-25f0-4e85-82e4-5922318155a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 8,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 117,
                "y": 32
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "0585abdf-7673-417f-b9b1-bd178ec6db47",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 8,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 86,
                "y": 12
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "033e323c-4b32-488c-9862-e2a587bcacbe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 8,
                "offset": 0,
                "shift": 2,
                "w": 1,
                "x": 6,
                "y": 42
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "2c4bb890-ef8b-47da-913a-4a0e6b61b0ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 8,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 65,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "622eba5b-020e-4dd9-a617-f812af1f328b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 8,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 80,
                "y": 12
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "9cb01a27-92b4-4316-b5a3-1f638eead775",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 8,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 116,
                "y": 12
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "28bab5b0-d92f-45d7-a820-462497f7a1bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 8,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 74,
                "y": 12
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "a355885b-7992-4ff7-b290-ddef6a7466c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 8,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 62,
                "y": 12
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "c2930e03-5076-4696-b5fe-2a730fbba1af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 8,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 22,
                "y": 32
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "eb4a521a-aac2-4303-aac3-9b4e04197c8f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 8,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 56,
                "y": 12
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "84b90760-8c5e-414e-8081-7cd6893c0c87",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 8,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 120,
                "y": 22
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "af7b99bb-f1e2-4578-80b5-f1033b3b30ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 8,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 50,
                "y": 12
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "37ce056a-caa6-4a8a-b246-31a514895a82",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 8,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 44,
                "y": 12
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "c7a77296-eca1-469c-b4a0-928110a8cb45",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 8,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 23,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "e5c77e22-1a4c-4fd1-8e5e-95ffc6351515",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 8,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 7,
                "y": 32
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "0dee2d3b-63fc-49bd-bfa8-b72411219555",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 8,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 38,
                "y": 12
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "ad6dedad-a05c-40a9-9817-9da198f5a043",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 8,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 32,
                "y": 12
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "64809195-bcfe-4d2a-b69d-217b8cf356b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 8,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 17,
                "y": 32
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "9d62525a-89f6-4a00-9ac2-2a809fc67d5e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 8,
                "offset": 0,
                "shift": 2,
                "w": 1,
                "x": 21,
                "y": 42
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "3f515280-cfb3-4c30-bad1-601b9dbab442",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 8,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 62,
                "y": 32
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "7b3afeb0-7bc3-414b-b31d-6b2ac9484c12",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 8,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 14,
                "y": 22
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 6,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}