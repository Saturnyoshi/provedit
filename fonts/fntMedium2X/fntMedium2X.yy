{
    "id": "13f716d7-108e-48c6-a3d6-0770b8e1be43",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fntMedium2X",
    "AntiAlias": 0,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Kenney Pixel",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "8fa2afed-c9d2-4690-8a03-231df811f140",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 24,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "9f28f722-4698-44da-a75e-39d815b29648",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 24,
                "offset": 0,
                "shift": 4,
                "w": 2,
                "x": 240,
                "y": 54
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "b35b66d0-eea4-4b2c-b1d4-e434b6edc929",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 24,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 232,
                "y": 54
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "fd294cc3-13af-450b-a1c6-3d6ad9a41d5e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 220,
                "y": 54
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "94facc4b-9aeb-4558-bc07-7fbdc6af2947",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 208,
                "y": 54
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "3a9e4ae7-b143-4807-9e4b-e995ddbb0ed5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 196,
                "y": 54
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "91328e2d-20c8-41d8-9f33-269d46309c0c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 24,
                "offset": 0,
                "shift": 14,
                "w": 12,
                "x": 182,
                "y": 54
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "29f3df2a-3b9a-4bce-8cbd-d05ae781ac02",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 24,
                "offset": 0,
                "shift": 4,
                "w": 2,
                "x": 178,
                "y": 54
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "0df8dacc-c940-4a7c-b3b8-3c479160be63",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 24,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 172,
                "y": 54
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "89f19d5e-f125-4eb2-b523-b4dede0e1645",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 24,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 166,
                "y": 54
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "92aeebb2-5370-4038-a9e3-a90a621ae1d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 24,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 244,
                "y": 54
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "da2dafa2-23a0-4cac-a0e5-4af5e53cdba5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 154,
                "y": 54
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "53429536-ddd3-4fac-89e1-6089a4a28bc9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 24,
                "offset": 0,
                "shift": 4,
                "w": 2,
                "x": 138,
                "y": 54
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "2c9303f2-1f30-45c9-aa03-d1b80ba87286",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 126,
                "y": 54
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "6e6edc4f-29cf-46a1-910a-dfb01cc301ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 24,
                "offset": 0,
                "shift": 4,
                "w": 2,
                "x": 122,
                "y": 54
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "a8960485-cd49-4078-95fe-b5c03980d28d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 110,
                "y": 54
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "52b107e7-a548-42a0-a9d7-3c0da8e55cb0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 98,
                "y": 54
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "b39e103f-844c-4962-aa17-bafd5d3b295f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 24,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 90,
                "y": 54
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "6b5ffec5-1b28-4d26-81ff-86011bb6096d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 78,
                "y": 54
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "7cc65ee1-e049-4a34-ac32-2321dade10f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 66,
                "y": 54
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "a2eb2b22-22a4-4d98-836d-6a27d544a23c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 54,
                "y": 54
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "e5158329-6fd0-46a7-b432-e66871917eff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 142,
                "y": 54
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "30cf29c4-3434-4140-bb3e-cc1a328c1abf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 14,
                "y": 80
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "008ac686-d139-456a-b5ea-a266a8410ea1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 136,
                "y": 80
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "18cddbb0-b363-44bb-914d-378d8d3311cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 26,
                "y": 80
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "24bbbae4-dc3c-4ecc-b15b-5745b6454e89",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 236,
                "y": 80
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "169adcff-0bf9-4f14-8819-1f685d5eb6a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 24,
                "offset": 0,
                "shift": 4,
                "w": 2,
                "x": 232,
                "y": 80
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "2b3f1039-e594-4101-91d2-7cc298ac4a47",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 24,
                "offset": 0,
                "shift": 4,
                "w": 2,
                "x": 228,
                "y": 80
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "e6a66c85-6b2e-4cd0-9207-cdefac41faef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 24,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 218,
                "y": 80
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "50ba55b0-acae-4e4f-8cf2-907a9eafddb0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 206,
                "y": 80
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "efc3d7f3-30fc-4e0e-af49-2dc9408f4a5b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 24,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 196,
                "y": 80
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "547c76f9-675a-40aa-a604-481a40580eb2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 184,
                "y": 80
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "e9d642a8-36b5-4feb-b3b0-512bb5fa6f3a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 172,
                "y": 80
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "457b0404-d47c-4b54-84b2-9d8c030a8805",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 160,
                "y": 80
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "e35d1067-7dbc-4a78-be55-1106aac96a0b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 2,
                "y": 106
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "73f89c95-666c-4795-8ae8-24b264b0d41d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 148,
                "y": 80
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "686ce347-a90d-453b-9669-98f1939b2b78",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 124,
                "y": 80
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "145339da-bdc5-4779-b0e7-51c80ade1849",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 112,
                "y": 80
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "13cea730-c33d-461c-936a-f14c1e97ef14",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 100,
                "y": 80
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "fe3326a0-ba9c-41f0-bc56-43e130d243bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 88,
                "y": 80
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "70fc3ea1-96f5-4a9c-9eab-b4166117b6ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 76,
                "y": 80
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "83ca66c3-e4c5-4641-be40-9c215d2ad82b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 24,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 68,
                "y": 80
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "83e531e9-ffd5-448e-b7d7-881d4f8410b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 24,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 60,
                "y": 80
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "14514486-d1d8-4a1b-b416-171a21471e22",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 48,
                "y": 80
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "fcb35897-a0b1-42f4-8963-32a528d739b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 24,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 38,
                "y": 80
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "d8cd38ad-c1cd-41a2-84e7-c2578f5b7712",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 24,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 38,
                "y": 54
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "4f7f0730-5b4f-47d8-bd2c-f563a2ec1efa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 2,
                "y": 80
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "c50cfb66-086c-4049-b283-b80f2b6450d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 26,
                "y": 54
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "e2cfb8a1-b035-4187-b01b-eec5c30d1e66",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 2,
                "y": 28
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "fef06536-67bf-4259-9639-92cf600ca6e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 220,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "a5e81454-13a1-4d84-8b14-633f14d54f32",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 208,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "46d35866-5200-4235-ac7b-e16c6b513882",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 196,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "64523be2-5b68-4e21-830b-787c1fbe52aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 184,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "a0349d7d-5ec0-4f66-9d1f-87315ed73712",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 172,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "592f4889-f241-4f62-b74f-e01381c3f2db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 160,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "58cdc5c4-e8e2-42e3-a171-a785ef74f0b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 24,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 144,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "bd2f5e1c-9a4a-4805-91e1-e4ad48985dff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 132,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "93a81e14-a1c1-45f4-bd14-d6ea570354fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 120,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "3db748e6-68c4-4b44-9f1c-4f4b1fb10d22",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 232,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "7064a024-b1c5-437c-b3a2-f4a182880c02",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 24,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 114,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "9f07c598-202c-4e80-b572-d1f0bc27796b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 90,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "7a4a29fd-ba08-4eef-875b-2eac49099c54",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 24,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 84,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "d67dcc99-dbb9-4eda-8e4c-6d952d39f15c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 72,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "7fde1474-fd6e-467a-89fb-80c7716e2869",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 60,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "04a76810-1f68-426e-acbd-8b2498bf2639",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 24,
                "offset": 8,
                "shift": 12,
                "w": 2,
                "x": 56,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "d5a932cf-730b-480f-8b4c-5e4c36aa6985",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 44,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "91238009-2de6-43af-b3b3-7a7a8b378c75",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 32,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "4c4860fe-210e-4e6b-ae77-d2bd4e682786",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 20,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "d030418a-6b28-4831-89ea-b717208eafb6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 8,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "5a6d8b8f-680f-4327-a28e-691cff8aed8c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 102,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "46f0b659-db6e-4f52-8e32-3fe1a7c17183",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 14,
                "y": 28
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "7636528b-d781-4d82-bf9e-2cd2e38290e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 136,
                "y": 28
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "b9ef2b88-dd51-4c14-b4d8-84cde4a9c540",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 26,
                "y": 28
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "a8b895b1-6e51-45d7-b0b0-4f594a7c4ee9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 24,
                "offset": 0,
                "shift": 4,
                "w": 2,
                "x": 2,
                "y": 54
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "1dd002f2-a836-4dad-8a65-59bd0b089a7b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 24,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 246,
                "y": 28
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "da41ab61-a109-4588-88a0-ab1fc9345ca0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 234,
                "y": 28
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "98972dc0-baf6-4385-bfad-c8e9318dccbc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 24,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 224,
                "y": 28
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "62270657-9d34-4238-a1e3-5bfe94dbfdfa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 24,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 208,
                "y": 28
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "ff1b618c-55e9-47ea-821d-98f3ec149a16",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 196,
                "y": 28
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "01c44e22-8d63-4fa4-91e0-7a980a6b1d7f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 184,
                "y": 28
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "7896a630-e048-441c-9444-43ca47716f45",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 172,
                "y": 28
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "8f3a8540-2050-48f1-bdf8-7481fc06a9e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 160,
                "y": 28
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "9817bd8b-d76f-4630-869a-d7285a2fd471",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 6,
                "y": 54
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "c00fbc93-db6b-433a-b4ba-b2fe6f4fe70a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 148,
                "y": 28
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "f7bb7f9c-5f30-425e-8f19-d2e404f5f4af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 24,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 126,
                "y": 28
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "9f09a458-faf9-44eb-a038-ccdf9844e93a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 114,
                "y": 28
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "4d9a736c-580b-48ff-9101-fd0b865e2765",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 102,
                "y": 28
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "90c0d782-af66-407a-af76-458f216cb07c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 24,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 86,
                "y": 28
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "65fed54b-888b-4076-a252-4ae47c9ad850",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 74,
                "y": 28
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "92f7a028-b524-4abc-be5b-bd76844110d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 62,
                "y": 28
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "7d13722d-b386-49bf-8d97-b74f4612480e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 50,
                "y": 28
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "3a6b171d-3702-461b-adc9-5930aeb740d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 24,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 42,
                "y": 28
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "dc924294-dbb1-45ca-86bc-6c07189e6234",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 24,
                "offset": 0,
                "shift": 4,
                "w": 2,
                "x": 38,
                "y": 28
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "c1b4a52f-1ee6-4da0-8660-1cc9f195f33d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 24,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 18,
                "y": 54
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "10e40a4a-3311-4368-a5c9-aa58d58e17e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 24,
                "offset": 0,
                "shift": 14,
                "w": 12,
                "x": 14,
                "y": 106
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 24,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}