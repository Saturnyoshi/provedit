{
    "id": "372eb6f4-c62d-4f05-a20e-263c8c87af7c",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fntTiny2X",
    "AntiAlias": 0,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Kenney Mini Square",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "9a1597fb-bea3-4224-a003-beddc12677d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 20,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 38,
                "y": 68
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "98111d5e-1463-4bf6-ab5c-59af5852f09d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 20,
                "offset": 0,
                "shift": 4,
                "w": 2,
                "x": 86,
                "y": 68
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "d472c6c9-f560-404f-aede-0d3a65bbc27d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 182,
                "y": 46
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "7898a009-8e2b-430e-8e61-3f84494b1949",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 20,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 14,
                "y": 2
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "3ddc760d-a771-4646-8b61-72720ba483ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 190,
                "y": 46
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "04fe5da1-a567-48e1-8056-655a4d36756f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 20,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 38,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "12cb7167-babe-49ce-929f-6a43241c7ca0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 20,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 50,
                "y": 2
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "e8c13378-207e-426a-ba7f-07cea1ff6580",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 20,
                "offset": 0,
                "shift": 4,
                "w": 2,
                "x": 78,
                "y": 68
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "32e4eb41-9bad-40db-8061-cea91f8fd72d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 20,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 26,
                "y": 68
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "998dd126-87fe-4e3e-bae1-bd8e1880b3b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 20,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 32,
                "y": 68
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "44dcf675-5793-438d-94c2-c3fa47e3c48a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 158,
                "y": 46
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "df5db397-0f03-4110-a722-afa4249cf218",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 206,
                "y": 46
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "af270804-b8b8-4dd7-a142-c4193d4170ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 20,
                "offset": 0,
                "shift": 4,
                "w": 2,
                "x": 94,
                "y": 68
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "c4d06f7b-21fa-475c-a68f-5cc980564ab3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 18,
                "y": 68
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "86a5dfa6-9404-413c-852e-470ac3850536",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 20,
                "offset": 0,
                "shift": 4,
                "w": 2,
                "x": 62,
                "y": 68
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "59ab22a8-82b9-4728-84df-a4611f293cba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 20,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 122,
                "y": 2
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "97f72baa-8e63-4487-947c-9302c739d1e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 122,
                "y": 46
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "6be4048a-23f5-428b-b21a-b5f09288839b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 20,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 44,
                "y": 68
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "3a247a0a-39a7-4afc-9d30-8b4677e6b65f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 2,
                "y": 46
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "a1889368-ed9c-4ea1-98ad-03f7c090da4f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 242,
                "y": 24
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "42d71fa7-121d-4e63-bbe1-d588be00b1cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 232,
                "y": 24
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "904050d6-d8df-4286-9a48-ff78759a081f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 222,
                "y": 24
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "72e409fb-9d7f-41f7-9303-1beea4dd1de0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 212,
                "y": 24
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "1b3d77b6-8f7b-46c8-8d1d-95bc1510d355",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 202,
                "y": 24
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "af960b3b-69f4-45b7-95c8-00a75126e665",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 32,
                "y": 46
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "7120b0a0-6f2e-4210-a6cb-b1ad1ac9b373",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 132,
                "y": 46
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "50286d6d-056a-45ec-9b15-d08c8213abd1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 20,
                "offset": 0,
                "shift": 4,
                "w": 2,
                "x": 70,
                "y": 68
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "c35cb38f-1e03-464f-9719-4280b9ec5de2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 20,
                "offset": 0,
                "shift": 4,
                "w": 2,
                "x": 82,
                "y": 68
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "a1a9fc6a-e67b-41fe-9767-47adf639e8c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 214,
                "y": 46
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "7817a6c8-c04d-4a5e-9c29-a3548695b013",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 230,
                "y": 46
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "f5e7adb7-6aa6-4d9c-acce-ed6867c39fb0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 238,
                "y": 46
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "13a03262-c803-4175-8622-b72fdbc02fec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 12,
                "y": 46
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "7cfe9610-c694-4f34-b61c-5186a1e7801a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 22,
                "y": 46
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "728c565b-9cad-446c-829c-0a799ee0648f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 52,
                "y": 46
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "b3e01f6f-7170-4e7e-913a-e9ed792f53e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 42,
                "y": 46
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "c2e4af6e-3005-4d85-a3e7-1d6a0d0b40b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 62,
                "y": 46
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "e83c6881-9b78-4662-8e94-41158622d689",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 72,
                "y": 46
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "085ef01f-7a81-463f-a002-9afcdc5c0c8d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 82,
                "y": 46
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "61337e03-fc06-457d-ba99-0e8598e7c107",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 92,
                "y": 46
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "03b7ab16-9f98-47a6-b71a-f3ea703b2712",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 102,
                "y": 46
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "f66db79e-83dc-4e31-9437-f8b5af24d94d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 112,
                "y": 46
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "ed959fc9-6441-463a-ad69-a195daddbebd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 20,
                "offset": 0,
                "shift": 4,
                "w": 2,
                "x": 98,
                "y": 68
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "a06374b4-d0c1-469b-bc54-3616e240284f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 166,
                "y": 46
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "ed40479e-61c2-4009-b00d-59023639f73f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 182,
                "y": 24
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "6b79ad03-25be-421e-ae83-4abe92ad185e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 10,
                "y": 68
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "09724db1-0700-4b5e-a479-76e162f1779a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 20,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "0776ba74-1ebc-43e7-bf78-b47d40002b83",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 234,
                "y": 2
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "28249cc0-23d0-4813-8331-e0ffa6d11017",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 134,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "7a0383cc-2672-4ab9-abe2-bcd026edb02c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 224,
                "y": 2
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "56b9d8f7-1371-4f45-bcfd-42558a4fd6e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 214,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "71f0479d-347f-42ad-89cc-7b92c86a56be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 204,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "0ace018e-fae5-485e-a471-a263e1e361ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 194,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "82133c66-8824-4475-80bc-c82a3cb9fdb7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 246,
                "y": 46
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "ff153c77-baae-4536-a4c4-41bb27bf30fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 174,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "72b887da-aabb-459c-82d2-880edc71e891",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 20,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 98,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "2454b8be-d80d-4e57-ae87-a36d82139aae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 20,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 110,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "f128993a-7500-4ab8-a29f-225a2eeaf746",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 244,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "c84b6fb5-f980-4844-ba48-5480da70f42f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 164,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "89e56efa-9755-4ecd-9630-f3698d292aea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 154,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "07411b7c-2245-412f-b7a3-fcda031208d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 20,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 56,
                "y": 68
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "2710a084-33bf-4300-b7a9-5246a7d7bf71",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 20,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 26,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "752eba45-f37c-415f-b0a7-95763f196f50",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 20,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 50,
                "y": 68
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "c3ee5e4d-6b87-4a13-9f7c-bf6034246472",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 198,
                "y": 46
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "eb44ea2e-33b1-4da0-a774-9fd705786394",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 144,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "b6756d17-8d9a-4d2d-ad2b-9b3c97f1908e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 20,
                "offset": 6,
                "shift": 10,
                "w": 2,
                "x": 74,
                "y": 68
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "63c690b3-8685-405c-af0a-7924afed868c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 184,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "3461cb65-3358-47e7-a714-fe25ca7f577b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 2,
                "y": 24
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "e0990253-5f8a-40b2-9cfa-20bb2654ded2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 92,
                "y": 24
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "5f39b9d8-57e3-4952-860a-6ae663292362",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 22,
                "y": 24
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "3d99f4a2-41df-4c77-a3a6-7cad00dc86b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 162,
                "y": 24
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "d7b4a646-11e1-4323-bc0a-5b4435dd0dd4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 152,
                "y": 24
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "70a0c5bc-288c-458e-8758-b317f773c64d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 12,
                "y": 24
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "e7c5ce6e-53cd-44a5-8166-27f8118cc06f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 142,
                "y": 24
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "27640eff-f293-4b20-b809-c15b76c9b167",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 20,
                "offset": 0,
                "shift": 4,
                "w": 2,
                "x": 66,
                "y": 68
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "50626e83-7226-4d00-84a4-b0b87c350801",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 142,
                "y": 46
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "157a79c8-47b5-444d-ae74-3de93061ba43",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 132,
                "y": 24
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "5a5ac580-f0b5-4a31-b110-01dc19823da9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 174,
                "y": 46
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "f2b664a9-f1bf-4691-b192-9b98ecbd8d75",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 20,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 86,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "7a3f33f0-ec23-4fbe-9252-e2b0057b4cc9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 122,
                "y": 24
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "86891c78-b820-4daa-a05e-bfc428d05038",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 112,
                "y": 24
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "399daed7-8e09-4f1f-9368-a9c18d816be6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 172,
                "y": 24
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "41c17677-9523-44d8-8d7a-1f6c687803f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 102,
                "y": 24
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "4897672a-e2f5-40eb-926e-da1031822f2f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 82,
                "y": 24
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "9479e398-f79c-4a39-8dd8-2bd9409c5919",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 72,
                "y": 24
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "526ad9c4-9437-4204-93af-707ff55f7504",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 150,
                "y": 46
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "0616bad3-dd74-455e-bf62-dc4d4ceb0469",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 62,
                "y": 24
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "84c98efc-aefe-43aa-9033-4d98f0b7ddd7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 20,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 74,
                "y": 2
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "54beb33b-452b-4861-8775-634de62a70e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 20,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 62,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "bcb98928-e469-4480-900d-a8c1ada37292",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 52,
                "y": 24
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "871517c6-ff32-4513-a795-5d297098c96d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 42,
                "y": 24
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "3069ce5d-afba-412f-bf96-95ed257be7b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 32,
                "y": 24
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "4ef69b81-6579-4ce0-9b23-50ac276e4354",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 222,
                "y": 46
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "839cb0da-9433-44a3-83c3-deffec35b1d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 20,
                "offset": 0,
                "shift": 4,
                "w": 2,
                "x": 90,
                "y": 68
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "f2105f29-ffd5-45ab-957d-37e400a1750f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 2,
                "y": 68
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "7901d9ae-eab0-4ead-a8fc-0ed2f5b88cf5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 192,
                "y": 24
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 12,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}