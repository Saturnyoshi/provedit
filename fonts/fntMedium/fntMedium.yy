{
    "id": "b4f12974-46ba-4c29-83ec-ba0f89588019",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fntMedium",
    "AntiAlias": 0,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Kenney Pixel",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "e452f327-6987-46df-9c48-d6dc1a82d661",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 12,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 79,
                "y": 58
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "d22b4905-2282-49c0-99b3-9a546d26d067",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 12,
                "offset": 0,
                "shift": 2,
                "w": 1,
                "x": 102,
                "y": 58
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "5fded3e5-871e-43ea-b479-512224f9fd76",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 12,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 49,
                "y": 58
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "ce067185-33d8-4698-9b5a-ab65d6d4e7ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 37,
                "y": 44
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "05fbefc1-c33a-4705-a60a-3f87901c6d70",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 30,
                "y": 44
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "9ca37463-5b64-4fd3-8768-eb4a04c7be66",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 23,
                "y": 44
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "ee9568ba-5cac-49e9-9d1e-4f8f315b2f77",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 12,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 46,
                "y": 2
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "c249e090-7158-4248-856b-cc743e53b3d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 12,
                "offset": 0,
                "shift": 2,
                "w": 1,
                "x": 108,
                "y": 58
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "58fb478b-866a-445e-b9d5-c663c6e3d6ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 12,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 87,
                "y": 58
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "506624d2-00c3-432c-b861-95d1accbc3c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 12,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 83,
                "y": 58
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "9242299b-ec80-42a1-b059-9cf9b36356f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 12,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 39,
                "y": 58
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "45ff6853-fee4-4135-bc1b-762532137a7f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 86,
                "y": 30
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "c587deeb-9bbe-4cda-b61a-21c632656026",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 12,
                "offset": 0,
                "shift": 2,
                "w": 1,
                "x": 114,
                "y": 58
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "be7b0840-5022-4afd-b62c-52112c52ecb3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 72,
                "y": 30
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "18e1be44-8c02-4bb8-9620-d25944dcc223",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 12,
                "offset": 0,
                "shift": 2,
                "w": 1,
                "x": 123,
                "y": 58
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "7b7d7a52-bebf-4288-a27f-3268c65fca7c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 44,
                "y": 44
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "e2214d61-6efb-4ece-8dab-cacc593d61fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 79,
                "y": 30
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "f25dac29-bb92-4426-bfc9-e8957ff6a270",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 12,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 64,
                "y": 58
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "772c2667-3e43-4c39-9d22-e10eb1980c5c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 93,
                "y": 30
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "708bc435-935f-41b7-87c4-dd94762bce86",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 100,
                "y": 30
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "9cd09928-01f7-4b89-9bcf-c42bc1894580",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 65,
                "y": 30
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "b77dd050-e32d-4d2e-91dc-7e6c90d05497",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 107,
                "y": 30
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "efcb90f6-4807-4dcc-86e6-eec4c5c2f8e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 114,
                "y": 30
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "bfb3eb35-7cbd-4a32-bc95-c71776623b67",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 2,
                "y": 44
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "2bcbde3e-7c81-4fee-86e2-ad2e174985a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 9,
                "y": 44
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "3009b533-e86d-493f-be06-f6a592c16d45",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 16,
                "y": 44
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "0b7063f2-a471-4f8c-9966-fc729f477f62",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 12,
                "offset": 0,
                "shift": 2,
                "w": 1,
                "x": 120,
                "y": 58
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "bee3aaaa-363f-485b-b92c-17787c3001d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 12,
                "offset": 0,
                "shift": 2,
                "w": 1,
                "x": 105,
                "y": 58
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "fd2b6c6f-d369-45c8-a301-68767ada541f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 15,
                "y": 58
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "1daf3ae7-4bb5-4d14-81cf-3172b4b5d8c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 107,
                "y": 44
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "6d722539-429a-4ce3-afe7-51396a721a75",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 27,
                "y": 58
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "de888ab0-57f6-405a-ba99-0c2ecb257394",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 65,
                "y": 44
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "c396dbe9-c11a-4ad2-8e87-d685985e8d5d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 72,
                "y": 44
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "a34346bf-8dd2-4039-b198-430109af5dd8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 79,
                "y": 44
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "7e19491e-c1d8-4957-a968-ba1b3c1a9f11",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 86,
                "y": 44
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "4f8901cd-6371-4384-bd69-12324a56cafc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 100,
                "y": 44
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "7378df20-7fdd-4e73-8c73-70ad72f9069e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 51,
                "y": 44
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "b860c149-43b0-4220-8edc-a69494adf879",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 114,
                "y": 44
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "58d3d059-3e07-469d-a490-a59ec2bc0779",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 2,
                "y": 58
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "8a421847-12a7-485f-b174-4c4f86b2aaab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 93,
                "y": 44
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "2ab3f6dc-08d7-4a45-8902-b48f80d5c00b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 58,
                "y": 44
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "bb7ac647-422d-4ce9-90f5-497c022373b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 12,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 69,
                "y": 58
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "065362c6-0074-49a1-aa35-50d038ce9630",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 12,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 54,
                "y": 58
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "e1133f3b-5ecf-4d4c-afa4-ce42346ec4b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 51,
                "y": 30
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "2a7b6dba-be60-4bc9-81b3-2ff8eda955f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 9,
                "y": 58
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "c893d9b9-d171-4c00-b3a3-89220b5ccbf2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 12,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 20,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "bdecc849-1047-4483-9213-295abb1c0ffe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 23,
                "y": 16
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "18e904fb-c1f6-4b37-86fd-929d49e71e6e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 58,
                "y": 30
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "a0d3390c-2acb-4d97-b18d-e48ef1ac7c0e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 16,
                "y": 16
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "5879845d-67f0-4274-a818-adcea31a0a90",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 9,
                "y": 16
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "9aef57e3-3220-4aa4-9cae-2a85a08c6799",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 2,
                "y": 16
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "a07ce435-c0a7-49d5-9dee-a2ede2db928c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 117,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "7d54f87e-2f04-4e78-858e-ff5dd071b799",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 110,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "e5739b02-df53-41a3-8655-de36324af6cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 103,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "0cd23a97-a1f4-4849-9280-f6c5491552a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 96,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "67896f1c-8137-45b8-80d0-30f8fbae25d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 12,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 29,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "d68a1301-cccc-4608-9817-624a606a63c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 30,
                "y": 16
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "9f2de18f-756f-4a8f-b266-432539222a67",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 89,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "84da6d70-5db4-4cd4-ada9-2af98e1d8418",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 82,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "1891084d-b65f-41b7-9f1c-f955227f1b16",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 12,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 95,
                "y": 58
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "d6534a5c-5d9a-4bbf-90b3-e07b403a1ce8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 75,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "5528cf4c-610d-476f-b56e-642b700d14bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 12,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 91,
                "y": 58
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "a00b505f-be6b-4dab-840a-60bf81ea2ec4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 68,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "a7414b17-9f93-4136-91ea-7ef34fb3c4dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 61,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "5f298593-119c-4271-832a-75515aab9390",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 12,
                "offset": 4,
                "shift": 6,
                "w": 1,
                "x": 111,
                "y": 58
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "9ffd8cfa-5454-440a-823b-6df314fdbcd9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 54,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "050ce9b1-20f7-4e14-8c5e-5ecc1ff9fa59",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 37,
                "y": 16
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "cfc1d580-11e6-4485-9396-8b2567da1820",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 44,
                "y": 30
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "06c8f62b-e26f-4791-8588-04b6fd1cda23",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 44,
                "y": 16
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "8a4eec6f-3434-48eb-9702-12525ef6c98c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 51,
                "y": 16
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "1a747069-ddda-4287-9914-25ffb9a8ab96",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 37,
                "y": 30
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "54e9d8fa-af31-436b-ae0d-8d5f8ee3645c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 30,
                "y": 30
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "18580796-fc41-4dda-8f3d-e0487eb1f46a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 23,
                "y": 30
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "83456425-3bb8-4125-ad37-b9c54b67ce3f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 12,
                "offset": 0,
                "shift": 2,
                "w": 1,
                "x": 117,
                "y": 58
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "de9a210a-0888-4010-b8c8-bbe322336a09",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 12,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 74,
                "y": 58
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "da9e7007-839b-4d51-8b6a-576406f508ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 16,
                "y": 30
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "adea38a6-d8f8-489b-a284-d8039b52fad2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 33,
                "y": 58
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "187f61b1-6be3-4607-973e-f7cc93946881",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 12,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 11,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "971a7a33-e49e-4380-9815-a0c002c3f2a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 9,
                "y": 30
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "f5ed60ac-6410-415a-8469-e6165a9c226b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 2,
                "y": 30
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "dd56461c-b263-4db1-9c0c-a2b6933dcb34",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 114,
                "y": 16
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "0c34c31b-5d72-48f1-86e9-5cabe0e8be4f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 107,
                "y": 16
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "ecf63efa-c915-4823-830f-0e072fd0c483",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 100,
                "y": 16
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "ad8ff492-4415-4cff-adf7-1518e3ea83a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 93,
                "y": 16
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "01c280e7-640e-410d-ad56-ada2b8621575",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 21,
                "y": 58
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "f8014099-2786-4020-9fe8-21483c686cda",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 86,
                "y": 16
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "92b4e7a6-b234-488f-ada4-a4fd76adcd2d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 79,
                "y": 16
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "748a66cc-2c97-4403-bf63-345251b1a6d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 12,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "71b0a766-0b83-436f-9013-7da627cb52be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 72,
                "y": 16
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "1a30352b-8008-4ee8-923c-fc13b8c1861e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 65,
                "y": 16
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "e650bd45-a53d-4fdf-b0fc-be6590060499",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 58,
                "y": 16
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "b37a2a16-cc7c-4b11-ae96-bee044dbe848",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 12,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 44,
                "y": 58
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "baf0d52c-3f7e-412b-9a09-e7999ee96e5f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 12,
                "offset": 0,
                "shift": 2,
                "w": 1,
                "x": 99,
                "y": 58
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "bde3bb25-469a-463d-b955-ff3adac14507",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 12,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 59,
                "y": 58
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "76232b49-da8c-4422-bc54-d3cc5dd90e60",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 12,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 38,
                "y": 2
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 12,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}