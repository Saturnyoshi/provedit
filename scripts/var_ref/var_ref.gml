/// @function var_ref(instance, name)
/// @param instance id
/// @param name string

gml_pragma("forceinline")
return [argument0.id, argument1]