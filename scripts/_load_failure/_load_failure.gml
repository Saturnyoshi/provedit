/// @function _load_failure
/// @param type
/// @param name
/// @param error
scr_alert("Failed to load " + argument0 + " '" + argument1 + "': " + argument2)