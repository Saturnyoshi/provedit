/// @function real_int
/// @param string
if (!string_is_int(argument0))
	return 0
else
	return real(argument0)