{
    "id": "5733e934-49f7-446a-aacf-572fc13ee2f3",
    "modelName": "GMExtension",
    "mvc": "1.0",
    "name": "ProvEditLib",
    "IncludedResources": [
        
    ],
    "androidPermissions": [
        
    ],
    "androidProps": false,
    "androidactivityinject": "",
    "androidclassname": "",
    "androidinject": "",
    "androidmanifestinject": "",
    "androidsourcedir": "",
    "author": "",
    "classname": "",
    "copyToTargets": -1,
    "date": "2019-17-23 03:06:46",
    "description": "",
    "extensionName": "",
    "files": [
        {
            "id": "02d89b75-f8cf-4a42-a8b4-af44bc7f9a4a",
            "modelName": "GMExtensionFile",
            "mvc": "1.0",
            "ProxyFiles": [
                
            ],
            "constants": [
                
            ],
            "copyToTargets": -1,
            "filename": "TilesetLoadLib.gml",
            "final": "",
            "functions": [
                {
                    "id": "7cfad838-bf9b-46ee-be37-2b01d543c4dd",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "_____provedit_tilesetloadlib_init",
                    "help": "",
                    "hidden": false,
                    "kind": 2,
                    "name": "_____provedit_tilesetloadlib_init",
                    "returnType": 1
                },
                {
                    "id": "f963ae78-36e2-4faa-814d-4725b43d6061",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "ProvEdit_tileset_add",
                    "help": "name, image, internalName, resName, modName, hasGrid",
                    "hidden": false,
                    "kind": 2,
                    "name": "ProvEdit_tileset_add",
                    "returnType": 2
                },
                {
                    "id": "ae925979-e0c4-4619-93fd-4f9484624470",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "ProvEdit_load_tilesets",
                    "help": "",
                    "hidden": false,
                    "kind": 2,
                    "name": "ProvEdit_load_tilesets",
                    "returnType": 1
                },
                {
                    "id": "93821e2a-074f-499c-86dc-357778874a81",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "ProvEdit_brush_add",
                    "help": "tileset, name, data",
                    "hidden": false,
                    "kind": 2,
                    "name": "ProvEdit_brush_add",
                    "returnType": 1
                },
                {
                    "id": "bef75e59-6f60-45c0-ac58-383c0cbdf171",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "ProvEdit_load_tilesets_path",
                    "help": "path",
                    "hidden": false,
                    "kind": 2,
                    "name": "ProvEdit_load_tilesets_path",
                    "returnType": 1
                },
                {
                    "id": "0faba487-b65b-4031-8a22-87dc814bf317",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "ProvEdit_tileset_find",
                    "help": "",
                    "hidden": false,
                    "kind": 2,
                    "name": "ProvEdit_tileset_find",
                    "returnType": 1
                },
                {
                    "id": "b3046a3d-331f-4840-8701-3cec4d6746b5",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "ProvEdit_tileset_exists",
                    "help": "id",
                    "hidden": false,
                    "kind": 2,
                    "name": "ProvEdit_tileset_exists",
                    "returnType": 1
                }
            ],
            "init": "_____provedit_tilesetloadlib_init",
            "kind": 2,
            "order": [
                "7cfad838-bf9b-46ee-be37-2b01d543c4dd",
                "f963ae78-36e2-4faa-814d-4725b43d6061",
                "ae925979-e0c4-4619-93fd-4f9484624470",
                "93821e2a-074f-499c-86dc-357778874a81",
                "bef75e59-6f60-45c0-ac58-383c0cbdf171",
                "0faba487-b65b-4031-8a22-87dc814bf317",
                "b3046a3d-331f-4840-8701-3cec4d6746b5"
            ],
            "origname": "",
            "uncompress": false
        },
        {
            "id": "e0c202e6-5c8c-404a-95be-0ac1fd298145",
            "modelName": "GMExtensionFile",
            "mvc": "1.0",
            "ProxyFiles": [
                
            ],
            "constants": [
                
            ],
            "copyToTargets": -1,
            "filename": "ProvEditLibMain.gml",
            "final": "",
            "functions": [
                {
                    "id": "feb12658-9158-46fc-a3bb-75657dd9bd0b",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "_____provedit_libmain_init",
                    "help": "",
                    "hidden": false,
                    "kind": 2,
                    "name": "_____provedit_libmain_init",
                    "returnType": 1
                },
                {
                    "id": "f8815b53-ddb7-4b37-b948-3a9dab1cb4fa",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "ProvEdit_asset_path",
                    "help": "[value]",
                    "hidden": false,
                    "kind": 2,
                    "name": "ProvEdit_asset_path",
                    "returnType": 1
                },
                {
                    "id": "1348a18c-e7f1-4324-872d-345bf426ccae",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "____ProvEdit_load_error",
                    "help": "",
                    "hidden": false,
                    "kind": 2,
                    "name": "____ProvEdit_load_error",
                    "returnType": 1
                },
                {
                    "id": "34a01f4d-cbb8-47e0-872c-118d604b2a56",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "ProvEdit_error_handler",
                    "help": "",
                    "hidden": false,
                    "kind": 2,
                    "name": "ProvEdit_error_handler",
                    "returnType": 1
                },
                {
                    "id": "b363bc88-3e3d-4df1-afa9-17c5c0a4958b",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "ProvEdit_load_default_assets",
                    "help": "path",
                    "hidden": false,
                    "kind": 2,
                    "name": "ProvEdit_load_default_assets",
                    "returnType": 1
                }
            ],
            "init": "_____provedit_libmain_init",
            "kind": 2,
            "order": [
                "feb12658-9158-46fc-a3bb-75657dd9bd0b",
                "f8815b53-ddb7-4b37-b948-3a9dab1cb4fa",
                "1348a18c-e7f1-4324-872d-345bf426ccae",
                "34a01f4d-cbb8-47e0-872c-118d604b2a56",
                "b363bc88-3e3d-4df1-afa9-17c5c0a4958b"
            ],
            "origname": "",
            "uncompress": false
        },
        {
            "id": "7829d74d-d707-4000-af07-d6fde2d735c3",
            "modelName": "GMExtensionFile",
            "mvc": "1.0",
            "ProxyFiles": [
                
            ],
            "constants": [
                
            ],
            "copyToTargets": -1,
            "filename": "MapObjectLoadLib.gml",
            "final": "",
            "functions": [
                {
                    "id": "a57cde60-7139-488e-9a30-f23f55b231d7",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "_____provedit_mapobjectloadlib_init",
                    "help": "",
                    "hidden": false,
                    "kind": 2,
                    "name": "_____provedit_mapobjectloadlib_init",
                    "returnType": 2
                },
                {
                    "id": "7c06c454-c138-471b-a15d-8e424652fa1d",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "ProvEdit_load_objects",
                    "help": "",
                    "hidden": false,
                    "kind": 2,
                    "name": "ProvEdit_load_objects",
                    "returnType": 1
                },
                {
                    "id": "5bb8d12c-7362-4c7e-95ed-b5ff2b99e854",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "ProvEdit_load_objects_path",
                    "help": "path",
                    "hidden": false,
                    "kind": 2,
                    "name": "ProvEdit_load_objects_path",
                    "returnType": 1
                },
                {
                    "id": "bca84209-0a49-437e-939c-ec9b47552004",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "ProvEdit_object_add",
                    "help": "name, sprite, mod, object",
                    "hidden": false,
                    "kind": 2,
                    "name": "ProvEdit_object_add",
                    "returnType": 1
                },
                {
                    "id": "e0baaea7-e96e-4a95-bb7f-a91b6d1855e7",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "ProvEdit_object_exists",
                    "help": "id",
                    "hidden": false,
                    "kind": 2,
                    "name": "ProvEdit_object_exists",
                    "returnType": 1
                },
                {
                    "id": "1e1186da-d5f2-4e95-b5b8-7a91e6b08ad7",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "ProvEdit_object_find",
                    "help": "key",
                    "hidden": false,
                    "kind": 2,
                    "name": "ProvEdit_object_find",
                    "returnType": 1
                }
            ],
            "init": "_____provedit_mapobjectloadlib_init",
            "kind": 2,
            "order": [
                "a57cde60-7139-488e-9a30-f23f55b231d7",
                "7c06c454-c138-471b-a15d-8e424652fa1d",
                "5bb8d12c-7362-4c7e-95ed-b5ff2b99e854",
                "bca84209-0a49-437e-939c-ec9b47552004",
                "e0baaea7-e96e-4a95-bb7f-a91b6d1855e7",
                "1e1186da-d5f2-4e95-b5b8-7a91e6b08ad7"
            ],
            "origname": "",
            "uncompress": false
        },
        {
            "id": "4e136017-962f-4584-9d23-2589dd1b6dde",
            "modelName": "GMExtensionFile",
            "mvc": "1.0",
            "ProxyFiles": [
                
            ],
            "constants": [
                
            ],
            "copyToTargets": -1,
            "filename": "InteractableLoadLib.gml",
            "final": "",
            "functions": [
                {
                    "id": "d30c9cf6-ea7a-4e84-8982-131649317e8a",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "_____provedit_interactableloadlib_init",
                    "help": "",
                    "hidden": false,
                    "kind": 2,
                    "name": "_____provedit_interactableloadlib_init",
                    "returnType": 1
                },
                {
                    "id": "44548e17-f911-47a0-b4c3-6f7088e9a179",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "ProvEdit_load_interactables",
                    "help": "",
                    "hidden": false,
                    "kind": 2,
                    "name": "ProvEdit_load_interactables",
                    "returnType": 1
                },
                {
                    "id": "00e64af4-1c00-4e61-8e5f-cbde2099a31d",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "ProvEdit_load_interactables_path",
                    "help": "path",
                    "hidden": false,
                    "kind": 2,
                    "name": "ProvEdit_load_interactables_path",
                    "returnType": 1
                },
                {
                    "id": "82a590d5-8108-4a5f-949a-c56ea1409d63",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "Provedit_interactable_add",
                    "help": "name, mod, object",
                    "hidden": false,
                    "kind": 2,
                    "name": "Provedit_interactable_add",
                    "returnType": 1
                },
                {
                    "id": "a8177d13-d590-48d0-b5ba-9ff7cafc3194",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "ProvEdit_interactable_exists",
                    "help": "id",
                    "hidden": false,
                    "kind": 2,
                    "name": "ProvEdit_interactable_exists",
                    "returnType": 1
                },
                {
                    "id": "2ceedc00-ad0b-4825-ad0d-7ba2e174cd29",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "ProvEdit_interactable_find",
                    "help": "key",
                    "hidden": false,
                    "kind": 2,
                    "name": "ProvEdit_interactable_find",
                    "returnType": 1
                }
            ],
            "init": "_____provedit_interactableloadlib_init",
            "kind": 2,
            "order": [
                "d30c9cf6-ea7a-4e84-8982-131649317e8a",
                "44548e17-f911-47a0-b4c3-6f7088e9a179",
                "00e64af4-1c00-4e61-8e5f-cbde2099a31d",
                "82a590d5-8108-4a5f-949a-c56ea1409d63",
                "a8177d13-d590-48d0-b5ba-9ff7cafc3194",
                "2ceedc00-ad0b-4825-ad0d-7ba2e174cd29"
            ],
            "origname": "",
            "uncompress": false
        },
        {
            "id": "54849747-15cb-4688-8545-6a67b1ade1f4",
            "modelName": "GMExtensionFile",
            "mvc": "1.0",
            "ProxyFiles": [
                
            ],
            "constants": [
                
            ],
            "copyToTargets": -1,
            "filename": "EnemyLoadLib.gml",
            "final": "",
            "functions": [
                {
                    "id": "1393bbc9-6064-4b0e-8d83-63d5fe77128a",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "_____provedit_enemyloadlib_init",
                    "help": "",
                    "hidden": false,
                    "kind": 2,
                    "name": "_____provedit_enemyloadlib_init",
                    "returnType": 1
                },
                {
                    "id": "281c43b4-b49a-4476-a0dd-3313eeeafdae",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "ProvEdit_load_enemies",
                    "help": "",
                    "hidden": false,
                    "kind": 2,
                    "name": "ProvEdit_load_enemies",
                    "returnType": 1
                },
                {
                    "id": "03984c0f-7694-4284-bb4b-3ae98a34cd86",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "ProvEdit_load_enemies_path",
                    "help": "path",
                    "hidden": false,
                    "kind": 2,
                    "name": "ProvEdit_load_enemies_path",
                    "returnType": 1
                },
                {
                    "id": "2dbbd493-5eb2-47d0-a622-68c31422422e",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "Provedit_enemy_add",
                    "help": "name, mod, object",
                    "hidden": false,
                    "kind": 2,
                    "name": "Provedit_enemy_add",
                    "returnType": 1
                },
                {
                    "id": "55a4dd1d-e956-4bff-8c2f-b8464b028aeb",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "ProvEdit_enemy_exists",
                    "help": "id",
                    "hidden": false,
                    "kind": 2,
                    "name": "ProvEdit_enemy_exists",
                    "returnType": 1
                },
                {
                    "id": "1a0176dc-fd70-406f-8765-0b98e3d50a36",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "ProvEdit_enemy_find",
                    "help": "key",
                    "hidden": false,
                    "kind": 2,
                    "name": "ProvEdit_enemy_find",
                    "returnType": 1
                }
            ],
            "init": "_____provedit_enemyloadlib_init",
            "kind": 2,
            "order": [
                "1393bbc9-6064-4b0e-8d83-63d5fe77128a",
                "281c43b4-b49a-4476-a0dd-3313eeeafdae",
                "03984c0f-7694-4284-bb4b-3ae98a34cd86",
                "2dbbd493-5eb2-47d0-a622-68c31422422e",
                "55a4dd1d-e956-4bff-8c2f-b8464b028aeb",
                "1a0176dc-fd70-406f-8765-0b98e3d50a36"
            ],
            "origname": "",
            "uncompress": false
        },
        {
            "id": "49f89f51-92a1-4806-8c57-435b229a6cf9",
            "modelName": "GMExtensionFile",
            "mvc": "1.0",
            "ProxyFiles": [
                
            ],
            "constants": [
                
            ],
            "copyToTargets": 144713361056071910,
            "filename": "BackgroundLoadLib.gml",
            "final": "",
            "functions": [
                {
                    "id": "5c54adbc-8219-4d8b-820c-fc87b354e857",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "_____provedit_backgroundloadlib_init",
                    "help": "",
                    "hidden": false,
                    "kind": 2,
                    "name": "_____provedit_backgroundloadlib_init",
                    "returnType": 1
                },
                {
                    "id": "3b7d1ce6-aba7-4c1c-beeb-d9e069eb6dbd",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "ProvEdit_load_backgrounds",
                    "help": "",
                    "hidden": false,
                    "kind": 2,
                    "name": "ProvEdit_load_backgrounds",
                    "returnType": 1
                },
                {
                    "id": "fba1ff90-2d6c-41a2-b026-c7273f139c3f",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "ProvEdit_load_backgrounds_path",
                    "help": "path",
                    "hidden": false,
                    "kind": 2,
                    "name": "ProvEdit_load_backgrounds_path",
                    "returnType": 1
                },
                {
                    "id": "ecfe7098-8213-4a8a-84ae-e2581939f4b2",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "ProvEdit_background_add",
                    "help": "name, sprite, internal, resName, modName",
                    "hidden": false,
                    "kind": 2,
                    "name": "ProvEdit_background_add",
                    "returnType": 1
                },
                {
                    "id": "03ca2d68-3e9f-44ca-b1db-b471f4551170",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "ProvEdit_background_exists",
                    "help": "id",
                    "hidden": false,
                    "kind": 2,
                    "name": "ProvEdit_background_exists",
                    "returnType": 1
                },
                {
                    "id": "df093c92-6d68-4d13-93b9-1d2dd2f08ee3",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "ProvEdit_background_find",
                    "help": "internal",
                    "hidden": false,
                    "kind": 2,
                    "name": "ProvEdit_background_find",
                    "returnType": 1
                }
            ],
            "init": "_____provedit_backgroundloadlib_init",
            "kind": 2,
            "order": [
                "5c54adbc-8219-4d8b-820c-fc87b354e857",
                "3b7d1ce6-aba7-4c1c-beeb-d9e069eb6dbd",
                "fba1ff90-2d6c-41a2-b026-c7273f139c3f",
                "ecfe7098-8213-4a8a-84ae-e2581939f4b2",
                "03ca2d68-3e9f-44ca-b1db-b471f4551170",
                "df093c92-6d68-4d13-93b9-1d2dd2f08ee3"
            ],
            "origname": "",
            "uncompress": false
        },
        {
            "id": "231e37c6-000f-4d34-a1fa-cadaf61372d4",
            "modelName": "GMExtensionFile",
            "mvc": "1.0",
            "ProxyFiles": [
                
            ],
            "constants": [
                
            ],
            "copyToTargets": -1,
            "filename": "MusicLoadLib.gml",
            "final": "",
            "functions": [
                {
                    "id": "00f70b7b-17b1-4fc4-b8d1-587f9a2c68d4",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "_____provedit_musicloadlib_init",
                    "help": "",
                    "hidden": false,
                    "kind": 2,
                    "name": "_____provedit_musicloadlib_init",
                    "returnType": 1
                },
                {
                    "id": "0193a124-fbf6-4f28-806e-0cc7a2514c0e",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "ProvEdit_load_music",
                    "help": "",
                    "hidden": false,
                    "kind": 2,
                    "name": "ProvEdit_load_music",
                    "returnType": 1
                },
                {
                    "id": "e2a69c9f-4c7a-4381-b5ed-576760f0bab4",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "ProvEdit_load_music_path",
                    "help": "path",
                    "hidden": false,
                    "kind": 2,
                    "name": "ProvEdit_load_music_path",
                    "returnType": 1
                },
                {
                    "id": "de2aee7c-bff9-41e0-83a6-2c8f3749575e",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "ProvEdit_music_find",
                    "help": "name",
                    "hidden": false,
                    "kind": 2,
                    "name": "ProvEdit_music_find",
                    "returnType": 1
                },
                {
                    "id": "63636573-9288-43d2-96b9-06ab70003b40",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "ProvEdit_music_exists",
                    "help": "value",
                    "hidden": false,
                    "kind": 2,
                    "name": "ProvEdit_music_exists",
                    "returnType": 1
                }
            ],
            "init": "_____provedit_musicloadlib_init",
            "kind": 2,
            "order": [
                "00f70b7b-17b1-4fc4-b8d1-587f9a2c68d4",
                "0193a124-fbf6-4f28-806e-0cc7a2514c0e",
                "e2a69c9f-4c7a-4381-b5ed-576760f0bab4",
                "de2aee7c-bff9-41e0-83a6-2c8f3749575e",
                "63636573-9288-43d2-96b9-06ab70003b40"
            ],
            "origname": "",
            "uncompress": false
        },
        {
            "id": "554c2c4a-cdb2-429a-850c-881745b32d3c",
            "modelName": "GMExtensionFile",
            "mvc": "1.0",
            "ProxyFiles": [
                
            ],
            "constants": [
                
            ],
            "copyToTargets": -1,
            "filename": "LevelFormatLib.gml",
            "final": "",
            "functions": [
                {
                    "id": "b9cd6e44-0718-4edf-9960-32fd2dc5b77e",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "ProvEdit_semver_parse",
                    "help": "string",
                    "hidden": false,
                    "kind": 2,
                    "name": "ProvEdit_semver_parse",
                    "returnType": 1
                },
                {
                    "id": "033058b3-4bfc-4079-9635-f4361133f8f0",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "ProvEdit_semver_compare",
                    "help": "ver, major, [minor, patch]",
                    "hidden": false,
                    "kind": 2,
                    "name": "ProvEdit_semver_compare",
                    "returnType": 1
                },
                {
                    "id": "4a12a934-55a8-4b8a-98c1-a53bc8526c7b",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "_____provedit_levelformatlib_init",
                    "help": "",
                    "hidden": false,
                    "kind": 2,
                    "name": "_____provedit_levelformatlib_init",
                    "returnType": 1
                },
                {
                    "id": "e506e99e-b837-401e-a76d-600aa67bc6b9",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "ProvEdit_semver_get_level_features",
                    "help": "ver",
                    "hidden": false,
                    "kind": 2,
                    "name": "ProvEdit_semver_get_level_features",
                    "returnType": 1
                },
                {
                    "id": "8cf59327-f9dc-4f3a-99c3-6713049b675f",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "ProvEdit_level_read_buffer_background",
                    "help": "buffer, features",
                    "hidden": false,
                    "kind": 2,
                    "name": "ProvEdit_level_read_buffer_background",
                    "returnType": 1
                },
                {
                    "id": "352f7b3b-b42a-4c77-b5f1-344f75556f21",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "ProvEdit_level_read_buffer_object",
                    "help": "buffer, features",
                    "hidden": false,
                    "kind": 2,
                    "name": "ProvEdit_level_read_buffer_object",
                    "returnType": 1
                },
                {
                    "id": "3de05fe0-5a46-460c-aa30-d6f6d1bc4ebe",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "ProvEdit_level_read_buffer_collision",
                    "help": "buffer, features",
                    "hidden": false,
                    "kind": 2,
                    "name": "ProvEdit_level_read_buffer_collision",
                    "returnType": 1
                },
                {
                    "id": "6bdd34a8-78be-48eb-bba4-708b8c6d6010",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "ProvEdit_level_read_buffer_layer",
                    "help": "buffer, features",
                    "hidden": false,
                    "kind": 2,
                    "name": "ProvEdit_level_read_buffer_layer",
                    "returnType": 1
                },
                {
                    "id": "06b7363b-e0f0-44ba-859b-cd8ecb57dd05",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "ProvEdit_level_read",
                    "help": "buffer",
                    "hidden": false,
                    "kind": 2,
                    "name": "ProvEdit_level_read",
                    "returnType": 1
                }
            ],
            "init": "_____provedit_levelformatlib_init",
            "kind": 2,
            "order": [
                "b9cd6e44-0718-4edf-9960-32fd2dc5b77e",
                "033058b3-4bfc-4079-9635-f4361133f8f0",
                "4a12a934-55a8-4b8a-98c1-a53bc8526c7b",
                "e506e99e-b837-401e-a76d-600aa67bc6b9",
                "8cf59327-f9dc-4f3a-99c3-6713049b675f",
                "352f7b3b-b42a-4c77-b5f1-344f75556f21",
                "3de05fe0-5a46-460c-aa30-d6f6d1bc4ebe",
                "6bdd34a8-78be-48eb-bba4-708b8c6d6010",
                "06b7363b-e0f0-44ba-859b-cd8ecb57dd05"
            ],
            "origname": "",
            "uncompress": false
        }
    ],
    "gradleinject": "",
    "helpfile": "",
    "installdir": "",
    "iosProps": false,
    "iosSystemFrameworkEntries": [
        
    ],
    "iosThirdPartyFrameworkEntries": [
        
    ],
    "iosdelegatename": "",
    "iosplistinject": "",
    "license": "",
    "maccompilerflags": "",
    "maclinkerflags": "",
    "macsourcedir": "",
    "packageID": "",
    "productID": "",
    "sourcedir": "",
    "tvosProps": false,
    "tvosSystemFrameworkEntries": [
        
    ],
    "tvosThirdPartyFrameworkEntries": [
        
    ],
    "tvosclassname": "",
    "tvosdelegatename": "",
    "tvosmaccompilerflags": "",
    "tvosmaclinkerflags": "",
    "tvosplistinject": "",
    "version": "0.0.1"
}